import 'package:Biospdatabase/Presenter/form.dart';
import 'package:Biospdatabase/Presenter/parts/view.dart';
import 'package:Biospdatabase/Routes/app_routes.dart';
import 'package:Biospdatabase/data/moor_database.dart';
import 'package:animated_splash/animated_splash.dart';
import 'package:flutter/material.dart';
import 'package:moor/moor.dart';
import 'package:path/path.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

import 'Model/Benificiario.dart';
import 'Presenter/home_page.dart';

void main() {

 // var database  =  MyDatabase();
  WidgetsFlutterBinding.ensureInitialized();


  runApp(MyApp());
}
void _sendSMS(String message, List<String> recipents) async {
  const url = 'sms:84434680?body=sabenta';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Provider(
        create: (_) => MyDatabase(),
        child: MaterialApp(
          title:'Biosp Database',
          theme: ThemeData(

            primaryColor: Colors.indigo,
            //backgroundColor: Colors.white,
            //textSelectionColor: Colors.black

          ),
          routes: {
            AppRoutes.Home: (_) => HomePage(),
            AppRoutes.ViewBens: (_) =>ViewBen()
          },
        ),
    );
  }
}



/**
   *
   *  var database = MyDatabase();
   *
   * //TestWidgetsFlutterBinding.ensureInitialized();
      WidgetsFlutterBinding.ensureInitialized();
      database.addBairro(
      Bairro(nome: 'sambrota',
      createdAt: DateTime.now(),
      updatedAt: DateTime.now())
      );
   *  database.allBairroEntries.then((value) => print(value));
      database.addProvincia(Provincia( nome: 'Maputo', createdAt: DateTime.now(), updatedAt: DateTime.now()));
      database.allProvinciaEntries.then((value) => print(value));
      database.addProviniencia(Proviniencia(nome: 'Comunidade', createdAt: DateTime.now(), updatedAt: DateTime.now()));
      database.allProvinienciaEntries.then((value) => print(value));
      database.addSexo(Sexo( nome: 'Masculino', createdAt: DateTime.now(), updatedAt: DateTime.now()));
      database.allSexoEntries.then((value) => print(value));
      database.addDocumento(Documento( nome: 'B.I', createdAt: DateTime.now(), updatedAt: DateTime.now()));
      database.allDocumentoEntries.then((value) => print(value));
      database.addMotivoDeAberturaDeProcesso(MotivoDeAberturaDeProcesso(nome: 'Visita', createdAt: DateTime.now(), updatedAt: DateTime.now()));
      database.allMotivoDeAberturaDeProcessoEntries.then((value) => print(value));
      database.addServicoEncaminhado(ServicoEncaminhado(nome: 'Fip', createdAt: DateTime.now(), updatedAt: DateTime.now()));
      database.allServicoEncaminhadoEntries.then((value) => print(value));
      database.addObjectivoDaVisita(ObjectivoDaVisita(nome: 'Conhecer Biosp', createdAt:DateTime.now() , updatedAt: DateTime.now()));
      database.allObjectivoDaVisitaEntries.then((value) => print(value));
      database.addBenificiario(Benificiario(nome: 'Nelson Mutane',  numeroDeVisita: 2, dataDeNascimento:DateTime.now(),provinciaId:1,bairroId: 1, sexoId: 1, provinienciaId: 1,ObjectivoDaVistaId: 1,dataDeAtendimento: DateTime.now(), createdAt: DateTime.now(), updatedAat: DateTime.now()));
      database.allBenificiarioEntries.then((value) => print(value));
      database.addBenificiarioEncaminhado(BenificiarioEncaminhado(necessitaDeAcompanhamenBairromiciliar: true,benificiarioId: 1, dataQueFoiRecebido: DateTime.now(), problemaResolvido: true, createdAt: DateTime.now(), updatedAt: DateTime.now()));
      database.allBenificiarioEncaminhadoEntries.then((value) => print(value));
      database.addContacto(Contacto(contacto: '+258847607095', NomeDoResponsavel: 'Inkomo', benificiarioId: 1,createdAt: DateTime.now(), updatedAt: DateTime.now()));
      database.allContactoEntries.then((value) => print(value));
      database.addBenificiarioEncaminhadoDocumento(BenificiarioEncaminhadoDocumento(especificar: 'FIP', benificiarioEncaminhadoId: 1,documentoId: 1,createdAt: DateTime.now(), updatedAt: DateTime.now()));
      database.allBenificiarioEncaminhadoDocumentoEntries.then((value) => print(value));
      database.addBenificiarioEncaminhadoServicoEncaminhado(BenificiarioEncaminhadoServicoEncaminhado(especificar: 'Dic',benificiarioEncaminhadoId:1,servicoEncaminhadoId:1, createdAt: DateTime.now(), updatedAt: DateTime.now()));
      database.allBenificiarioEncaminhadoServicoEncaminhadoEntries.then((value) => print(value));
   */
 // database.addBenificiario(Benificiario(nome: 'Nelson Mutane',  numeroDeVisita: 2, dataDeNascimento:DateTime.now(),provinciaId:1,bairroId: 1, sexoId: 1, provinienciaId: 1,ObjectivoDaVistaId: 1,dataDeAtendimento: DateTime.now(), createdAt: DateTime.now(), updatedAat: DateTime.now()));
///  database.addBenificiarioEncaminhadoDocumento(BenificiarioEncaminhadoDocumento(especificar: 'FIP', benificiarioEncaminhadoId: 1,documentoId: 3,createdAt: DateTime.now(), updatedAt: DateTime.now()));
  //database.addDocumento(Documento( nome: 'Cedula.I', createdAt: DateTime.now(), updatedAt: DateTime.now()));
  //database.addDocumento(Documento( nome: 'Livramento', createdAt: DateTime.now(), updatedAt: DateTime.now()));
//  database.allBenificiarioEntries.then((value) => value.forEach((element) async {print(await BenificiarioResource.resource(database,element));}));
 // database.allProvinienciaEntries.then((value) => print(value));
/**
    WidgetsFlutterBinding.ensureInitialized();
    var database  = MyDatabase();
    var t = database.select(database.benificiarios )..where((row) =>
    row.createdAt.month.equals(DateTime.now().month) &
    row.createdAt.year.equals(DateTime.now().year));
    var b  = t.get();

    database.allBenificiarioEntries.then(
    (value) async{
    var std =  await List.generate(value.length, (index) async{
    return  BenificiarioResource().maps(database,value.elementAt(index));

    });
    std.forEach((element) {
    element.then((value) => print(value.ref));
    });
    }
    //  value.forEach((element) async {print(await BenificiarioResource.resource(database,element));})
    );*/