// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'moor_database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class Bairro extends DataClass implements Insertable<Bairro> {
  final int id;
  final String nome;
  final DateTime createdAt;
  final DateTime updatedAt;
  Bairro(
      {@required this.id,
      @required this.nome,
      @required this.createdAt,
      @required this.updatedAt});
  factory Bairro.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return Bairro(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      nome: stringType.mapFromDatabaseResponse(data['${effectivePrefix}nome']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      updatedAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || nome != null) {
      map['nome'] = Variable<String>(nome);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<DateTime>(createdAt);
    }
    if (!nullToAbsent || updatedAt != null) {
      map['updated_at'] = Variable<DateTime>(updatedAt);
    }
    return map;
  }

  BairrosCompanion toCompanion(bool nullToAbsent) {
    return BairrosCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      nome: nome == null && nullToAbsent ? const Value.absent() : Value(nome),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  factory Bairro.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Bairro(
      id: serializer.fromJson<int>(json['id']),
      nome: serializer.fromJson<String>(json['nome']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'nome': serializer.toJson<String>(nome),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

  Bairro copyWith(
          {int id, String nome, DateTime createdAt, DateTime updatedAt}) =>
      Bairro(
        id: id ?? this.id,
        nome: nome ?? this.nome,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('Bairro(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(id.hashCode,
      $mrjc(nome.hashCode, $mrjc(createdAt.hashCode, updatedAt.hashCode))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Bairro &&
          other.id == this.id &&
          other.nome == this.nome &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class BairrosCompanion extends UpdateCompanion<Bairro> {
  final Value<int> id;
  final Value<String> nome;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  const BairrosCompanion({
    this.id = const Value.absent(),
    this.nome = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  BairrosCompanion.insert({
    this.id = const Value.absent(),
    @required String nome,
    @required DateTime createdAt,
    @required DateTime updatedAt,
  })  : nome = Value(nome),
        createdAt = Value(createdAt),
        updatedAt = Value(updatedAt);
  static Insertable<Bairro> custom({
    Expression<int> id,
    Expression<String> nome,
    Expression<DateTime> createdAt,
    Expression<DateTime> updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (nome != null) 'nome': nome,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  BairrosCompanion copyWith(
      {Value<int> id,
      Value<String> nome,
      Value<DateTime> createdAt,
      Value<DateTime> updatedAt}) {
    return BairrosCompanion(
      id: id ?? this.id,
      nome: nome ?? this.nome,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (nome.present) {
      map['nome'] = Variable<String>(nome.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('BairrosCompanion(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $BairrosTable extends Bairros with TableInfo<$BairrosTable, Bairro> {
  final GeneratedDatabase _db;
  final String _alias;
  $BairrosTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _nomeMeta = const VerificationMeta('nome');
  GeneratedTextColumn _nome;
  @override
  GeneratedTextColumn get nome => _nome ??= _constructNome();
  GeneratedTextColumn _constructNome() {
    return GeneratedTextColumn('nome', $tableName, false, maxTextLength: 191);
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedDateTimeColumn _updatedAt;
  @override
  GeneratedDateTimeColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedDateTimeColumn _constructUpdatedAt() {
    return GeneratedDateTimeColumn(
      'updated_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, nome, createdAt, updatedAt];
  @override
  $BairrosTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'bairros';
  @override
  final String actualTableName = 'bairros';
  @override
  VerificationContext validateIntegrity(Insertable<Bairro> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('nome')) {
      context.handle(
          _nomeMeta, nome.isAcceptableOrUnknown(data['nome'], _nomeMeta));
    } else if (isInserting) {
      context.missing(_nomeMeta);
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at'], _updatedAtMeta));
    } else if (isInserting) {
      context.missing(_updatedAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Bairro map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Bairro.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $BairrosTable createAlias(String alias) {
    return $BairrosTable(_db, alias);
  }
}

class Contacto extends DataClass implements Insertable<Contacto> {
  final int id;
  final String contacto;
  final String NomeDoResponsavel;
  final int benificiarioId;
  final DateTime createdAt;
  final DateTime updatedAt;
  Contacto(
      {@required this.id,
      @required this.contacto,
      @required this.NomeDoResponsavel,
      this.benificiarioId,
      @required this.createdAt,
      @required this.updatedAt});
  factory Contacto.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return Contacto(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      contacto: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}contacto']),
      NomeDoResponsavel: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}nome_do_responsavel']),
      benificiarioId: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}benificiario_id']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      updatedAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || contacto != null) {
      map['contacto'] = Variable<String>(contacto);
    }
    if (!nullToAbsent || NomeDoResponsavel != null) {
      map['nome_do_responsavel'] = Variable<String>(NomeDoResponsavel);
    }
    if (!nullToAbsent || benificiarioId != null) {
      map['benificiario_id'] = Variable<int>(benificiarioId);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<DateTime>(createdAt);
    }
    if (!nullToAbsent || updatedAt != null) {
      map['updated_at'] = Variable<DateTime>(updatedAt);
    }
    return map;
  }

  ContactosCompanion toCompanion(bool nullToAbsent) {
    return ContactosCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      contacto: contacto == null && nullToAbsent
          ? const Value.absent()
          : Value(contacto),
      NomeDoResponsavel: NomeDoResponsavel == null && nullToAbsent
          ? const Value.absent()
          : Value(NomeDoResponsavel),
      benificiarioId: benificiarioId == null && nullToAbsent
          ? const Value.absent()
          : Value(benificiarioId),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  factory Contacto.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Contacto(
      id: serializer.fromJson<int>(json['id']),
      contacto: serializer.fromJson<String>(json['contacto']),
      NomeDoResponsavel: serializer.fromJson<String>(json['NomeDoResponsavel']),
      benificiarioId: serializer.fromJson<int>(json['benificiarioId']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'contacto': serializer.toJson<String>(contacto),
      'NomeDoResponsavel': serializer.toJson<String>(NomeDoResponsavel),
      'benificiarioId': serializer.toJson<int>(benificiarioId),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

  Contacto copyWith(
          {int id,
          String contacto,
          String NomeDoResponsavel,
          int benificiarioId,
          DateTime createdAt,
          DateTime updatedAt}) =>
      Contacto(
        id: id ?? this.id,
        contacto: contacto ?? this.contacto,
        NomeDoResponsavel: NomeDoResponsavel ?? this.NomeDoResponsavel,
        benificiarioId: benificiarioId ?? this.benificiarioId,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('Contacto(')
          ..write('id: $id, ')
          ..write('contacto: $contacto, ')
          ..write('NomeDoResponsavel: $NomeDoResponsavel, ')
          ..write('benificiarioId: $benificiarioId, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          contacto.hashCode,
          $mrjc(
              NomeDoResponsavel.hashCode,
              $mrjc(benificiarioId.hashCode,
                  $mrjc(createdAt.hashCode, updatedAt.hashCode))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Contacto &&
          other.id == this.id &&
          other.contacto == this.contacto &&
          other.NomeDoResponsavel == this.NomeDoResponsavel &&
          other.benificiarioId == this.benificiarioId &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class ContactosCompanion extends UpdateCompanion<Contacto> {
  final Value<int> id;
  final Value<String> contacto;
  final Value<String> NomeDoResponsavel;
  final Value<int> benificiarioId;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  const ContactosCompanion({
    this.id = const Value.absent(),
    this.contacto = const Value.absent(),
    this.NomeDoResponsavel = const Value.absent(),
    this.benificiarioId = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  ContactosCompanion.insert({
    this.id = const Value.absent(),
    @required String contacto,
    @required String NomeDoResponsavel,
    this.benificiarioId = const Value.absent(),
    @required DateTime createdAt,
    @required DateTime updatedAt,
  })  : contacto = Value(contacto),
        NomeDoResponsavel = Value(NomeDoResponsavel),
        createdAt = Value(createdAt),
        updatedAt = Value(updatedAt);
  static Insertable<Contacto> custom({
    Expression<int> id,
    Expression<String> contacto,
    Expression<String> NomeDoResponsavel,
    Expression<int> benificiarioId,
    Expression<DateTime> createdAt,
    Expression<DateTime> updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (contacto != null) 'contacto': contacto,
      if (NomeDoResponsavel != null) 'nome_do_responsavel': NomeDoResponsavel,
      if (benificiarioId != null) 'benificiario_id': benificiarioId,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  ContactosCompanion copyWith(
      {Value<int> id,
      Value<String> contacto,
      Value<String> NomeDoResponsavel,
      Value<int> benificiarioId,
      Value<DateTime> createdAt,
      Value<DateTime> updatedAt}) {
    return ContactosCompanion(
      id: id ?? this.id,
      contacto: contacto ?? this.contacto,
      NomeDoResponsavel: NomeDoResponsavel ?? this.NomeDoResponsavel,
      benificiarioId: benificiarioId ?? this.benificiarioId,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (contacto.present) {
      map['contacto'] = Variable<String>(contacto.value);
    }
    if (NomeDoResponsavel.present) {
      map['nome_do_responsavel'] = Variable<String>(NomeDoResponsavel.value);
    }
    if (benificiarioId.present) {
      map['benificiario_id'] = Variable<int>(benificiarioId.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('ContactosCompanion(')
          ..write('id: $id, ')
          ..write('contacto: $contacto, ')
          ..write('NomeDoResponsavel: $NomeDoResponsavel, ')
          ..write('benificiarioId: $benificiarioId, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $ContactosTable extends Contactos
    with TableInfo<$ContactosTable, Contacto> {
  final GeneratedDatabase _db;
  final String _alias;
  $ContactosTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _contactoMeta = const VerificationMeta('contacto');
  GeneratedTextColumn _contacto;
  @override
  GeneratedTextColumn get contacto => _contacto ??= _constructContacto();
  GeneratedTextColumn _constructContacto() {
    return GeneratedTextColumn('contacto', $tableName, false,
        maxTextLength: 25);
  }

  final VerificationMeta _NomeDoResponsavelMeta =
      const VerificationMeta('NomeDoResponsavel');
  GeneratedTextColumn _NomeDoResponsavel;
  @override
  GeneratedTextColumn get NomeDoResponsavel =>
      _NomeDoResponsavel ??= _constructNomeDoResponsavel();
  GeneratedTextColumn _constructNomeDoResponsavel() {
    return GeneratedTextColumn('nome_do_responsavel', $tableName, false,
        maxTextLength: 191);
  }

  final VerificationMeta _benificiarioIdMeta =
      const VerificationMeta('benificiarioId');
  GeneratedIntColumn _benificiarioId;
  @override
  GeneratedIntColumn get benificiarioId =>
      _benificiarioId ??= _constructBenificiarioId();
  GeneratedIntColumn _constructBenificiarioId() {
    return GeneratedIntColumn('benificiario_id', $tableName, true,
        $customConstraints: 'NULL REFERENCES benificiarios(id)');
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedDateTimeColumn _updatedAt;
  @override
  GeneratedDateTimeColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedDateTimeColumn _constructUpdatedAt() {
    return GeneratedDateTimeColumn(
      'updated_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns =>
      [id, contacto, NomeDoResponsavel, benificiarioId, createdAt, updatedAt];
  @override
  $ContactosTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'contactos';
  @override
  final String actualTableName = 'contactos';
  @override
  VerificationContext validateIntegrity(Insertable<Contacto> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('contacto')) {
      context.handle(_contactoMeta,
          contacto.isAcceptableOrUnknown(data['contacto'], _contactoMeta));
    } else if (isInserting) {
      context.missing(_contactoMeta);
    }
    if (data.containsKey('nome_do_responsavel')) {
      context.handle(
          _NomeDoResponsavelMeta,
          NomeDoResponsavel.isAcceptableOrUnknown(
              data['nome_do_responsavel'], _NomeDoResponsavelMeta));
    } else if (isInserting) {
      context.missing(_NomeDoResponsavelMeta);
    }
    if (data.containsKey('benificiario_id')) {
      context.handle(
          _benificiarioIdMeta,
          benificiarioId.isAcceptableOrUnknown(
              data['benificiario_id'], _benificiarioIdMeta));
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at'], _updatedAtMeta));
    } else if (isInserting) {
      context.missing(_updatedAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Contacto map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Contacto.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $ContactosTable createAlias(String alias) {
    return $ContactosTable(_db, alias);
  }
}

class BenificiarioEncaminhadoDocumento extends DataClass
    implements Insertable<BenificiarioEncaminhadoDocumento> {
  final int id;
  final int benificiarioEncaminhadoId;
  final int documentoId;
  final String especificar;
  final DateTime createdAt;
  final DateTime updatedAt;
  BenificiarioEncaminhadoDocumento(
      {@required this.id,
      this.benificiarioEncaminhadoId,
      this.documentoId,
      @required this.especificar,
      @required this.createdAt,
      @required this.updatedAt});
  factory BenificiarioEncaminhadoDocumento.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return BenificiarioEncaminhadoDocumento(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      benificiarioEncaminhadoId: intType.mapFromDatabaseResponse(
          data['${effectivePrefix}benificiario_encaminhado_id']),
      documentoId: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}documento_id']),
      especificar: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}especificar']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      updatedAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || benificiarioEncaminhadoId != null) {
      map['benificiario_encaminhado_id'] =
          Variable<int>(benificiarioEncaminhadoId);
    }
    if (!nullToAbsent || documentoId != null) {
      map['documento_id'] = Variable<int>(documentoId);
    }
    if (!nullToAbsent || especificar != null) {
      map['especificar'] = Variable<String>(especificar);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<DateTime>(createdAt);
    }
    if (!nullToAbsent || updatedAt != null) {
      map['updated_at'] = Variable<DateTime>(updatedAt);
    }
    return map;
  }

  BenificiarioEncaminhadoDocumentosCompanion toCompanion(bool nullToAbsent) {
    return BenificiarioEncaminhadoDocumentosCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      benificiarioEncaminhadoId:
          benificiarioEncaminhadoId == null && nullToAbsent
              ? const Value.absent()
              : Value(benificiarioEncaminhadoId),
      documentoId: documentoId == null && nullToAbsent
          ? const Value.absent()
          : Value(documentoId),
      especificar: especificar == null && nullToAbsent
          ? const Value.absent()
          : Value(especificar),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  factory BenificiarioEncaminhadoDocumento.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return BenificiarioEncaminhadoDocumento(
      id: serializer.fromJson<int>(json['id']),
      benificiarioEncaminhadoId:
          serializer.fromJson<int>(json['benificiarioEncaminhadoId']),
      documentoId: serializer.fromJson<int>(json['documentoId']),
      especificar: serializer.fromJson<String>(json['especificar']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'benificiarioEncaminhadoId':
          serializer.toJson<int>(benificiarioEncaminhadoId),
      'documentoId': serializer.toJson<int>(documentoId),
      'especificar': serializer.toJson<String>(especificar),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

  BenificiarioEncaminhadoDocumento copyWith(
          {int id,
          int benificiarioEncaminhadoId,
          int documentoId,
          String especificar,
          DateTime createdAt,
          DateTime updatedAt}) =>
      BenificiarioEncaminhadoDocumento(
        id: id ?? this.id,
        benificiarioEncaminhadoId:
            benificiarioEncaminhadoId ?? this.benificiarioEncaminhadoId,
        documentoId: documentoId ?? this.documentoId,
        especificar: especificar ?? this.especificar,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('BenificiarioEncaminhadoDocumento(')
          ..write('id: $id, ')
          ..write('benificiarioEncaminhadoId: $benificiarioEncaminhadoId, ')
          ..write('documentoId: $documentoId, ')
          ..write('especificar: $especificar, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          benificiarioEncaminhadoId.hashCode,
          $mrjc(
              documentoId.hashCode,
              $mrjc(especificar.hashCode,
                  $mrjc(createdAt.hashCode, updatedAt.hashCode))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is BenificiarioEncaminhadoDocumento &&
          other.id == this.id &&
          other.benificiarioEncaminhadoId == this.benificiarioEncaminhadoId &&
          other.documentoId == this.documentoId &&
          other.especificar == this.especificar &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class BenificiarioEncaminhadoDocumentosCompanion
    extends UpdateCompanion<BenificiarioEncaminhadoDocumento> {
  final Value<int> id;
  final Value<int> benificiarioEncaminhadoId;
  final Value<int> documentoId;
  final Value<String> especificar;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  const BenificiarioEncaminhadoDocumentosCompanion({
    this.id = const Value.absent(),
    this.benificiarioEncaminhadoId = const Value.absent(),
    this.documentoId = const Value.absent(),
    this.especificar = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  BenificiarioEncaminhadoDocumentosCompanion.insert({
    this.id = const Value.absent(),
    this.benificiarioEncaminhadoId = const Value.absent(),
    this.documentoId = const Value.absent(),
    @required String especificar,
    @required DateTime createdAt,
    @required DateTime updatedAt,
  })  : especificar = Value(especificar),
        createdAt = Value(createdAt),
        updatedAt = Value(updatedAt);
  static Insertable<BenificiarioEncaminhadoDocumento> custom({
    Expression<int> id,
    Expression<int> benificiarioEncaminhadoId,
    Expression<int> documentoId,
    Expression<String> especificar,
    Expression<DateTime> createdAt,
    Expression<DateTime> updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (benificiarioEncaminhadoId != null)
        'benificiario_encaminhado_id': benificiarioEncaminhadoId,
      if (documentoId != null) 'documento_id': documentoId,
      if (especificar != null) 'especificar': especificar,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  BenificiarioEncaminhadoDocumentosCompanion copyWith(
      {Value<int> id,
      Value<int> benificiarioEncaminhadoId,
      Value<int> documentoId,
      Value<String> especificar,
      Value<DateTime> createdAt,
      Value<DateTime> updatedAt}) {
    return BenificiarioEncaminhadoDocumentosCompanion(
      id: id ?? this.id,
      benificiarioEncaminhadoId:
          benificiarioEncaminhadoId ?? this.benificiarioEncaminhadoId,
      documentoId: documentoId ?? this.documentoId,
      especificar: especificar ?? this.especificar,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (benificiarioEncaminhadoId.present) {
      map['benificiario_encaminhado_id'] =
          Variable<int>(benificiarioEncaminhadoId.value);
    }
    if (documentoId.present) {
      map['documento_id'] = Variable<int>(documentoId.value);
    }
    if (especificar.present) {
      map['especificar'] = Variable<String>(especificar.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('BenificiarioEncaminhadoDocumentosCompanion(')
          ..write('id: $id, ')
          ..write('benificiarioEncaminhadoId: $benificiarioEncaminhadoId, ')
          ..write('documentoId: $documentoId, ')
          ..write('especificar: $especificar, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $BenificiarioEncaminhadoDocumentosTable
    extends BenificiarioEncaminhadoDocumentos
    with
        TableInfo<$BenificiarioEncaminhadoDocumentosTable,
            BenificiarioEncaminhadoDocumento> {
  final GeneratedDatabase _db;
  final String _alias;
  $BenificiarioEncaminhadoDocumentosTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _benificiarioEncaminhadoIdMeta =
      const VerificationMeta('benificiarioEncaminhadoId');
  GeneratedIntColumn _benificiarioEncaminhadoId;
  @override
  GeneratedIntColumn get benificiarioEncaminhadoId =>
      _benificiarioEncaminhadoId ??= _constructBenificiarioEncaminhadoId();
  GeneratedIntColumn _constructBenificiarioEncaminhadoId() {
    return GeneratedIntColumn('benificiario_encaminhado_id', $tableName, true,
        $customConstraints: 'NULL REFERENCES benificiario_encaminhados(id)');
  }

  final VerificationMeta _documentoIdMeta =
      const VerificationMeta('documentoId');
  GeneratedIntColumn _documentoId;
  @override
  GeneratedIntColumn get documentoId =>
      _documentoId ??= _constructDocumentoId();
  GeneratedIntColumn _constructDocumentoId() {
    return GeneratedIntColumn('documento_id', $tableName, true,
        $customConstraints: 'NULL REFERENCES documentos(id)');
  }

  final VerificationMeta _especificarMeta =
      const VerificationMeta('especificar');
  GeneratedTextColumn _especificar;
  @override
  GeneratedTextColumn get especificar =>
      _especificar ??= _constructEspecificar();
  GeneratedTextColumn _constructEspecificar() {
    return GeneratedTextColumn('especificar', $tableName, false,
        maxTextLength: 191);
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedDateTimeColumn _updatedAt;
  @override
  GeneratedDateTimeColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedDateTimeColumn _constructUpdatedAt() {
    return GeneratedDateTimeColumn(
      'updated_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [
        id,
        benificiarioEncaminhadoId,
        documentoId,
        especificar,
        createdAt,
        updatedAt
      ];
  @override
  $BenificiarioEncaminhadoDocumentosTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'benificiario_encaminhado_documentos';
  @override
  final String actualTableName = 'benificiario_encaminhado_documentos';
  @override
  VerificationContext validateIntegrity(
      Insertable<BenificiarioEncaminhadoDocumento> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('benificiario_encaminhado_id')) {
      context.handle(
          _benificiarioEncaminhadoIdMeta,
          benificiarioEncaminhadoId.isAcceptableOrUnknown(
              data['benificiario_encaminhado_id'],
              _benificiarioEncaminhadoIdMeta));
    }
    if (data.containsKey('documento_id')) {
      context.handle(
          _documentoIdMeta,
          documentoId.isAcceptableOrUnknown(
              data['documento_id'], _documentoIdMeta));
    }
    if (data.containsKey('especificar')) {
      context.handle(
          _especificarMeta,
          especificar.isAcceptableOrUnknown(
              data['especificar'], _especificarMeta));
    } else if (isInserting) {
      context.missing(_especificarMeta);
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at'], _updatedAtMeta));
    } else if (isInserting) {
      context.missing(_updatedAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  BenificiarioEncaminhadoDocumento map(Map<String, dynamic> data,
      {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return BenificiarioEncaminhadoDocumento.fromData(data, _db,
        prefix: effectivePrefix);
  }

  @override
  $BenificiarioEncaminhadoDocumentosTable createAlias(String alias) {
    return $BenificiarioEncaminhadoDocumentosTable(_db, alias);
  }
}

class BenificiarioEncaminhadoServicoEncaminhado extends DataClass
    implements Insertable<BenificiarioEncaminhadoServicoEncaminhado> {
  final int id;
  final int benificiarioEncaminhadoId;
  final int servicoEncaminhadoId;
  final String especificar;
  final DateTime createdAt;
  final DateTime updatedAt;
  BenificiarioEncaminhadoServicoEncaminhado(
      {@required this.id,
      this.benificiarioEncaminhadoId,
      this.servicoEncaminhadoId,
      @required this.especificar,
      @required this.createdAt,
      @required this.updatedAt});
  factory BenificiarioEncaminhadoServicoEncaminhado.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return BenificiarioEncaminhadoServicoEncaminhado(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      benificiarioEncaminhadoId: intType.mapFromDatabaseResponse(
          data['${effectivePrefix}benificiario_encaminhado_id']),
      servicoEncaminhadoId: intType.mapFromDatabaseResponse(
          data['${effectivePrefix}servico_encaminhado_id']),
      especificar: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}especificar']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      updatedAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || benificiarioEncaminhadoId != null) {
      map['benificiario_encaminhado_id'] =
          Variable<int>(benificiarioEncaminhadoId);
    }
    if (!nullToAbsent || servicoEncaminhadoId != null) {
      map['servico_encaminhado_id'] = Variable<int>(servicoEncaminhadoId);
    }
    if (!nullToAbsent || especificar != null) {
      map['especificar'] = Variable<String>(especificar);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<DateTime>(createdAt);
    }
    if (!nullToAbsent || updatedAt != null) {
      map['updated_at'] = Variable<DateTime>(updatedAt);
    }
    return map;
  }

  BenificiarioEncaminhadoServicoEncaminhadosCompanion toCompanion(
      bool nullToAbsent) {
    return BenificiarioEncaminhadoServicoEncaminhadosCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      benificiarioEncaminhadoId:
          benificiarioEncaminhadoId == null && nullToAbsent
              ? const Value.absent()
              : Value(benificiarioEncaminhadoId),
      servicoEncaminhadoId: servicoEncaminhadoId == null && nullToAbsent
          ? const Value.absent()
          : Value(servicoEncaminhadoId),
      especificar: especificar == null && nullToAbsent
          ? const Value.absent()
          : Value(especificar),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  factory BenificiarioEncaminhadoServicoEncaminhado.fromJson(
      Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return BenificiarioEncaminhadoServicoEncaminhado(
      id: serializer.fromJson<int>(json['id']),
      benificiarioEncaminhadoId:
          serializer.fromJson<int>(json['benificiarioEncaminhadoId']),
      servicoEncaminhadoId:
          serializer.fromJson<int>(json['servicoEncaminhadoId']),
      especificar: serializer.fromJson<String>(json['especificar']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'benificiarioEncaminhadoId':
          serializer.toJson<int>(benificiarioEncaminhadoId),
      'servicoEncaminhadoId': serializer.toJson<int>(servicoEncaminhadoId),
      'especificar': serializer.toJson<String>(especificar),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

  BenificiarioEncaminhadoServicoEncaminhado copyWith(
          {int id,
          int benificiarioEncaminhadoId,
          int servicoEncaminhadoId,
          String especificar,
          DateTime createdAt,
          DateTime updatedAt}) =>
      BenificiarioEncaminhadoServicoEncaminhado(
        id: id ?? this.id,
        benificiarioEncaminhadoId:
            benificiarioEncaminhadoId ?? this.benificiarioEncaminhadoId,
        servicoEncaminhadoId: servicoEncaminhadoId ?? this.servicoEncaminhadoId,
        especificar: especificar ?? this.especificar,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('BenificiarioEncaminhadoServicoEncaminhado(')
          ..write('id: $id, ')
          ..write('benificiarioEncaminhadoId: $benificiarioEncaminhadoId, ')
          ..write('servicoEncaminhadoId: $servicoEncaminhadoId, ')
          ..write('especificar: $especificar, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          benificiarioEncaminhadoId.hashCode,
          $mrjc(
              servicoEncaminhadoId.hashCode,
              $mrjc(especificar.hashCode,
                  $mrjc(createdAt.hashCode, updatedAt.hashCode))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is BenificiarioEncaminhadoServicoEncaminhado &&
          other.id == this.id &&
          other.benificiarioEncaminhadoId == this.benificiarioEncaminhadoId &&
          other.servicoEncaminhadoId == this.servicoEncaminhadoId &&
          other.especificar == this.especificar &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class BenificiarioEncaminhadoServicoEncaminhadosCompanion
    extends UpdateCompanion<BenificiarioEncaminhadoServicoEncaminhado> {
  final Value<int> id;
  final Value<int> benificiarioEncaminhadoId;
  final Value<int> servicoEncaminhadoId;
  final Value<String> especificar;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  const BenificiarioEncaminhadoServicoEncaminhadosCompanion({
    this.id = const Value.absent(),
    this.benificiarioEncaminhadoId = const Value.absent(),
    this.servicoEncaminhadoId = const Value.absent(),
    this.especificar = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  BenificiarioEncaminhadoServicoEncaminhadosCompanion.insert({
    this.id = const Value.absent(),
    this.benificiarioEncaminhadoId = const Value.absent(),
    this.servicoEncaminhadoId = const Value.absent(),
    @required String especificar,
    @required DateTime createdAt,
    @required DateTime updatedAt,
  })  : especificar = Value(especificar),
        createdAt = Value(createdAt),
        updatedAt = Value(updatedAt);
  static Insertable<BenificiarioEncaminhadoServicoEncaminhado> custom({
    Expression<int> id,
    Expression<int> benificiarioEncaminhadoId,
    Expression<int> servicoEncaminhadoId,
    Expression<String> especificar,
    Expression<DateTime> createdAt,
    Expression<DateTime> updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (benificiarioEncaminhadoId != null)
        'benificiario_encaminhado_id': benificiarioEncaminhadoId,
      if (servicoEncaminhadoId != null)
        'servico_encaminhado_id': servicoEncaminhadoId,
      if (especificar != null) 'especificar': especificar,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  BenificiarioEncaminhadoServicoEncaminhadosCompanion copyWith(
      {Value<int> id,
      Value<int> benificiarioEncaminhadoId,
      Value<int> servicoEncaminhadoId,
      Value<String> especificar,
      Value<DateTime> createdAt,
      Value<DateTime> updatedAt}) {
    return BenificiarioEncaminhadoServicoEncaminhadosCompanion(
      id: id ?? this.id,
      benificiarioEncaminhadoId:
          benificiarioEncaminhadoId ?? this.benificiarioEncaminhadoId,
      servicoEncaminhadoId: servicoEncaminhadoId ?? this.servicoEncaminhadoId,
      especificar: especificar ?? this.especificar,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (benificiarioEncaminhadoId.present) {
      map['benificiario_encaminhado_id'] =
          Variable<int>(benificiarioEncaminhadoId.value);
    }
    if (servicoEncaminhadoId.present) {
      map['servico_encaminhado_id'] = Variable<int>(servicoEncaminhadoId.value);
    }
    if (especificar.present) {
      map['especificar'] = Variable<String>(especificar.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('BenificiarioEncaminhadoServicoEncaminhadosCompanion(')
          ..write('id: $id, ')
          ..write('benificiarioEncaminhadoId: $benificiarioEncaminhadoId, ')
          ..write('servicoEncaminhadoId: $servicoEncaminhadoId, ')
          ..write('especificar: $especificar, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $BenificiarioEncaminhadoServicoEncaminhadosTable
    extends BenificiarioEncaminhadoServicoEncaminhados
    with
        TableInfo<$BenificiarioEncaminhadoServicoEncaminhadosTable,
            BenificiarioEncaminhadoServicoEncaminhado> {
  final GeneratedDatabase _db;
  final String _alias;
  $BenificiarioEncaminhadoServicoEncaminhadosTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _benificiarioEncaminhadoIdMeta =
      const VerificationMeta('benificiarioEncaminhadoId');
  GeneratedIntColumn _benificiarioEncaminhadoId;
  @override
  GeneratedIntColumn get benificiarioEncaminhadoId =>
      _benificiarioEncaminhadoId ??= _constructBenificiarioEncaminhadoId();
  GeneratedIntColumn _constructBenificiarioEncaminhadoId() {
    return GeneratedIntColumn('benificiario_encaminhado_id', $tableName, true,
        $customConstraints: 'NULL REFERENCES benificiario_encaminhados(id)');
  }

  final VerificationMeta _servicoEncaminhadoIdMeta =
      const VerificationMeta('servicoEncaminhadoId');
  GeneratedIntColumn _servicoEncaminhadoId;
  @override
  GeneratedIntColumn get servicoEncaminhadoId =>
      _servicoEncaminhadoId ??= _constructServicoEncaminhadoId();
  GeneratedIntColumn _constructServicoEncaminhadoId() {
    return GeneratedIntColumn('servico_encaminhado_id', $tableName, true,
        $customConstraints: 'NULL REFERENCES servico_encaminhados(id)');
  }

  final VerificationMeta _especificarMeta =
      const VerificationMeta('especificar');
  GeneratedTextColumn _especificar;
  @override
  GeneratedTextColumn get especificar =>
      _especificar ??= _constructEspecificar();
  GeneratedTextColumn _constructEspecificar() {
    return GeneratedTextColumn('especificar', $tableName, false,
        maxTextLength: 191);
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedDateTimeColumn _updatedAt;
  @override
  GeneratedDateTimeColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedDateTimeColumn _constructUpdatedAt() {
    return GeneratedDateTimeColumn(
      'updated_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [
        id,
        benificiarioEncaminhadoId,
        servicoEncaminhadoId,
        especificar,
        createdAt,
        updatedAt
      ];
  @override
  $BenificiarioEncaminhadoServicoEncaminhadosTable get asDslTable => this;
  @override
  String get $tableName =>
      _alias ?? 'benificiario_encaminhado_servico_encaminhados';
  @override
  final String actualTableName =
      'benificiario_encaminhado_servico_encaminhados';
  @override
  VerificationContext validateIntegrity(
      Insertable<BenificiarioEncaminhadoServicoEncaminhado> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('benificiario_encaminhado_id')) {
      context.handle(
          _benificiarioEncaminhadoIdMeta,
          benificiarioEncaminhadoId.isAcceptableOrUnknown(
              data['benificiario_encaminhado_id'],
              _benificiarioEncaminhadoIdMeta));
    }
    if (data.containsKey('servico_encaminhado_id')) {
      context.handle(
          _servicoEncaminhadoIdMeta,
          servicoEncaminhadoId.isAcceptableOrUnknown(
              data['servico_encaminhado_id'], _servicoEncaminhadoIdMeta));
    }
    if (data.containsKey('especificar')) {
      context.handle(
          _especificarMeta,
          especificar.isAcceptableOrUnknown(
              data['especificar'], _especificarMeta));
    } else if (isInserting) {
      context.missing(_especificarMeta);
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at'], _updatedAtMeta));
    } else if (isInserting) {
      context.missing(_updatedAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  BenificiarioEncaminhadoServicoEncaminhado map(Map<String, dynamic> data,
      {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return BenificiarioEncaminhadoServicoEncaminhado.fromData(data, _db,
        prefix: effectivePrefix);
  }

  @override
  $BenificiarioEncaminhadoServicoEncaminhadosTable createAlias(String alias) {
    return $BenificiarioEncaminhadoServicoEncaminhadosTable(_db, alias);
  }
}

class Provincia extends DataClass implements Insertable<Provincia> {
  final int id;
  final String nome;
  final DateTime createdAt;
  final DateTime updatedAt;
  Provincia(
      {@required this.id,
      @required this.nome,
      @required this.createdAt,
      @required this.updatedAt});
  factory Provincia.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return Provincia(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      nome: stringType.mapFromDatabaseResponse(data['${effectivePrefix}nome']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      updatedAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || nome != null) {
      map['nome'] = Variable<String>(nome);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<DateTime>(createdAt);
    }
    if (!nullToAbsent || updatedAt != null) {
      map['updated_at'] = Variable<DateTime>(updatedAt);
    }
    return map;
  }

  ProvinciasCompanion toCompanion(bool nullToAbsent) {
    return ProvinciasCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      nome: nome == null && nullToAbsent ? const Value.absent() : Value(nome),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  factory Provincia.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Provincia(
      id: serializer.fromJson<int>(json['id']),
      nome: serializer.fromJson<String>(json['nome']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'nome': serializer.toJson<String>(nome),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

  Provincia copyWith(
          {int id, String nome, DateTime createdAt, DateTime updatedAt}) =>
      Provincia(
        id: id ?? this.id,
        nome: nome ?? this.nome,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('Provincia(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(id.hashCode,
      $mrjc(nome.hashCode, $mrjc(createdAt.hashCode, updatedAt.hashCode))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Provincia &&
          other.id == this.id &&
          other.nome == this.nome &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class ProvinciasCompanion extends UpdateCompanion<Provincia> {
  final Value<int> id;
  final Value<String> nome;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  const ProvinciasCompanion({
    this.id = const Value.absent(),
    this.nome = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  ProvinciasCompanion.insert({
    this.id = const Value.absent(),
    @required String nome,
    @required DateTime createdAt,
    @required DateTime updatedAt,
  })  : nome = Value(nome),
        createdAt = Value(createdAt),
        updatedAt = Value(updatedAt);
  static Insertable<Provincia> custom({
    Expression<int> id,
    Expression<String> nome,
    Expression<DateTime> createdAt,
    Expression<DateTime> updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (nome != null) 'nome': nome,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  ProvinciasCompanion copyWith(
      {Value<int> id,
      Value<String> nome,
      Value<DateTime> createdAt,
      Value<DateTime> updatedAt}) {
    return ProvinciasCompanion(
      id: id ?? this.id,
      nome: nome ?? this.nome,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (nome.present) {
      map['nome'] = Variable<String>(nome.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('ProvinciasCompanion(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $ProvinciasTable extends Provincias
    with TableInfo<$ProvinciasTable, Provincia> {
  final GeneratedDatabase _db;
  final String _alias;
  $ProvinciasTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _nomeMeta = const VerificationMeta('nome');
  GeneratedTextColumn _nome;
  @override
  GeneratedTextColumn get nome => _nome ??= _constructNome();
  GeneratedTextColumn _constructNome() {
    return GeneratedTextColumn('nome', $tableName, false, maxTextLength: 191);
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedDateTimeColumn _updatedAt;
  @override
  GeneratedDateTimeColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedDateTimeColumn _constructUpdatedAt() {
    return GeneratedDateTimeColumn(
      'updated_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, nome, createdAt, updatedAt];
  @override
  $ProvinciasTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'provincias';
  @override
  final String actualTableName = 'provincias';
  @override
  VerificationContext validateIntegrity(Insertable<Provincia> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('nome')) {
      context.handle(
          _nomeMeta, nome.isAcceptableOrUnknown(data['nome'], _nomeMeta));
    } else if (isInserting) {
      context.missing(_nomeMeta);
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at'], _updatedAtMeta));
    } else if (isInserting) {
      context.missing(_updatedAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Provincia map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Provincia.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $ProvinciasTable createAlias(String alias) {
    return $ProvinciasTable(_db, alias);
  }
}

class Sexo extends DataClass implements Insertable<Sexo> {
  final int id;
  final String nome;
  final DateTime createdAt;
  final DateTime updatedAt;
  Sexo(
      {@required this.id,
      @required this.nome,
      @required this.createdAt,
      @required this.updatedAt});
  factory Sexo.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return Sexo(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      nome: stringType.mapFromDatabaseResponse(data['${effectivePrefix}nome']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      updatedAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || nome != null) {
      map['nome'] = Variable<String>(nome);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<DateTime>(createdAt);
    }
    if (!nullToAbsent || updatedAt != null) {
      map['updated_at'] = Variable<DateTime>(updatedAt);
    }
    return map;
  }

  SexosCompanion toCompanion(bool nullToAbsent) {
    return SexosCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      nome: nome == null && nullToAbsent ? const Value.absent() : Value(nome),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  factory Sexo.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Sexo(
      id: serializer.fromJson<int>(json['id']),
      nome: serializer.fromJson<String>(json['nome']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'nome': serializer.toJson<String>(nome),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

  Sexo copyWith(
          {int id, String nome, DateTime createdAt, DateTime updatedAt}) =>
      Sexo(
        id: id ?? this.id,
        nome: nome ?? this.nome,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('Sexo(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(id.hashCode,
      $mrjc(nome.hashCode, $mrjc(createdAt.hashCode, updatedAt.hashCode))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Sexo &&
          other.id == this.id &&
          other.nome == this.nome &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class SexosCompanion extends UpdateCompanion<Sexo> {
  final Value<int> id;
  final Value<String> nome;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  const SexosCompanion({
    this.id = const Value.absent(),
    this.nome = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  SexosCompanion.insert({
    this.id = const Value.absent(),
    @required String nome,
    @required DateTime createdAt,
    @required DateTime updatedAt,
  })  : nome = Value(nome),
        createdAt = Value(createdAt),
        updatedAt = Value(updatedAt);
  static Insertable<Sexo> custom({
    Expression<int> id,
    Expression<String> nome,
    Expression<DateTime> createdAt,
    Expression<DateTime> updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (nome != null) 'nome': nome,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  SexosCompanion copyWith(
      {Value<int> id,
      Value<String> nome,
      Value<DateTime> createdAt,
      Value<DateTime> updatedAt}) {
    return SexosCompanion(
      id: id ?? this.id,
      nome: nome ?? this.nome,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (nome.present) {
      map['nome'] = Variable<String>(nome.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('SexosCompanion(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $SexosTable extends Sexos with TableInfo<$SexosTable, Sexo> {
  final GeneratedDatabase _db;
  final String _alias;
  $SexosTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _nomeMeta = const VerificationMeta('nome');
  GeneratedTextColumn _nome;
  @override
  GeneratedTextColumn get nome => _nome ??= _constructNome();
  GeneratedTextColumn _constructNome() {
    return GeneratedTextColumn('nome', $tableName, false, maxTextLength: 191);
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedDateTimeColumn _updatedAt;
  @override
  GeneratedDateTimeColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedDateTimeColumn _constructUpdatedAt() {
    return GeneratedDateTimeColumn(
      'updated_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, nome, createdAt, updatedAt];
  @override
  $SexosTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'sexos';
  @override
  final String actualTableName = 'sexos';
  @override
  VerificationContext validateIntegrity(Insertable<Sexo> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('nome')) {
      context.handle(
          _nomeMeta, nome.isAcceptableOrUnknown(data['nome'], _nomeMeta));
    } else if (isInserting) {
      context.missing(_nomeMeta);
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at'], _updatedAtMeta));
    } else if (isInserting) {
      context.missing(_updatedAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Sexo map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Sexo.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $SexosTable createAlias(String alias) {
    return $SexosTable(_db, alias);
  }
}

class ObjectivoDaVisita extends DataClass
    implements Insertable<ObjectivoDaVisita> {
  final int id;
  final String nome;
  final DateTime createdAt;
  final DateTime updatedAt;
  ObjectivoDaVisita(
      {@required this.id,
      @required this.nome,
      @required this.createdAt,
      @required this.updatedAt});
  factory ObjectivoDaVisita.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return ObjectivoDaVisita(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      nome: stringType.mapFromDatabaseResponse(data['${effectivePrefix}nome']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      updatedAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || nome != null) {
      map['nome'] = Variable<String>(nome);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<DateTime>(createdAt);
    }
    if (!nullToAbsent || updatedAt != null) {
      map['updated_at'] = Variable<DateTime>(updatedAt);
    }
    return map;
  }

  ObjectivoDaVisitasCompanion toCompanion(bool nullToAbsent) {
    return ObjectivoDaVisitasCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      nome: nome == null && nullToAbsent ? const Value.absent() : Value(nome),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  factory ObjectivoDaVisita.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return ObjectivoDaVisita(
      id: serializer.fromJson<int>(json['id']),
      nome: serializer.fromJson<String>(json['nome']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'nome': serializer.toJson<String>(nome),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

  ObjectivoDaVisita copyWith(
          {int id, String nome, DateTime createdAt, DateTime updatedAt}) =>
      ObjectivoDaVisita(
        id: id ?? this.id,
        nome: nome ?? this.nome,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('ObjectivoDaVisita(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(id.hashCode,
      $mrjc(nome.hashCode, $mrjc(createdAt.hashCode, updatedAt.hashCode))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is ObjectivoDaVisita &&
          other.id == this.id &&
          other.nome == this.nome &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class ObjectivoDaVisitasCompanion extends UpdateCompanion<ObjectivoDaVisita> {
  final Value<int> id;
  final Value<String> nome;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  const ObjectivoDaVisitasCompanion({
    this.id = const Value.absent(),
    this.nome = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  ObjectivoDaVisitasCompanion.insert({
    this.id = const Value.absent(),
    @required String nome,
    @required DateTime createdAt,
    @required DateTime updatedAt,
  })  : nome = Value(nome),
        createdAt = Value(createdAt),
        updatedAt = Value(updatedAt);
  static Insertable<ObjectivoDaVisita> custom({
    Expression<int> id,
    Expression<String> nome,
    Expression<DateTime> createdAt,
    Expression<DateTime> updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (nome != null) 'nome': nome,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  ObjectivoDaVisitasCompanion copyWith(
      {Value<int> id,
      Value<String> nome,
      Value<DateTime> createdAt,
      Value<DateTime> updatedAt}) {
    return ObjectivoDaVisitasCompanion(
      id: id ?? this.id,
      nome: nome ?? this.nome,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (nome.present) {
      map['nome'] = Variable<String>(nome.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('ObjectivoDaVisitasCompanion(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $ObjectivoDaVisitasTable extends ObjectivoDaVisitas
    with TableInfo<$ObjectivoDaVisitasTable, ObjectivoDaVisita> {
  final GeneratedDatabase _db;
  final String _alias;
  $ObjectivoDaVisitasTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _nomeMeta = const VerificationMeta('nome');
  GeneratedTextColumn _nome;
  @override
  GeneratedTextColumn get nome => _nome ??= _constructNome();
  GeneratedTextColumn _constructNome() {
    return GeneratedTextColumn('nome', $tableName, false, maxTextLength: 191);
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedDateTimeColumn _updatedAt;
  @override
  GeneratedDateTimeColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedDateTimeColumn _constructUpdatedAt() {
    return GeneratedDateTimeColumn(
      'updated_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, nome, createdAt, updatedAt];
  @override
  $ObjectivoDaVisitasTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'objectivo_da_visitas';
  @override
  final String actualTableName = 'objectivo_da_visitas';
  @override
  VerificationContext validateIntegrity(Insertable<ObjectivoDaVisita> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('nome')) {
      context.handle(
          _nomeMeta, nome.isAcceptableOrUnknown(data['nome'], _nomeMeta));
    } else if (isInserting) {
      context.missing(_nomeMeta);
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at'], _updatedAtMeta));
    } else if (isInserting) {
      context.missing(_updatedAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  ObjectivoDaVisita map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return ObjectivoDaVisita.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $ObjectivoDaVisitasTable createAlias(String alias) {
    return $ObjectivoDaVisitasTable(_db, alias);
  }
}

class ServicoEncaminhado extends DataClass
    implements Insertable<ServicoEncaminhado> {
  final int id;
  final String nome;
  final DateTime createdAt;
  final DateTime updatedAt;
  ServicoEncaminhado(
      {@required this.id,
      @required this.nome,
      @required this.createdAt,
      @required this.updatedAt});
  factory ServicoEncaminhado.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return ServicoEncaminhado(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      nome: stringType.mapFromDatabaseResponse(data['${effectivePrefix}nome']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      updatedAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || nome != null) {
      map['nome'] = Variable<String>(nome);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<DateTime>(createdAt);
    }
    if (!nullToAbsent || updatedAt != null) {
      map['updated_at'] = Variable<DateTime>(updatedAt);
    }
    return map;
  }

  ServicoEncaminhadosCompanion toCompanion(bool nullToAbsent) {
    return ServicoEncaminhadosCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      nome: nome == null && nullToAbsent ? const Value.absent() : Value(nome),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  factory ServicoEncaminhado.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return ServicoEncaminhado(
      id: serializer.fromJson<int>(json['id']),
      nome: serializer.fromJson<String>(json['nome']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'nome': serializer.toJson<String>(nome),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

  ServicoEncaminhado copyWith(
          {int id, String nome, DateTime createdAt, DateTime updatedAt}) =>
      ServicoEncaminhado(
        id: id ?? this.id,
        nome: nome ?? this.nome,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('ServicoEncaminhado(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(id.hashCode,
      $mrjc(nome.hashCode, $mrjc(createdAt.hashCode, updatedAt.hashCode))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is ServicoEncaminhado &&
          other.id == this.id &&
          other.nome == this.nome &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class ServicoEncaminhadosCompanion extends UpdateCompanion<ServicoEncaminhado> {
  final Value<int> id;
  final Value<String> nome;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  const ServicoEncaminhadosCompanion({
    this.id = const Value.absent(),
    this.nome = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  ServicoEncaminhadosCompanion.insert({
    this.id = const Value.absent(),
    @required String nome,
    @required DateTime createdAt,
    @required DateTime updatedAt,
  })  : nome = Value(nome),
        createdAt = Value(createdAt),
        updatedAt = Value(updatedAt);
  static Insertable<ServicoEncaminhado> custom({
    Expression<int> id,
    Expression<String> nome,
    Expression<DateTime> createdAt,
    Expression<DateTime> updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (nome != null) 'nome': nome,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  ServicoEncaminhadosCompanion copyWith(
      {Value<int> id,
      Value<String> nome,
      Value<DateTime> createdAt,
      Value<DateTime> updatedAt}) {
    return ServicoEncaminhadosCompanion(
      id: id ?? this.id,
      nome: nome ?? this.nome,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (nome.present) {
      map['nome'] = Variable<String>(nome.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('ServicoEncaminhadosCompanion(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $ServicoEncaminhadosTable extends ServicoEncaminhados
    with TableInfo<$ServicoEncaminhadosTable, ServicoEncaminhado> {
  final GeneratedDatabase _db;
  final String _alias;
  $ServicoEncaminhadosTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _nomeMeta = const VerificationMeta('nome');
  GeneratedTextColumn _nome;
  @override
  GeneratedTextColumn get nome => _nome ??= _constructNome();
  GeneratedTextColumn _constructNome() {
    return GeneratedTextColumn('nome', $tableName, false, maxTextLength: 191);
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedDateTimeColumn _updatedAt;
  @override
  GeneratedDateTimeColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedDateTimeColumn _constructUpdatedAt() {
    return GeneratedDateTimeColumn(
      'updated_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, nome, createdAt, updatedAt];
  @override
  $ServicoEncaminhadosTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'servico_encaminhados';
  @override
  final String actualTableName = 'servico_encaminhados';
  @override
  VerificationContext validateIntegrity(Insertable<ServicoEncaminhado> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('nome')) {
      context.handle(
          _nomeMeta, nome.isAcceptableOrUnknown(data['nome'], _nomeMeta));
    } else if (isInserting) {
      context.missing(_nomeMeta);
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at'], _updatedAtMeta));
    } else if (isInserting) {
      context.missing(_updatedAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  ServicoEncaminhado map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return ServicoEncaminhado.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $ServicoEncaminhadosTable createAlias(String alias) {
    return $ServicoEncaminhadosTable(_db, alias);
  }
}

class Documento extends DataClass implements Insertable<Documento> {
  final int id;
  final String nome;
  final DateTime createdAt;
  final DateTime updatedAt;
  Documento(
      {@required this.id,
      @required this.nome,
      @required this.createdAt,
      @required this.updatedAt});
  factory Documento.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return Documento(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      nome: stringType.mapFromDatabaseResponse(data['${effectivePrefix}nome']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      updatedAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || nome != null) {
      map['nome'] = Variable<String>(nome);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<DateTime>(createdAt);
    }
    if (!nullToAbsent || updatedAt != null) {
      map['updated_at'] = Variable<DateTime>(updatedAt);
    }
    return map;
  }

  DocumentosCompanion toCompanion(bool nullToAbsent) {
    return DocumentosCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      nome: nome == null && nullToAbsent ? const Value.absent() : Value(nome),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  factory Documento.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Documento(
      id: serializer.fromJson<int>(json['id']),
      nome: serializer.fromJson<String>(json['nome']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'nome': serializer.toJson<String>(nome),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

  Documento copyWith(
          {int id, String nome, DateTime createdAt, DateTime updatedAt}) =>
      Documento(
        id: id ?? this.id,
        nome: nome ?? this.nome,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('Documento(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(id.hashCode,
      $mrjc(nome.hashCode, $mrjc(createdAt.hashCode, updatedAt.hashCode))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Documento &&
          other.id == this.id &&
          other.nome == this.nome &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class DocumentosCompanion extends UpdateCompanion<Documento> {
  final Value<int> id;
  final Value<String> nome;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  const DocumentosCompanion({
    this.id = const Value.absent(),
    this.nome = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  DocumentosCompanion.insert({
    this.id = const Value.absent(),
    @required String nome,
    @required DateTime createdAt,
    @required DateTime updatedAt,
  })  : nome = Value(nome),
        createdAt = Value(createdAt),
        updatedAt = Value(updatedAt);
  static Insertable<Documento> custom({
    Expression<int> id,
    Expression<String> nome,
    Expression<DateTime> createdAt,
    Expression<DateTime> updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (nome != null) 'nome': nome,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  DocumentosCompanion copyWith(
      {Value<int> id,
      Value<String> nome,
      Value<DateTime> createdAt,
      Value<DateTime> updatedAt}) {
    return DocumentosCompanion(
      id: id ?? this.id,
      nome: nome ?? this.nome,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (nome.present) {
      map['nome'] = Variable<String>(nome.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('DocumentosCompanion(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $DocumentosTable extends Documentos
    with TableInfo<$DocumentosTable, Documento> {
  final GeneratedDatabase _db;
  final String _alias;
  $DocumentosTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _nomeMeta = const VerificationMeta('nome');
  GeneratedTextColumn _nome;
  @override
  GeneratedTextColumn get nome => _nome ??= _constructNome();
  GeneratedTextColumn _constructNome() {
    return GeneratedTextColumn('nome', $tableName, false, maxTextLength: 191);
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedDateTimeColumn _updatedAt;
  @override
  GeneratedDateTimeColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedDateTimeColumn _constructUpdatedAt() {
    return GeneratedDateTimeColumn(
      'updated_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, nome, createdAt, updatedAt];
  @override
  $DocumentosTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'documentos';
  @override
  final String actualTableName = 'documentos';
  @override
  VerificationContext validateIntegrity(Insertable<Documento> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('nome')) {
      context.handle(
          _nomeMeta, nome.isAcceptableOrUnknown(data['nome'], _nomeMeta));
    } else if (isInserting) {
      context.missing(_nomeMeta);
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at'], _updatedAtMeta));
    } else if (isInserting) {
      context.missing(_updatedAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Documento map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Documento.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $DocumentosTable createAlias(String alias) {
    return $DocumentosTable(_db, alias);
  }
}

class MotivoDeAberturaDeProcesso extends DataClass
    implements Insertable<MotivoDeAberturaDeProcesso> {
  final int id;
  final String nome;
  final DateTime createdAt;
  final DateTime updatedAt;
  MotivoDeAberturaDeProcesso(
      {@required this.id,
      @required this.nome,
      @required this.createdAt,
      @required this.updatedAt});
  factory MotivoDeAberturaDeProcesso.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return MotivoDeAberturaDeProcesso(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      nome: stringType.mapFromDatabaseResponse(data['${effectivePrefix}nome']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      updatedAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || nome != null) {
      map['nome'] = Variable<String>(nome);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<DateTime>(createdAt);
    }
    if (!nullToAbsent || updatedAt != null) {
      map['updated_at'] = Variable<DateTime>(updatedAt);
    }
    return map;
  }

  MotivoDeAberturaDeProcessosCompanion toCompanion(bool nullToAbsent) {
    return MotivoDeAberturaDeProcessosCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      nome: nome == null && nullToAbsent ? const Value.absent() : Value(nome),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  factory MotivoDeAberturaDeProcesso.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return MotivoDeAberturaDeProcesso(
      id: serializer.fromJson<int>(json['id']),
      nome: serializer.fromJson<String>(json['nome']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'nome': serializer.toJson<String>(nome),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

  MotivoDeAberturaDeProcesso copyWith(
          {int id, String nome, DateTime createdAt, DateTime updatedAt}) =>
      MotivoDeAberturaDeProcesso(
        id: id ?? this.id,
        nome: nome ?? this.nome,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('MotivoDeAberturaDeProcesso(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(id.hashCode,
      $mrjc(nome.hashCode, $mrjc(createdAt.hashCode, updatedAt.hashCode))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is MotivoDeAberturaDeProcesso &&
          other.id == this.id &&
          other.nome == this.nome &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class MotivoDeAberturaDeProcessosCompanion
    extends UpdateCompanion<MotivoDeAberturaDeProcesso> {
  final Value<int> id;
  final Value<String> nome;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  const MotivoDeAberturaDeProcessosCompanion({
    this.id = const Value.absent(),
    this.nome = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  MotivoDeAberturaDeProcessosCompanion.insert({
    this.id = const Value.absent(),
    @required String nome,
    @required DateTime createdAt,
    @required DateTime updatedAt,
  })  : nome = Value(nome),
        createdAt = Value(createdAt),
        updatedAt = Value(updatedAt);
  static Insertable<MotivoDeAberturaDeProcesso> custom({
    Expression<int> id,
    Expression<String> nome,
    Expression<DateTime> createdAt,
    Expression<DateTime> updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (nome != null) 'nome': nome,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  MotivoDeAberturaDeProcessosCompanion copyWith(
      {Value<int> id,
      Value<String> nome,
      Value<DateTime> createdAt,
      Value<DateTime> updatedAt}) {
    return MotivoDeAberturaDeProcessosCompanion(
      id: id ?? this.id,
      nome: nome ?? this.nome,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (nome.present) {
      map['nome'] = Variable<String>(nome.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('MotivoDeAberturaDeProcessosCompanion(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $MotivoDeAberturaDeProcessosTable extends MotivoDeAberturaDeProcessos
    with
        TableInfo<$MotivoDeAberturaDeProcessosTable,
            MotivoDeAberturaDeProcesso> {
  final GeneratedDatabase _db;
  final String _alias;
  $MotivoDeAberturaDeProcessosTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _nomeMeta = const VerificationMeta('nome');
  GeneratedTextColumn _nome;
  @override
  GeneratedTextColumn get nome => _nome ??= _constructNome();
  GeneratedTextColumn _constructNome() {
    return GeneratedTextColumn('nome', $tableName, false, maxTextLength: 191);
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedDateTimeColumn _updatedAt;
  @override
  GeneratedDateTimeColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedDateTimeColumn _constructUpdatedAt() {
    return GeneratedDateTimeColumn(
      'updated_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, nome, createdAt, updatedAt];
  @override
  $MotivoDeAberturaDeProcessosTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'motivo_de_abertura_de_processos';
  @override
  final String actualTableName = 'motivo_de_abertura_de_processos';
  @override
  VerificationContext validateIntegrity(
      Insertable<MotivoDeAberturaDeProcesso> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('nome')) {
      context.handle(
          _nomeMeta, nome.isAcceptableOrUnknown(data['nome'], _nomeMeta));
    } else if (isInserting) {
      context.missing(_nomeMeta);
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at'], _updatedAtMeta));
    } else if (isInserting) {
      context.missing(_updatedAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  MotivoDeAberturaDeProcesso map(Map<String, dynamic> data,
      {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return MotivoDeAberturaDeProcesso.fromData(data, _db,
        prefix: effectivePrefix);
  }

  @override
  $MotivoDeAberturaDeProcessosTable createAlias(String alias) {
    return $MotivoDeAberturaDeProcessosTable(_db, alias);
  }
}

class Benificiario extends DataClass implements Insertable<Benificiario> {
  final int id;
  final String nome;
  final String ref;
  final int numeroDeVisita;
  final DateTime dataDeNascimento;
  final DateTime dataDeAtendimento;
  final int provinciaId;
  final int bairroId;
  final int sexoId;
  final int provinienciaId;
  final int ObjectivoDaVistaId;
  final DateTime createdAt;
  final DateTime updatedAat;
  Benificiario(
      {@required this.id,
      @required this.nome,
      @required this.ref,
      @required this.numeroDeVisita,
      @required this.dataDeNascimento,
      @required this.dataDeAtendimento,
      this.provinciaId,
      this.bairroId,
      this.sexoId,
      this.provinienciaId,
      this.ObjectivoDaVistaId,
      @required this.createdAt,
      @required this.updatedAat});
  factory Benificiario.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return Benificiario(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      nome: stringType.mapFromDatabaseResponse(data['${effectivePrefix}nome']),
      ref: stringType.mapFromDatabaseResponse(data['${effectivePrefix}ref']),
      numeroDeVisita: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}numero_de_visita']),
      dataDeNascimento: dateTimeType.mapFromDatabaseResponse(
          data['${effectivePrefix}data_de_nascimento']),
      dataDeAtendimento: dateTimeType.mapFromDatabaseResponse(
          data['${effectivePrefix}data_de_atendimento']),
      provinciaId: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}provincia_id']),
      bairroId:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}bairro_id']),
      sexoId:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}sexo_id']),
      provinienciaId: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}proviniencia_id']),
      ObjectivoDaVistaId: intType.mapFromDatabaseResponse(
          data['${effectivePrefix}objectivo_da_vista_id']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      updatedAat: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_aat']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || nome != null) {
      map['nome'] = Variable<String>(nome);
    }
    if (!nullToAbsent || ref != null) {
      map['ref'] = Variable<String>(ref);
    }
    if (!nullToAbsent || numeroDeVisita != null) {
      map['numero_de_visita'] = Variable<int>(numeroDeVisita);
    }
    if (!nullToAbsent || dataDeNascimento != null) {
      map['data_de_nascimento'] = Variable<DateTime>(dataDeNascimento);
    }
    if (!nullToAbsent || dataDeAtendimento != null) {
      map['data_de_atendimento'] = Variable<DateTime>(dataDeAtendimento);
    }
    if (!nullToAbsent || provinciaId != null) {
      map['provincia_id'] = Variable<int>(provinciaId);
    }
    if (!nullToAbsent || bairroId != null) {
      map['bairro_id'] = Variable<int>(bairroId);
    }
    if (!nullToAbsent || sexoId != null) {
      map['sexo_id'] = Variable<int>(sexoId);
    }
    if (!nullToAbsent || provinienciaId != null) {
      map['proviniencia_id'] = Variable<int>(provinienciaId);
    }
    if (!nullToAbsent || ObjectivoDaVistaId != null) {
      map['objectivo_da_vista_id'] = Variable<int>(ObjectivoDaVistaId);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<DateTime>(createdAt);
    }
    if (!nullToAbsent || updatedAat != null) {
      map['updated_aat'] = Variable<DateTime>(updatedAat);
    }
    return map;
  }

  BenificiariosCompanion toCompanion(bool nullToAbsent) {
    return BenificiariosCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      nome: nome == null && nullToAbsent ? const Value.absent() : Value(nome),
      ref: ref == null && nullToAbsent ? const Value.absent() : Value(ref),
      numeroDeVisita: numeroDeVisita == null && nullToAbsent
          ? const Value.absent()
          : Value(numeroDeVisita),
      dataDeNascimento: dataDeNascimento == null && nullToAbsent
          ? const Value.absent()
          : Value(dataDeNascimento),
      dataDeAtendimento: dataDeAtendimento == null && nullToAbsent
          ? const Value.absent()
          : Value(dataDeAtendimento),
      provinciaId: provinciaId == null && nullToAbsent
          ? const Value.absent()
          : Value(provinciaId),
      bairroId: bairroId == null && nullToAbsent
          ? const Value.absent()
          : Value(bairroId),
      sexoId:
          sexoId == null && nullToAbsent ? const Value.absent() : Value(sexoId),
      provinienciaId: provinienciaId == null && nullToAbsent
          ? const Value.absent()
          : Value(provinienciaId),
      ObjectivoDaVistaId: ObjectivoDaVistaId == null && nullToAbsent
          ? const Value.absent()
          : Value(ObjectivoDaVistaId),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      updatedAat: updatedAat == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAat),
    );
  }

  factory Benificiario.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Benificiario(
      id: serializer.fromJson<int>(json['id']),
      nome: serializer.fromJson<String>(json['nome']),
      ref: serializer.fromJson<String>(json['ref']),
      numeroDeVisita: serializer.fromJson<int>(json['numeroDeVisita']),
      dataDeNascimento: serializer.fromJson<DateTime>(json['dataDeNascimento']),
      dataDeAtendimento:
          serializer.fromJson<DateTime>(json['dataDeAtendimento']),
      provinciaId: serializer.fromJson<int>(json['provinciaId']),
      bairroId: serializer.fromJson<int>(json['bairroId']),
      sexoId: serializer.fromJson<int>(json['sexoId']),
      provinienciaId: serializer.fromJson<int>(json['provinienciaId']),
      ObjectivoDaVistaId: serializer.fromJson<int>(json['ObjectivoDaVistaId']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAat: serializer.fromJson<DateTime>(json['updatedAat']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'nome': serializer.toJson<String>(nome),
      'ref': serializer.toJson<String>(ref),
      'numeroDeVisita': serializer.toJson<int>(numeroDeVisita),
      'dataDeNascimento': serializer.toJson<DateTime>(dataDeNascimento),
      'dataDeAtendimento': serializer.toJson<DateTime>(dataDeAtendimento),
      'provinciaId': serializer.toJson<int>(provinciaId),
      'bairroId': serializer.toJson<int>(bairroId),
      'sexoId': serializer.toJson<int>(sexoId),
      'provinienciaId': serializer.toJson<int>(provinienciaId),
      'ObjectivoDaVistaId': serializer.toJson<int>(ObjectivoDaVistaId),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAat': serializer.toJson<DateTime>(updatedAat),
    };
  }

  Benificiario copyWith(
          {int id,
          String nome,
          String ref,
          int numeroDeVisita,
          DateTime dataDeNascimento,
          DateTime dataDeAtendimento,
          int provinciaId,
          int bairroId,
          int sexoId,
          int provinienciaId,
          int ObjectivoDaVistaId,
          DateTime createdAt,
          DateTime updatedAat}) =>
      Benificiario(
        id: id ?? this.id,
        nome: nome ?? this.nome,
        ref: ref ?? this.ref,
        numeroDeVisita: numeroDeVisita ?? this.numeroDeVisita,
        dataDeNascimento: dataDeNascimento ?? this.dataDeNascimento,
        dataDeAtendimento: dataDeAtendimento ?? this.dataDeAtendimento,
        provinciaId: provinciaId ?? this.provinciaId,
        bairroId: bairroId ?? this.bairroId,
        sexoId: sexoId ?? this.sexoId,
        provinienciaId: provinienciaId ?? this.provinienciaId,
        ObjectivoDaVistaId: ObjectivoDaVistaId ?? this.ObjectivoDaVistaId,
        createdAt: createdAt ?? this.createdAt,
        updatedAat: updatedAat ?? this.updatedAat,
      );
  @override
  String toString() {
    return (StringBuffer('Benificiario(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('ref: $ref, ')
          ..write('numeroDeVisita: $numeroDeVisita, ')
          ..write('dataDeNascimento: $dataDeNascimento, ')
          ..write('dataDeAtendimento: $dataDeAtendimento, ')
          ..write('provinciaId: $provinciaId, ')
          ..write('bairroId: $bairroId, ')
          ..write('sexoId: $sexoId, ')
          ..write('provinienciaId: $provinienciaId, ')
          ..write('ObjectivoDaVistaId: $ObjectivoDaVistaId, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAat: $updatedAat')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          nome.hashCode,
          $mrjc(
              ref.hashCode,
              $mrjc(
                  numeroDeVisita.hashCode,
                  $mrjc(
                      dataDeNascimento.hashCode,
                      $mrjc(
                          dataDeAtendimento.hashCode,
                          $mrjc(
                              provinciaId.hashCode,
                              $mrjc(
                                  bairroId.hashCode,
                                  $mrjc(
                                      sexoId.hashCode,
                                      $mrjc(
                                          provinienciaId.hashCode,
                                          $mrjc(
                                              ObjectivoDaVistaId.hashCode,
                                              $mrjc(
                                                  createdAt.hashCode,
                                                  updatedAat
                                                      .hashCode)))))))))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Benificiario &&
          other.id == this.id &&
          other.nome == this.nome &&
          other.ref == this.ref &&
          other.numeroDeVisita == this.numeroDeVisita &&
          other.dataDeNascimento == this.dataDeNascimento &&
          other.dataDeAtendimento == this.dataDeAtendimento &&
          other.provinciaId == this.provinciaId &&
          other.bairroId == this.bairroId &&
          other.sexoId == this.sexoId &&
          other.provinienciaId == this.provinienciaId &&
          other.ObjectivoDaVistaId == this.ObjectivoDaVistaId &&
          other.createdAt == this.createdAt &&
          other.updatedAat == this.updatedAat);
}

class BenificiariosCompanion extends UpdateCompanion<Benificiario> {
  final Value<int> id;
  final Value<String> nome;
  final Value<String> ref;
  final Value<int> numeroDeVisita;
  final Value<DateTime> dataDeNascimento;
  final Value<DateTime> dataDeAtendimento;
  final Value<int> provinciaId;
  final Value<int> bairroId;
  final Value<int> sexoId;
  final Value<int> provinienciaId;
  final Value<int> ObjectivoDaVistaId;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAat;
  const BenificiariosCompanion({
    this.id = const Value.absent(),
    this.nome = const Value.absent(),
    this.ref = const Value.absent(),
    this.numeroDeVisita = const Value.absent(),
    this.dataDeNascimento = const Value.absent(),
    this.dataDeAtendimento = const Value.absent(),
    this.provinciaId = const Value.absent(),
    this.bairroId = const Value.absent(),
    this.sexoId = const Value.absent(),
    this.provinienciaId = const Value.absent(),
    this.ObjectivoDaVistaId = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAat = const Value.absent(),
  });
  BenificiariosCompanion.insert({
    this.id = const Value.absent(),
    @required String nome,
    this.ref = const Value.absent(),
    @required int numeroDeVisita,
    @required DateTime dataDeNascimento,
    @required DateTime dataDeAtendimento,
    this.provinciaId = const Value.absent(),
    this.bairroId = const Value.absent(),
    this.sexoId = const Value.absent(),
    this.provinienciaId = const Value.absent(),
    this.ObjectivoDaVistaId = const Value.absent(),
    @required DateTime createdAt,
    @required DateTime updatedAat,
  })  : nome = Value(nome),
        numeroDeVisita = Value(numeroDeVisita),
        dataDeNascimento = Value(dataDeNascimento),
        dataDeAtendimento = Value(dataDeAtendimento),
        createdAt = Value(createdAt),
        updatedAat = Value(updatedAat);
  static Insertable<Benificiario> custom({
    Expression<int> id,
    Expression<String> nome,
    Expression<String> ref,
    Expression<int> numeroDeVisita,
    Expression<DateTime> dataDeNascimento,
    Expression<DateTime> dataDeAtendimento,
    Expression<int> provinciaId,
    Expression<int> bairroId,
    Expression<int> sexoId,
    Expression<int> provinienciaId,
    Expression<int> ObjectivoDaVistaId,
    Expression<DateTime> createdAt,
    Expression<DateTime> updatedAat,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (nome != null) 'nome': nome,
      if (ref != null) 'ref': ref,
      if (numeroDeVisita != null) 'numero_de_visita': numeroDeVisita,
      if (dataDeNascimento != null) 'data_de_nascimento': dataDeNascimento,
      if (dataDeAtendimento != null) 'data_de_atendimento': dataDeAtendimento,
      if (provinciaId != null) 'provincia_id': provinciaId,
      if (bairroId != null) 'bairro_id': bairroId,
      if (sexoId != null) 'sexo_id': sexoId,
      if (provinienciaId != null) 'proviniencia_id': provinienciaId,
      if (ObjectivoDaVistaId != null)
        'objectivo_da_vista_id': ObjectivoDaVistaId,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAat != null) 'updated_aat': updatedAat,
    });
  }

  BenificiariosCompanion copyWith(
      {Value<int> id,
      Value<String> nome,
      Value<String> ref,
      Value<int> numeroDeVisita,
      Value<DateTime> dataDeNascimento,
      Value<DateTime> dataDeAtendimento,
      Value<int> provinciaId,
      Value<int> bairroId,
      Value<int> sexoId,
      Value<int> provinienciaId,
      Value<int> ObjectivoDaVistaId,
      Value<DateTime> createdAt,
      Value<DateTime> updatedAat}) {
    return BenificiariosCompanion(
      id: id ?? this.id,
      nome: nome ?? this.nome,
      ref: ref ?? this.ref,
      numeroDeVisita: numeroDeVisita ?? this.numeroDeVisita,
      dataDeNascimento: dataDeNascimento ?? this.dataDeNascimento,
      dataDeAtendimento: dataDeAtendimento ?? this.dataDeAtendimento,
      provinciaId: provinciaId ?? this.provinciaId,
      bairroId: bairroId ?? this.bairroId,
      sexoId: sexoId ?? this.sexoId,
      provinienciaId: provinienciaId ?? this.provinienciaId,
      ObjectivoDaVistaId: ObjectivoDaVistaId ?? this.ObjectivoDaVistaId,
      createdAt: createdAt ?? this.createdAt,
      updatedAat: updatedAat ?? this.updatedAat,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (nome.present) {
      map['nome'] = Variable<String>(nome.value);
    }
    if (ref.present) {
      map['ref'] = Variable<String>(ref.value);
    }
    if (numeroDeVisita.present) {
      map['numero_de_visita'] = Variable<int>(numeroDeVisita.value);
    }
    if (dataDeNascimento.present) {
      map['data_de_nascimento'] = Variable<DateTime>(dataDeNascimento.value);
    }
    if (dataDeAtendimento.present) {
      map['data_de_atendimento'] = Variable<DateTime>(dataDeAtendimento.value);
    }
    if (provinciaId.present) {
      map['provincia_id'] = Variable<int>(provinciaId.value);
    }
    if (bairroId.present) {
      map['bairro_id'] = Variable<int>(bairroId.value);
    }
    if (sexoId.present) {
      map['sexo_id'] = Variable<int>(sexoId.value);
    }
    if (provinienciaId.present) {
      map['proviniencia_id'] = Variable<int>(provinienciaId.value);
    }
    if (ObjectivoDaVistaId.present) {
      map['objectivo_da_vista_id'] = Variable<int>(ObjectivoDaVistaId.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAat.present) {
      map['updated_aat'] = Variable<DateTime>(updatedAat.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('BenificiariosCompanion(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('ref: $ref, ')
          ..write('numeroDeVisita: $numeroDeVisita, ')
          ..write('dataDeNascimento: $dataDeNascimento, ')
          ..write('dataDeAtendimento: $dataDeAtendimento, ')
          ..write('provinciaId: $provinciaId, ')
          ..write('bairroId: $bairroId, ')
          ..write('sexoId: $sexoId, ')
          ..write('provinienciaId: $provinienciaId, ')
          ..write('ObjectivoDaVistaId: $ObjectivoDaVistaId, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAat: $updatedAat')
          ..write(')'))
        .toString();
  }
}

class $BenificiariosTable extends Benificiarios
    with TableInfo<$BenificiariosTable, Benificiario> {
  final GeneratedDatabase _db;
  final String _alias;
  $BenificiariosTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _nomeMeta = const VerificationMeta('nome');
  GeneratedTextColumn _nome;
  @override
  GeneratedTextColumn get nome => _nome ??= _constructNome();
  GeneratedTextColumn _constructNome() {
    return GeneratedTextColumn('nome', $tableName, false, maxTextLength: 191);
  }

  final VerificationMeta _refMeta = const VerificationMeta('ref');
  GeneratedTextColumn _ref;
  @override
  GeneratedTextColumn get ref => _ref ??= _constructRef();
  GeneratedTextColumn _constructRef() {
    return GeneratedTextColumn('ref', $tableName, false,
        maxTextLength: 191, $customConstraints: 'UNIQUE')
      ..clientDefault = () => _uuid.v4();
  }

  final VerificationMeta _numeroDeVisitaMeta =
      const VerificationMeta('numeroDeVisita');
  GeneratedIntColumn _numeroDeVisita;
  @override
  GeneratedIntColumn get numeroDeVisita =>
      _numeroDeVisita ??= _constructNumeroDeVisita();
  GeneratedIntColumn _constructNumeroDeVisita() {
    return GeneratedIntColumn(
      'numero_de_visita',
      $tableName,
      false,
    );
  }

  final VerificationMeta _dataDeNascimentoMeta =
      const VerificationMeta('dataDeNascimento');
  GeneratedDateTimeColumn _dataDeNascimento;
  @override
  GeneratedDateTimeColumn get dataDeNascimento =>
      _dataDeNascimento ??= _constructDataDeNascimento();
  GeneratedDateTimeColumn _constructDataDeNascimento() {
    return GeneratedDateTimeColumn(
      'data_de_nascimento',
      $tableName,
      false,
    );
  }

  final VerificationMeta _dataDeAtendimentoMeta =
      const VerificationMeta('dataDeAtendimento');
  GeneratedDateTimeColumn _dataDeAtendimento;
  @override
  GeneratedDateTimeColumn get dataDeAtendimento =>
      _dataDeAtendimento ??= _constructDataDeAtendimento();
  GeneratedDateTimeColumn _constructDataDeAtendimento() {
    return GeneratedDateTimeColumn(
      'data_de_atendimento',
      $tableName,
      false,
    );
  }

  final VerificationMeta _provinciaIdMeta =
      const VerificationMeta('provinciaId');
  GeneratedIntColumn _provinciaId;
  @override
  GeneratedIntColumn get provinciaId =>
      _provinciaId ??= _constructProvinciaId();
  GeneratedIntColumn _constructProvinciaId() {
    return GeneratedIntColumn('provincia_id', $tableName, true,
        $customConstraints: 'NULL REFERENCES provincias(id)');
  }

  final VerificationMeta _bairroIdMeta = const VerificationMeta('bairroId');
  GeneratedIntColumn _bairroId;
  @override
  GeneratedIntColumn get bairroId => _bairroId ??= _constructBairroId();
  GeneratedIntColumn _constructBairroId() {
    return GeneratedIntColumn('bairro_id', $tableName, true,
        $customConstraints: 'NULL REFERENCES bairros(id)');
  }

  final VerificationMeta _sexoIdMeta = const VerificationMeta('sexoId');
  GeneratedIntColumn _sexoId;
  @override
  GeneratedIntColumn get sexoId => _sexoId ??= _constructSexoId();
  GeneratedIntColumn _constructSexoId() {
    return GeneratedIntColumn('sexo_id', $tableName, true,
        $customConstraints: 'NULL REFERENCES sexos(id)');
  }

  final VerificationMeta _provinienciaIdMeta =
      const VerificationMeta('provinienciaId');
  GeneratedIntColumn _provinienciaId;
  @override
  GeneratedIntColumn get provinienciaId =>
      _provinienciaId ??= _constructProvinienciaId();
  GeneratedIntColumn _constructProvinienciaId() {
    return GeneratedIntColumn('proviniencia_id', $tableName, true,
        $customConstraints: 'NULL REFERENCES proviniencias(id)');
  }

  final VerificationMeta _ObjectivoDaVistaIdMeta =
      const VerificationMeta('ObjectivoDaVistaId');
  GeneratedIntColumn _ObjectivoDaVistaId;
  @override
  GeneratedIntColumn get ObjectivoDaVistaId =>
      _ObjectivoDaVistaId ??= _constructObjectivoDaVistaId();
  GeneratedIntColumn _constructObjectivoDaVistaId() {
    return GeneratedIntColumn('objectivo_da_vista_id', $tableName, true,
        $customConstraints: 'NULL REFERENCES objectivo_da_vistas(id)');
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  final VerificationMeta _updatedAatMeta = const VerificationMeta('updatedAat');
  GeneratedDateTimeColumn _updatedAat;
  @override
  GeneratedDateTimeColumn get updatedAat =>
      _updatedAat ??= _constructUpdatedAat();
  GeneratedDateTimeColumn _constructUpdatedAat() {
    return GeneratedDateTimeColumn(
      'updated_aat',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [
        id,
        nome,
        ref,
        numeroDeVisita,
        dataDeNascimento,
        dataDeAtendimento,
        provinciaId,
        bairroId,
        sexoId,
        provinienciaId,
        ObjectivoDaVistaId,
        createdAt,
        updatedAat
      ];
  @override
  $BenificiariosTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'benificiarios';
  @override
  final String actualTableName = 'benificiarios';
  @override
  VerificationContext validateIntegrity(Insertable<Benificiario> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('nome')) {
      context.handle(
          _nomeMeta, nome.isAcceptableOrUnknown(data['nome'], _nomeMeta));
    } else if (isInserting) {
      context.missing(_nomeMeta);
    }
    if (data.containsKey('ref')) {
      context.handle(
          _refMeta, ref.isAcceptableOrUnknown(data['ref'], _refMeta));
    }
    if (data.containsKey('numero_de_visita')) {
      context.handle(
          _numeroDeVisitaMeta,
          numeroDeVisita.isAcceptableOrUnknown(
              data['numero_de_visita'], _numeroDeVisitaMeta));
    } else if (isInserting) {
      context.missing(_numeroDeVisitaMeta);
    }
    if (data.containsKey('data_de_nascimento')) {
      context.handle(
          _dataDeNascimentoMeta,
          dataDeNascimento.isAcceptableOrUnknown(
              data['data_de_nascimento'], _dataDeNascimentoMeta));
    } else if (isInserting) {
      context.missing(_dataDeNascimentoMeta);
    }
    if (data.containsKey('data_de_atendimento')) {
      context.handle(
          _dataDeAtendimentoMeta,
          dataDeAtendimento.isAcceptableOrUnknown(
              data['data_de_atendimento'], _dataDeAtendimentoMeta));
    } else if (isInserting) {
      context.missing(_dataDeAtendimentoMeta);
    }
    if (data.containsKey('provincia_id')) {
      context.handle(
          _provinciaIdMeta,
          provinciaId.isAcceptableOrUnknown(
              data['provincia_id'], _provinciaIdMeta));
    }
    if (data.containsKey('bairro_id')) {
      context.handle(_bairroIdMeta,
          bairroId.isAcceptableOrUnknown(data['bairro_id'], _bairroIdMeta));
    }
    if (data.containsKey('sexo_id')) {
      context.handle(_sexoIdMeta,
          sexoId.isAcceptableOrUnknown(data['sexo_id'], _sexoIdMeta));
    }
    if (data.containsKey('proviniencia_id')) {
      context.handle(
          _provinienciaIdMeta,
          provinienciaId.isAcceptableOrUnknown(
              data['proviniencia_id'], _provinienciaIdMeta));
    }
    if (data.containsKey('objectivo_da_vista_id')) {
      context.handle(
          _ObjectivoDaVistaIdMeta,
          ObjectivoDaVistaId.isAcceptableOrUnknown(
              data['objectivo_da_vista_id'], _ObjectivoDaVistaIdMeta));
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('updated_aat')) {
      context.handle(
          _updatedAatMeta,
          updatedAat.isAcceptableOrUnknown(
              data['updated_aat'], _updatedAatMeta));
    } else if (isInserting) {
      context.missing(_updatedAatMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Benificiario map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Benificiario.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $BenificiariosTable createAlias(String alias) {
    return $BenificiariosTable(_db, alias);
  }
}

class BenificiarioEncaminhado extends DataClass
    implements Insertable<BenificiarioEncaminhado> {
  final int id;
  final int benificiarioId;
  final String objectivoDaVista;
  final bool necessitaDeAcompanhamenBairromiciliar;
  final DateTime dataQueFoiRecebido;
  final bool problemaResolvido;
  final int motivoDeAberturaDeProcesso;
  final DateTime createdAt;
  final DateTime updatedAt;
  BenificiarioEncaminhado(
      {@required this.id,
      this.benificiarioId,
      this.objectivoDaVista,
      @required this.necessitaDeAcompanhamenBairromiciliar,
      @required this.dataQueFoiRecebido,
      @required this.problemaResolvido,
      this.motivoDeAberturaDeProcesso,
      @required this.createdAt,
      @required this.updatedAt});
  factory BenificiarioEncaminhado.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final boolType = db.typeSystem.forDartType<bool>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return BenificiarioEncaminhado(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      benificiarioId: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}benificiario_id']),
      objectivoDaVista: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}objectivo_da_vista']),
      necessitaDeAcompanhamenBairromiciliar: boolType.mapFromDatabaseResponse(
          data['${effectivePrefix}necessita_de_acompanhamen_bairromiciliar']),
      dataQueFoiRecebido: dateTimeType.mapFromDatabaseResponse(
          data['${effectivePrefix}data_que_foi_recebido']),
      problemaResolvido: boolType.mapFromDatabaseResponse(
          data['${effectivePrefix}problema_resolvido']),
      motivoDeAberturaDeProcesso: intType.mapFromDatabaseResponse(
          data['${effectivePrefix}motivo_de_abertura_de_processo']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      updatedAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || benificiarioId != null) {
      map['benificiario_id'] = Variable<int>(benificiarioId);
    }
    if (!nullToAbsent || objectivoDaVista != null) {
      map['objectivo_da_vista'] = Variable<String>(objectivoDaVista);
    }
    if (!nullToAbsent || necessitaDeAcompanhamenBairromiciliar != null) {
      map['necessita_de_acompanhamen_bairromiciliar'] =
          Variable<bool>(necessitaDeAcompanhamenBairromiciliar);
    }
    if (!nullToAbsent || dataQueFoiRecebido != null) {
      map['data_que_foi_recebido'] = Variable<DateTime>(dataQueFoiRecebido);
    }
    if (!nullToAbsent || problemaResolvido != null) {
      map['problema_resolvido'] = Variable<bool>(problemaResolvido);
    }
    if (!nullToAbsent || motivoDeAberturaDeProcesso != null) {
      map['motivo_de_abertura_de_processo'] =
          Variable<int>(motivoDeAberturaDeProcesso);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<DateTime>(createdAt);
    }
    if (!nullToAbsent || updatedAt != null) {
      map['updated_at'] = Variable<DateTime>(updatedAt);
    }
    return map;
  }

  BenificiarioEncaminhadosCompanion toCompanion(bool nullToAbsent) {
    return BenificiarioEncaminhadosCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      benificiarioId: benificiarioId == null && nullToAbsent
          ? const Value.absent()
          : Value(benificiarioId),
      objectivoDaVista: objectivoDaVista == null && nullToAbsent
          ? const Value.absent()
          : Value(objectivoDaVista),
      necessitaDeAcompanhamenBairromiciliar:
          necessitaDeAcompanhamenBairromiciliar == null && nullToAbsent
              ? const Value.absent()
              : Value(necessitaDeAcompanhamenBairromiciliar),
      dataQueFoiRecebido: dataQueFoiRecebido == null && nullToAbsent
          ? const Value.absent()
          : Value(dataQueFoiRecebido),
      problemaResolvido: problemaResolvido == null && nullToAbsent
          ? const Value.absent()
          : Value(problemaResolvido),
      motivoDeAberturaDeProcesso:
          motivoDeAberturaDeProcesso == null && nullToAbsent
              ? const Value.absent()
              : Value(motivoDeAberturaDeProcesso),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  factory BenificiarioEncaminhado.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return BenificiarioEncaminhado(
      id: serializer.fromJson<int>(json['id']),
      benificiarioId: serializer.fromJson<int>(json['benificiarioId']),
      objectivoDaVista: serializer.fromJson<String>(json['objectivoDaVista']),
      necessitaDeAcompanhamenBairromiciliar: serializer
          .fromJson<bool>(json['necessitaDeAcompanhamenBairromiciliar']),
      dataQueFoiRecebido:
          serializer.fromJson<DateTime>(json['dataQueFoiRecebido']),
      problemaResolvido: serializer.fromJson<bool>(json['problemaResolvido']),
      motivoDeAberturaDeProcesso:
          serializer.fromJson<int>(json['motivoDeAberturaDeProcesso']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'benificiarioId': serializer.toJson<int>(benificiarioId),
      'objectivoDaVista': serializer.toJson<String>(objectivoDaVista),
      'necessitaDeAcompanhamenBairromiciliar':
          serializer.toJson<bool>(necessitaDeAcompanhamenBairromiciliar),
      'dataQueFoiRecebido': serializer.toJson<DateTime>(dataQueFoiRecebido),
      'problemaResolvido': serializer.toJson<bool>(problemaResolvido),
      'motivoDeAberturaDeProcesso':
          serializer.toJson<int>(motivoDeAberturaDeProcesso),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

  BenificiarioEncaminhado copyWith(
          {int id,
          int benificiarioId,
          String objectivoDaVista,
          bool necessitaDeAcompanhamenBairromiciliar,
          DateTime dataQueFoiRecebido,
          bool problemaResolvido,
          int motivoDeAberturaDeProcesso,
          DateTime createdAt,
          DateTime updatedAt}) =>
      BenificiarioEncaminhado(
        id: id ?? this.id,
        benificiarioId: benificiarioId ?? this.benificiarioId,
        objectivoDaVista: objectivoDaVista ?? this.objectivoDaVista,
        necessitaDeAcompanhamenBairromiciliar:
            necessitaDeAcompanhamenBairromiciliar ??
                this.necessitaDeAcompanhamenBairromiciliar,
        dataQueFoiRecebido: dataQueFoiRecebido ?? this.dataQueFoiRecebido,
        problemaResolvido: problemaResolvido ?? this.problemaResolvido,
        motivoDeAberturaDeProcesso:
            motivoDeAberturaDeProcesso ?? this.motivoDeAberturaDeProcesso,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('BenificiarioEncaminhado(')
          ..write('id: $id, ')
          ..write('benificiarioId: $benificiarioId, ')
          ..write('objectivoDaVista: $objectivoDaVista, ')
          ..write(
              'necessitaDeAcompanhamenBairromiciliar: $necessitaDeAcompanhamenBairromiciliar, ')
          ..write('dataQueFoiRecebido: $dataQueFoiRecebido, ')
          ..write('problemaResolvido: $problemaResolvido, ')
          ..write('motivoDeAberturaDeProcesso: $motivoDeAberturaDeProcesso, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          benificiarioId.hashCode,
          $mrjc(
              objectivoDaVista.hashCode,
              $mrjc(
                  necessitaDeAcompanhamenBairromiciliar.hashCode,
                  $mrjc(
                      dataQueFoiRecebido.hashCode,
                      $mrjc(
                          problemaResolvido.hashCode,
                          $mrjc(
                              motivoDeAberturaDeProcesso.hashCode,
                              $mrjc(createdAt.hashCode,
                                  updatedAt.hashCode)))))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is BenificiarioEncaminhado &&
          other.id == this.id &&
          other.benificiarioId == this.benificiarioId &&
          other.objectivoDaVista == this.objectivoDaVista &&
          other.necessitaDeAcompanhamenBairromiciliar ==
              this.necessitaDeAcompanhamenBairromiciliar &&
          other.dataQueFoiRecebido == this.dataQueFoiRecebido &&
          other.problemaResolvido == this.problemaResolvido &&
          other.motivoDeAberturaDeProcesso == this.motivoDeAberturaDeProcesso &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class BenificiarioEncaminhadosCompanion
    extends UpdateCompanion<BenificiarioEncaminhado> {
  final Value<int> id;
  final Value<int> benificiarioId;
  final Value<String> objectivoDaVista;
  final Value<bool> necessitaDeAcompanhamenBairromiciliar;
  final Value<DateTime> dataQueFoiRecebido;
  final Value<bool> problemaResolvido;
  final Value<int> motivoDeAberturaDeProcesso;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  const BenificiarioEncaminhadosCompanion({
    this.id = const Value.absent(),
    this.benificiarioId = const Value.absent(),
    this.objectivoDaVista = const Value.absent(),
    this.necessitaDeAcompanhamenBairromiciliar = const Value.absent(),
    this.dataQueFoiRecebido = const Value.absent(),
    this.problemaResolvido = const Value.absent(),
    this.motivoDeAberturaDeProcesso = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  BenificiarioEncaminhadosCompanion.insert({
    this.id = const Value.absent(),
    this.benificiarioId = const Value.absent(),
    this.objectivoDaVista = const Value.absent(),
    this.necessitaDeAcompanhamenBairromiciliar = const Value.absent(),
    @required DateTime dataQueFoiRecebido,
    this.problemaResolvido = const Value.absent(),
    this.motivoDeAberturaDeProcesso = const Value.absent(),
    @required DateTime createdAt,
    @required DateTime updatedAt,
  })  : dataQueFoiRecebido = Value(dataQueFoiRecebido),
        createdAt = Value(createdAt),
        updatedAt = Value(updatedAt);
  static Insertable<BenificiarioEncaminhado> custom({
    Expression<int> id,
    Expression<int> benificiarioId,
    Expression<String> objectivoDaVista,
    Expression<bool> necessitaDeAcompanhamenBairromiciliar,
    Expression<DateTime> dataQueFoiRecebido,
    Expression<bool> problemaResolvido,
    Expression<int> motivoDeAberturaDeProcesso,
    Expression<DateTime> createdAt,
    Expression<DateTime> updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (benificiarioId != null) 'benificiario_id': benificiarioId,
      if (objectivoDaVista != null) 'objectivo_da_vista': objectivoDaVista,
      if (necessitaDeAcompanhamenBairromiciliar != null)
        'necessita_de_acompanhamen_bairromiciliar':
            necessitaDeAcompanhamenBairromiciliar,
      if (dataQueFoiRecebido != null)
        'data_que_foi_recebido': dataQueFoiRecebido,
      if (problemaResolvido != null) 'problema_resolvido': problemaResolvido,
      if (motivoDeAberturaDeProcesso != null)
        'motivo_de_abertura_de_processo': motivoDeAberturaDeProcesso,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  BenificiarioEncaminhadosCompanion copyWith(
      {Value<int> id,
      Value<int> benificiarioId,
      Value<String> objectivoDaVista,
      Value<bool> necessitaDeAcompanhamenBairromiciliar,
      Value<DateTime> dataQueFoiRecebido,
      Value<bool> problemaResolvido,
      Value<int> motivoDeAberturaDeProcesso,
      Value<DateTime> createdAt,
      Value<DateTime> updatedAt}) {
    return BenificiarioEncaminhadosCompanion(
      id: id ?? this.id,
      benificiarioId: benificiarioId ?? this.benificiarioId,
      objectivoDaVista: objectivoDaVista ?? this.objectivoDaVista,
      necessitaDeAcompanhamenBairromiciliar:
          necessitaDeAcompanhamenBairromiciliar ??
              this.necessitaDeAcompanhamenBairromiciliar,
      dataQueFoiRecebido: dataQueFoiRecebido ?? this.dataQueFoiRecebido,
      problemaResolvido: problemaResolvido ?? this.problemaResolvido,
      motivoDeAberturaDeProcesso:
          motivoDeAberturaDeProcesso ?? this.motivoDeAberturaDeProcesso,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (benificiarioId.present) {
      map['benificiario_id'] = Variable<int>(benificiarioId.value);
    }
    if (objectivoDaVista.present) {
      map['objectivo_da_vista'] = Variable<String>(objectivoDaVista.value);
    }
    if (necessitaDeAcompanhamenBairromiciliar.present) {
      map['necessita_de_acompanhamen_bairromiciliar'] =
          Variable<bool>(necessitaDeAcompanhamenBairromiciliar.value);
    }
    if (dataQueFoiRecebido.present) {
      map['data_que_foi_recebido'] =
          Variable<DateTime>(dataQueFoiRecebido.value);
    }
    if (problemaResolvido.present) {
      map['problema_resolvido'] = Variable<bool>(problemaResolvido.value);
    }
    if (motivoDeAberturaDeProcesso.present) {
      map['motivo_de_abertura_de_processo'] =
          Variable<int>(motivoDeAberturaDeProcesso.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('BenificiarioEncaminhadosCompanion(')
          ..write('id: $id, ')
          ..write('benificiarioId: $benificiarioId, ')
          ..write('objectivoDaVista: $objectivoDaVista, ')
          ..write(
              'necessitaDeAcompanhamenBairromiciliar: $necessitaDeAcompanhamenBairromiciliar, ')
          ..write('dataQueFoiRecebido: $dataQueFoiRecebido, ')
          ..write('problemaResolvido: $problemaResolvido, ')
          ..write('motivoDeAberturaDeProcesso: $motivoDeAberturaDeProcesso, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $BenificiarioEncaminhadosTable extends BenificiarioEncaminhados
    with TableInfo<$BenificiarioEncaminhadosTable, BenificiarioEncaminhado> {
  final GeneratedDatabase _db;
  final String _alias;
  $BenificiarioEncaminhadosTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _benificiarioIdMeta =
      const VerificationMeta('benificiarioId');
  GeneratedIntColumn _benificiarioId;
  @override
  GeneratedIntColumn get benificiarioId =>
      _benificiarioId ??= _constructBenificiarioId();
  GeneratedIntColumn _constructBenificiarioId() {
    return GeneratedIntColumn('benificiario_id', $tableName, true,
        $customConstraints: 'NULL REFERENCES benificiarios(id)');
  }

  final VerificationMeta _objectivoDaVistaMeta =
      const VerificationMeta('objectivoDaVista');
  GeneratedTextColumn _objectivoDaVista;
  @override
  GeneratedTextColumn get objectivoDaVista =>
      _objectivoDaVista ??= _constructObjectivoDaVista();
  GeneratedTextColumn _constructObjectivoDaVista() {
    return GeneratedTextColumn('objectivo_da_vista', $tableName, true,
        maxTextLength: 191);
  }

  final VerificationMeta _necessitaDeAcompanhamenBairromiciliarMeta =
      const VerificationMeta('necessitaDeAcompanhamenBairromiciliar');
  GeneratedBoolColumn _necessitaDeAcompanhamenBairromiciliar;
  @override
  GeneratedBoolColumn get necessitaDeAcompanhamenBairromiciliar =>
      _necessitaDeAcompanhamenBairromiciliar ??=
          _constructNecessitaDeAcompanhamenBairromiciliar();
  GeneratedBoolColumn _constructNecessitaDeAcompanhamenBairromiciliar() {
    return GeneratedBoolColumn(
      'necessita_de_acompanhamen_bairromiciliar',
      $tableName,
      false,
    )..clientDefault = () => false;
  }

  final VerificationMeta _dataQueFoiRecebidoMeta =
      const VerificationMeta('dataQueFoiRecebido');
  GeneratedDateTimeColumn _dataQueFoiRecebido;
  @override
  GeneratedDateTimeColumn get dataQueFoiRecebido =>
      _dataQueFoiRecebido ??= _constructDataQueFoiRecebido();
  GeneratedDateTimeColumn _constructDataQueFoiRecebido() {
    return GeneratedDateTimeColumn(
      'data_que_foi_recebido',
      $tableName,
      false,
    );
  }

  final VerificationMeta _problemaResolvidoMeta =
      const VerificationMeta('problemaResolvido');
  GeneratedBoolColumn _problemaResolvido;
  @override
  GeneratedBoolColumn get problemaResolvido =>
      _problemaResolvido ??= _constructProblemaResolvido();
  GeneratedBoolColumn _constructProblemaResolvido() {
    return GeneratedBoolColumn(
      'problema_resolvido',
      $tableName,
      false,
    )..clientDefault = () => false;
  }

  final VerificationMeta _motivoDeAberturaDeProcessoMeta =
      const VerificationMeta('motivoDeAberturaDeProcesso');
  GeneratedIntColumn _motivoDeAberturaDeProcesso;
  @override
  GeneratedIntColumn get motivoDeAberturaDeProcesso =>
      _motivoDeAberturaDeProcesso ??= _constructMotivoDeAberturaDeProcesso();
  GeneratedIntColumn _constructMotivoDeAberturaDeProcesso() {
    return GeneratedIntColumn(
        'motivo_de_abertura_de_processo', $tableName, true,
        $customConstraints:
            'NULL REFERENCES motivo_de_abertura_de_processos(id)');
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedDateTimeColumn _updatedAt;
  @override
  GeneratedDateTimeColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedDateTimeColumn _constructUpdatedAt() {
    return GeneratedDateTimeColumn(
      'updated_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [
        id,
        benificiarioId,
        objectivoDaVista,
        necessitaDeAcompanhamenBairromiciliar,
        dataQueFoiRecebido,
        problemaResolvido,
        motivoDeAberturaDeProcesso,
        createdAt,
        updatedAt
      ];
  @override
  $BenificiarioEncaminhadosTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'benificiario_encaminhados';
  @override
  final String actualTableName = 'benificiario_encaminhados';
  @override
  VerificationContext validateIntegrity(
      Insertable<BenificiarioEncaminhado> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('benificiario_id')) {
      context.handle(
          _benificiarioIdMeta,
          benificiarioId.isAcceptableOrUnknown(
              data['benificiario_id'], _benificiarioIdMeta));
    }
    if (data.containsKey('objectivo_da_vista')) {
      context.handle(
          _objectivoDaVistaMeta,
          objectivoDaVista.isAcceptableOrUnknown(
              data['objectivo_da_vista'], _objectivoDaVistaMeta));
    }
    if (data.containsKey('necessita_de_acompanhamen_bairromiciliar')) {
      context.handle(
          _necessitaDeAcompanhamenBairromiciliarMeta,
          necessitaDeAcompanhamenBairromiciliar.isAcceptableOrUnknown(
              data['necessita_de_acompanhamen_bairromiciliar'],
              _necessitaDeAcompanhamenBairromiciliarMeta));
    }
    if (data.containsKey('data_que_foi_recebido')) {
      context.handle(
          _dataQueFoiRecebidoMeta,
          dataQueFoiRecebido.isAcceptableOrUnknown(
              data['data_que_foi_recebido'], _dataQueFoiRecebidoMeta));
    } else if (isInserting) {
      context.missing(_dataQueFoiRecebidoMeta);
    }
    if (data.containsKey('problema_resolvido')) {
      context.handle(
          _problemaResolvidoMeta,
          problemaResolvido.isAcceptableOrUnknown(
              data['problema_resolvido'], _problemaResolvidoMeta));
    }
    if (data.containsKey('motivo_de_abertura_de_processo')) {
      context.handle(
          _motivoDeAberturaDeProcessoMeta,
          motivoDeAberturaDeProcesso.isAcceptableOrUnknown(
              data['motivo_de_abertura_de_processo'],
              _motivoDeAberturaDeProcessoMeta));
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at'], _updatedAtMeta));
    } else if (isInserting) {
      context.missing(_updatedAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  BenificiarioEncaminhado map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return BenificiarioEncaminhado.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $BenificiarioEncaminhadosTable createAlias(String alias) {
    return $BenificiarioEncaminhadosTable(_db, alias);
  }
}

class Proviniencia extends DataClass implements Insertable<Proviniencia> {
  final int id;
  final String nome;
  final DateTime createdAt;
  final DateTime updatedAt;
  Proviniencia(
      {@required this.id,
      @required this.nome,
      @required this.createdAt,
      @required this.updatedAt});
  factory Proviniencia.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return Proviniencia(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      nome: stringType.mapFromDatabaseResponse(data['${effectivePrefix}nome']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      updatedAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || nome != null) {
      map['nome'] = Variable<String>(nome);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<DateTime>(createdAt);
    }
    if (!nullToAbsent || updatedAt != null) {
      map['updated_at'] = Variable<DateTime>(updatedAt);
    }
    return map;
  }

  ProvinienciasCompanion toCompanion(bool nullToAbsent) {
    return ProvinienciasCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      nome: nome == null && nullToAbsent ? const Value.absent() : Value(nome),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  factory Proviniencia.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Proviniencia(
      id: serializer.fromJson<int>(json['id']),
      nome: serializer.fromJson<String>(json['nome']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'nome': serializer.toJson<String>(nome),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

  Proviniencia copyWith(
          {int id, String nome, DateTime createdAt, DateTime updatedAt}) =>
      Proviniencia(
        id: id ?? this.id,
        nome: nome ?? this.nome,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('Proviniencia(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(id.hashCode,
      $mrjc(nome.hashCode, $mrjc(createdAt.hashCode, updatedAt.hashCode))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Proviniencia &&
          other.id == this.id &&
          other.nome == this.nome &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class ProvinienciasCompanion extends UpdateCompanion<Proviniencia> {
  final Value<int> id;
  final Value<String> nome;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  const ProvinienciasCompanion({
    this.id = const Value.absent(),
    this.nome = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  ProvinienciasCompanion.insert({
    this.id = const Value.absent(),
    @required String nome,
    @required DateTime createdAt,
    @required DateTime updatedAt,
  })  : nome = Value(nome),
        createdAt = Value(createdAt),
        updatedAt = Value(updatedAt);
  static Insertable<Proviniencia> custom({
    Expression<int> id,
    Expression<String> nome,
    Expression<DateTime> createdAt,
    Expression<DateTime> updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (nome != null) 'nome': nome,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  ProvinienciasCompanion copyWith(
      {Value<int> id,
      Value<String> nome,
      Value<DateTime> createdAt,
      Value<DateTime> updatedAt}) {
    return ProvinienciasCompanion(
      id: id ?? this.id,
      nome: nome ?? this.nome,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (nome.present) {
      map['nome'] = Variable<String>(nome.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('ProvinienciasCompanion(')
          ..write('id: $id, ')
          ..write('nome: $nome, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $ProvinienciasTable extends Proviniencias
    with TableInfo<$ProvinienciasTable, Proviniencia> {
  final GeneratedDatabase _db;
  final String _alias;
  $ProvinienciasTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _nomeMeta = const VerificationMeta('nome');
  GeneratedTextColumn _nome;
  @override
  GeneratedTextColumn get nome => _nome ??= _constructNome();
  GeneratedTextColumn _constructNome() {
    return GeneratedTextColumn('nome', $tableName, false, maxTextLength: 191);
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedDateTimeColumn _updatedAt;
  @override
  GeneratedDateTimeColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedDateTimeColumn _constructUpdatedAt() {
    return GeneratedDateTimeColumn(
      'updated_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, nome, createdAt, updatedAt];
  @override
  $ProvinienciasTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'proviniencias';
  @override
  final String actualTableName = 'proviniencias';
  @override
  VerificationContext validateIntegrity(Insertable<Proviniencia> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('nome')) {
      context.handle(
          _nomeMeta, nome.isAcceptableOrUnknown(data['nome'], _nomeMeta));
    } else if (isInserting) {
      context.missing(_nomeMeta);
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at'], _updatedAtMeta));
    } else if (isInserting) {
      context.missing(_updatedAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Proviniencia map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Proviniencia.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $ProvinienciasTable createAlias(String alias) {
    return $ProvinienciasTable(_db, alias);
  }
}

abstract class _$MyDatabase extends GeneratedDatabase {
  _$MyDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $BairrosTable _bairros;
  $BairrosTable get bairros => _bairros ??= $BairrosTable(this);
  $ContactosTable _contactos;
  $ContactosTable get contactos => _contactos ??= $ContactosTable(this);
  $BenificiarioEncaminhadoDocumentosTable _benificiarioEncaminhadoDocumentos;
  $BenificiarioEncaminhadoDocumentosTable
      get benificiarioEncaminhadoDocumentos =>
          _benificiarioEncaminhadoDocumentos ??=
              $BenificiarioEncaminhadoDocumentosTable(this);
  $BenificiarioEncaminhadoServicoEncaminhadosTable
      _benificiarioEncaminhadoServicoEncaminhados;
  $BenificiarioEncaminhadoServicoEncaminhadosTable
      get benificiarioEncaminhadoServicoEncaminhados =>
          _benificiarioEncaminhadoServicoEncaminhados ??=
              $BenificiarioEncaminhadoServicoEncaminhadosTable(this);
  $ProvinciasTable _provincias;
  $ProvinciasTable get provincias => _provincias ??= $ProvinciasTable(this);
  $SexosTable _sexos;
  $SexosTable get sexos => _sexos ??= $SexosTable(this);
  $ObjectivoDaVisitasTable _objectivoDaVisitas;
  $ObjectivoDaVisitasTable get objectivoDaVisitas =>
      _objectivoDaVisitas ??= $ObjectivoDaVisitasTable(this);
  $ServicoEncaminhadosTable _servicoEncaminhados;
  $ServicoEncaminhadosTable get servicoEncaminhados =>
      _servicoEncaminhados ??= $ServicoEncaminhadosTable(this);
  $DocumentosTable _documentos;
  $DocumentosTable get documentos => _documentos ??= $DocumentosTable(this);
  $MotivoDeAberturaDeProcessosTable _motivoDeAberturaDeProcessos;
  $MotivoDeAberturaDeProcessosTable get motivoDeAberturaDeProcessos =>
      _motivoDeAberturaDeProcessos ??= $MotivoDeAberturaDeProcessosTable(this);
  $BenificiariosTable _benificiarios;
  $BenificiariosTable get benificiarios =>
      _benificiarios ??= $BenificiariosTable(this);
  $BenificiarioEncaminhadosTable _benificiarioEncaminhados;
  $BenificiarioEncaminhadosTable get benificiarioEncaminhados =>
      _benificiarioEncaminhados ??= $BenificiarioEncaminhadosTable(this);
  $ProvinienciasTable _proviniencias;
  $ProvinienciasTable get proviniencias =>
      _proviniencias ??= $ProvinienciasTable(this);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [
        bairros,
        contactos,
        benificiarioEncaminhadoDocumentos,
        benificiarioEncaminhadoServicoEncaminhados,
        provincias,
        sexos,
        objectivoDaVisitas,
        servicoEncaminhados,
        documentos,
        motivoDeAberturaDeProcessos,
        benificiarios,
        benificiarioEncaminhados,
        proviniencias
      ];
}
