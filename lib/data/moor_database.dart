import 'package:moor/moor.dart';
// These imports are only needed to open the database
import 'package:moor/ffi.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;
import 'dart:io';
import 'package:uuid/uuid.dart';
//part of generated tables of database
part 'moor_database.g.dart';
//.generating tables database

//tabela de bairros
@DataClassName("Bairro")
class Bairros extends Table{
  IntColumn get id => integer().autoIncrement()();
  TextColumn get nome => text().withLength(max: 191)();
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}
//tabela provincia
@DataClassName("Provincia")
class Provincias extends Table{
  IntColumn get id => integer().autoIncrement()();
  TextColumn get nome => text().withLength(max: 191)();
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}

//tabela de sexo
@DataClassName("Sexo")
class Sexos extends Table{
  IntColumn get id => integer().autoIncrement()();
  TextColumn get nome => text().withLength(max: 191)();
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}

//tabela de objectivos da visita
@DataClassName("ObjectivoDaVisita")
class ObjectivoDaVisitas extends Table{
  IntColumn get id => integer().autoIncrement()();
  TextColumn get nome => text().withLength(max: 191)();
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}

//tabela de servicos encaminhados
@DataClassName("ServicoEncaminhado")
class ServicoEncaminhados extends Table{
  IntColumn get id => integer().autoIncrement()();
  TextColumn get nome => text().withLength(max: 191)();
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}


LazyDatabase _openConnection() {
  // the LazyDatabase util lets us find the right location for the file async.
  return LazyDatabase(() async {
    // put the database file, called db.sqlite here, into the documents folder
    // for your app.
    final dbFolder = await getApplicationDocumentsDirectory();
    final file = File(p.join(dbFolder.path, 'haxs.sqlite'));
    return VmDatabase(file);
  });
}

@DataClassName("Documento")
class Documentos extends Table{
  IntColumn get id => integer().autoIncrement()();
  TextColumn get nome => text().withLength(max: 191)();
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}
//tabela de Motivos de abertura do processo
@DataClassName("MotivoDeAberturaDeProcesso")
class MotivoDeAberturaDeProcessos extends Table{
  IntColumn get id => integer().autoIncrement()();
  TextColumn get nome => text().withLength(max: 191)();
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}

//tabela de provenience
@DataClassName("Contacto")
class Contactos extends Table{
  IntColumn get id => integer().autoIncrement()();
  TextColumn get contacto => text().withLength(max: 25)();
  TextColumn get NomeDoResponsavel => text().withLength(max: 191)();
  IntColumn get  benificiarioId => integer().nullable().customConstraint('NULL REFERENCES benificiarios(id)')();
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}
//

//tabela de bairros
@DataClassName("Benificiario")
class Benificiarios extends Table{
  final _uuid = Uuid();
  IntColumn get id => integer().autoIncrement()();
  TextColumn get nome => text().withLength(max: 191)();
  TextColumn get ref => text().clientDefault(() => _uuid.v4()).withLength(max: 191).customConstraint("UNIQUE")();
  IntColumn get numeroDeVisita => integer()();
  DateTimeColumn get dataDeNascimento => dateTime()();
  DateTimeColumn get dataDeAtendimento => dateTime()();
  IntColumn get provinciaId => integer().nullable().customConstraint('NULL REFERENCES provincias(id)')();
  IntColumn get bairroId => integer().nullable().customConstraint('NULL REFERENCES bairros(id)')();
  IntColumn get sexoId => integer().nullable().customConstraint('NULL REFERENCES sexos(id)')();
  IntColumn get provinienciaId => integer().nullable().customConstraint('NULL REFERENCES proviniencias(id)')();
  IntColumn get ObjectivoDaVistaId => integer().nullable().customConstraint('NULL REFERENCES objectivo_da_vistas(id)')();

  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names s
  DateTimeColumn get updatedAat => dateTime()();
}

@DataClassName("BenificiarioEncaminhado")
class BenificiarioEncaminhados extends Table{
  IntColumn get  id => integer().autoIncrement()();
  IntColumn get  benificiarioId => integer().nullable().customConstraint('NULL REFERENCES benificiarios(id)')();
  TextColumn get objectivoDaVista => text().withLength(max: 191).nullable()();
  BoolColumn get necessitaDeAcompanhamenBairromiciliar => boolean().clientDefault(() => false)();
  DateTimeColumn get dataQueFoiRecebido => dateTime()();
  BoolColumn get problemaResolvido => boolean().clientDefault(() => false)();
  IntColumn get motivoDeAberturaDeProcesso =>  integer().nullable().customConstraint('NULL REFERENCES motivo_de_abertura_de_processos(id)')();

  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}


//tabela de benificiario servico
@DataClassName("BenificiarioEncaminhadoServicoEncaminhado")
class BenificiarioEncaminhadoServicoEncaminhados extends Table{
  IntColumn get id => integer().autoIncrement()();
  IntColumn get benificiarioEncaminhadoId => integer().nullable().customConstraint('NULL REFERENCES benificiario_encaminhados(id)')();
  IntColumn get servicoEncaminhadoId => integer().nullable().customConstraint('NULL REFERENCES servico_encaminhados(id)')();
  TextColumn get especificar => text().withLength(max: 191)();
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}

////BenificiarioEncaminhadoDocumento
//tabela de BenificiarioEncaminhadoDocumento
@DataClassName("BenificiarioEncaminhadoDocumento")
class BenificiarioEncaminhadoDocumentos extends Table{
  IntColumn get id => integer().autoIncrement()();
  IntColumn get benificiarioEncaminhadoId => integer().nullable().customConstraint('NULL REFERENCES benificiario_encaminhados(id)')();
  IntColumn get documentoId => integer().nullable().customConstraint('NULL REFERENCES documentos(id)')();
  TextColumn get especificar => text().withLength(max: 191)();
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}

//tabela de provenience
@DataClassName("Proviniencia")
class Proviniencias extends Table{
  IntColumn get id => integer().autoIncrement()();
  TextColumn get nome => text().withLength(max: 191)();
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}

@UseMoor(tables: [Bairros,Contactos,BenificiarioEncaminhadoDocumentos,BenificiarioEncaminhadoServicoEncaminhados,Provincias,Sexos,ObjectivoDaVisitas,ServicoEncaminhados,Documentos,MotivoDeAberturaDeProcessos,Benificiarios,BenificiarioEncaminhados,Proviniencias])
class MyDatabase extends _$MyDatabase {
  // we tell the database where to store the data with this constructor
  MyDatabase() : super(_openConnection());

  // you should bump this number whenever you change or add a table definition. Migrations
  // are covered later in this readme.
  @override
  int get schemaVersion => 4;
    //get all bairros
  Future<List<Bairro>> get allBairroEntries => select(bairros).get();
  Stream<List<Bairro>> watchEntries() {
    return (select(bairros)).watch();
  }

  Future<List<Bairro>> allBairrosOrdered() {
    return (select(bairros)..orderBy([(t)=>OrderingTerm(expression: t.nome)])).get();
  }
  //get Bairro as id

  Stream<Bairro> watchBairroById(int id) {
    return (select(bairros)..where((t) => t.id.equals(id))).watchSingle();
  }
  Future<Bairro> bairroById(int id) {
    return (select(bairros)..where((t) => t.id.equals(id))).getSingle();
  }
  //add Bairro
  Future<int> addBairro(Bairro entry) {
    return into(bairros).insert(entry);
  }
  Future<int> addBairroCompanion(BairrosCompanion entry) {
    return into(bairros).insert(entry);
  }

  //update Bairro
  Future updateBairro(Bairro entry) {
    return update(bairros).replace(entry);
  }
  Future updateBairroCompanion(BairrosCompanion entry) {
    return update(bairros).replace(entry);
  }

  //delete all bairros
  Future deleteAllBairro() {
    // delete the oldest nine tasks
    return delete(bairros).go();
  }

  Future deleteBairro(int entry) {
    // delete the oldest nine tasks
    return (delete(bairros)..where((t) => t.id.equals(entry))).go();
  }

/****
 *
 */
  //get all Provincias
  Future<List<Provincia>> get allProvinciaEntries => select(provincias).get();
  Stream<List<Provincia>> watchEntriesProvincias() {
    return (select(provincias)).watch();
  }

  Future<List<Provincia>> allProvinciasOrdered() {
    return (select(provincias)..orderBy([(t)=>OrderingTerm(expression: t.nome)])).get();
  }
  //get Provincia as id

  Stream<Provincia> watchProvinciaById(int id) {
    return (select(provincias)..where((t) => t.id.equals(id))).watchSingle();
  }
  Future<Provincia> provinciaById(int id) {
    return (select(provincias)..where((t) => t.id.equals(id))).getSingle();
  }
  //add Provincia
  Future<int> addProvincia(Provincia entry) {
    return into(provincias).insert(entry);
  }
  Future<int> addProvinciaCompanion(ProvinciasCompanion entry) {
    return into(provincias).insert(entry);
  }

  //update Provincia
  Future updateProvincia(Provincia entry) {
    return update(provincias).replace(entry);
  }
  Future updateProvinciaCompanion(ProvinciasCompanion entry) {
    return update(provincias).replace(entry);
  }

  //delete all Provincias
  Future deleteAllProvincia() {
    // delete the oldest nine tasks
    return delete(provincias).go();
  }

  Future deleteProvincia(int entry) {
    // delete the oldest nine tasks
    return (delete(provincias)..where((t) => t.id.equals(entry))).go();
  }



  //get all Sexos
  Future<List<Sexo>> get allSexoEntries => select(sexos).get();
  Stream<List<Sexo>> watchEntriesSexos() {
    return (select(sexos)).watch();
  }

  Future<List<Sexo>> allSexosOrdered() {
    return (select(sexos)..orderBy([(t)=>OrderingTerm(expression: t.nome)])).get();
  }
  //get Sexo as id

  Stream<Sexo> watchSexoById(int id) {
    return (select(sexos)..where((t) => t.id.equals(id))).watchSingle();
  }
  Future<Sexo> SexoById(int id) {
    return (select(sexos)..where((t) => t.id.equals(id))).getSingle();
  }
  //add Sexo
  Future<int> addSexo(Sexo entry) {
    return into(sexos).insert(entry);
  }
  Future<int> addSexoCompanion(SexosCompanion entry) {
    return into(sexos).insert(entry);
  }

  //update Sexo
  Future updateSexo(Sexo entry) {
    return update(sexos).replace(entry);
  }
  Future updateSexoCompanion(SexosCompanion entry) {
    return update(sexos).replace(entry);
  }

  //delete all Sexos
  Future deleteAllSexo() {
    // delete the oldest nine tasks
    return delete(sexos).go();
  }

  Future deleteSexo(int entry) {
    // delete the oldest nine tasks
    return (delete(sexos)..where((t) => t.id.equals(entry))).go();
  }


  //
  //get all ObjectivoDaVisitas
  Future<List<ObjectivoDaVisita>> get allObjectivoDaVisitaEntries => select(objectivoDaVisitas).get();
  Stream<List<ObjectivoDaVisita>> watchEntriesObjectivoDaVisitas() {
    return (select(objectivoDaVisitas)).watch();
  }

  Future<List<ObjectivoDaVisita>> allObjectivoDaVisitasOrdered() {
    return (select(objectivoDaVisitas)..orderBy([(t)=>OrderingTerm(expression: t.nome)])).get();
  }
  //get ObjectivoDaVisita as id

  Stream<ObjectivoDaVisita> watchObjectivoDaVisitaById(int id) {
    return (select(objectivoDaVisitas)..where((t) => t.id.equals(id))).watchSingle();
  }
  Future<ObjectivoDaVisita> ObjectivoDaVisitaById(int id) {
    return (select(objectivoDaVisitas)..where((t) => t.id.equals(id))).getSingle();
  }
  //add ObjectivoDaVisita
  Future<int> addObjectivoDaVisita(ObjectivoDaVisita entry) {
    return into(objectivoDaVisitas).insert(entry);
  }
  Future<int> addObjectivoDaVisitaCompanion(ObjectivoDaVisitasCompanion entry) {
    return into(objectivoDaVisitas).insert(entry);
  }

  //update ObjectivoDaVisita
  Future updateObjectivoDaVisita(ObjectivoDaVisita entry) {
    return update(objectivoDaVisitas).replace(entry);
  }
  Future updateObjectivoDaVisitaCompanion(ObjectivoDaVisitasCompanion entry) {
    return update(objectivoDaVisitas).replace(entry);
  }

  //delete all ObjectivoDaVisitas
  Future deleteAllObjectivoDaVisita() {
    // delete the oldest nine tasks
    return delete(objectivoDaVisitas).go();
  }

  Future deleteObjectivoDaVisita(int entry) {
    // delete the oldest nine tasks
    return (delete(objectivoDaVisitas)..where((t) => t.id.equals(entry))).go();
  }


  //

  //get all ServicoEncaminhados
  Future<List<ServicoEncaminhado>> get allServicoEncaminhadoEntries => select(servicoEncaminhados).get();
  Stream<List<ServicoEncaminhado>> watchEntriesServicoEncaminhados() {
    return (select(servicoEncaminhados)).watch();
  }

  Future<List<ServicoEncaminhado>> allServicoEncaminhadosOrdered() {
    return (select(servicoEncaminhados)..orderBy([(t)=>OrderingTerm(expression: t.nome)])).get();
  }
  //get ServicoEncaminhado as id

  Stream<ServicoEncaminhado> watchServicoEncaminhadoById(int id) {
    return (select(servicoEncaminhados)..where((t) => t.id.equals(id))).watchSingle();
  }
  Future<ServicoEncaminhado> ServicoEncaminhadoById(int id) {
    return (select(servicoEncaminhados)..where((t) => t.id.equals(id))).getSingle();
  }
  //add ServicoEncaminhado
  Future<int> addServicoEncaminhado(ServicoEncaminhado entry) {
    return into(servicoEncaminhados).insert(entry);
  }
  Future<int> addServicoEncaminhadoCompanion(ServicoEncaminhadosCompanion entry) {
    return into(servicoEncaminhados).insert(entry);
  }

  //update ServicoEncaminhado
  Future updateServicoEncaminhado(ServicoEncaminhado entry) {
    return update(servicoEncaminhados).replace(entry);
  }
  Future updateServicoEncaminhadoCompanion(ServicoEncaminhadosCompanion entry) {
    return update(servicoEncaminhados).replace(entry);
  }

  //delete all ServicoEncaminhados
  Future deleteAllServicoEncaminhado() {
    // delete the oldest nine tasks
    return delete(servicoEncaminhados).go();
  }

  Future deleteServicoEncaminhado(int entry) {
    // delete the oldest nine tasks
    return (delete(servicoEncaminhados)..where((t) => t.id.equals(entry))).go();
  }

  //get all Documentos
  Future<List<Documento>> get allDocumentoEntries => select(documentos).get();
  Stream<List<Documento>> watchEntriesDocumentos() {
    return (select(documentos)).watch();
  }

  Future<List<Documento>> allDocumentosOrdered() {
    return (select(documentos)..orderBy([(t)=>OrderingTerm(expression: t.nome)])).get();
  }
  //get Documento as id

  Stream<Documento> watchDocumentoById(int id) {
    return (select(documentos)..where((t) => t.id.equals(id))).watchSingle();
  }
  Future<Documento> DocumentoById(int id) {
    return (select(documentos)..where((t) => t.id.equals(id))).getSingle();
  }
  //add Documento
  Future<int> addDocumento(Documento entry) {
    return into(documentos).insert(entry);
  }
  Future<int> addDocumentoCompanion(DocumentosCompanion entry) {
    return into(documentos).insert(entry);
  }

  //update Documento
  Future updateDocumento(Documento entry) {
    return update(documentos).replace(entry);
  }
  Future updateDocumentoCompanion(DocumentosCompanion entry) {
    return update(documentos).replace(entry);
  }

  //delete all Documentos
  Future deleteAllDocumento() {
    // delete the oldest nine tasks
    return delete(documentos).go();
  }

  Future deleteDocumento(int entry) {
    // delete the oldest nine tasks
    return (delete(documentos)..where((t) => t.id.equals(entry))).go();
  }

  //get all MotivoDeAberturaDeProcessos
  Future<List<MotivoDeAberturaDeProcesso>> get allMotivoDeAberturaDeProcessoEntries => select(motivoDeAberturaDeProcessos).get();
  Stream<List<MotivoDeAberturaDeProcesso>> watchEntriesMotivoDeAberturaDeProcessos() {
    return (select(motivoDeAberturaDeProcessos)).watch();
  }

  Future<List<MotivoDeAberturaDeProcesso>> allMotivoDeAberturaDeProcessosOrdered() {
    return (select(motivoDeAberturaDeProcessos)..orderBy([(t)=>OrderingTerm(expression: t.nome)])).get();
  }
  //get MotivoDeAberturaDeProcesso as id

  Stream<MotivoDeAberturaDeProcesso> watchMotivoDeAberturaDeProcessoById(int id) {
    return (select(motivoDeAberturaDeProcessos)..where((t) => t.id.equals(id))).watchSingle();
  }
  Future<MotivoDeAberturaDeProcesso> MotivoDeAberturaDeProcessoById(int id) {
    return (select(motivoDeAberturaDeProcessos)..where((t) => t.id.equals(id))).getSingle();
  }
  //add MotivoDeAberturaDeProcesso
  Future<int> addMotivoDeAberturaDeProcesso(MotivoDeAberturaDeProcesso entry) {
    return into(motivoDeAberturaDeProcessos).insert(entry);
  }
  Future<int> addMotivoDeAberturaDeProcessoCompanion(MotivoDeAberturaDeProcessosCompanion entry) {
    return into(motivoDeAberturaDeProcessos).insert(entry);
  }

  //update MotivoDeAberturaDeProcesso
  Future updateMotivoDeAberturaDeProcesso(MotivoDeAberturaDeProcesso entry) {
    return update(motivoDeAberturaDeProcessos).replace(entry);
  }
  Future updateMotivoDeAberturaDeProcessoCompanion(MotivoDeAberturaDeProcessosCompanion entry) {
    return update(motivoDeAberturaDeProcessos).replace(entry);
  }

  //delete all MotivoDeAberturaDeProcessos
  Future deleteAllMotivoDeAberturaDeProcesso() {
    // delete the oldest nine tasks
    return delete(motivoDeAberturaDeProcessos).go();
  }

  Future deleteMotivoDeAberturaDeProcesso(int entry) {
    // delete the oldest nine tasks
    return (delete(motivoDeAberturaDeProcessos)..where((t) => t.id.equals(entry))).go();
  }
  //get all Contactos
  Future<List<Contacto>> get allContactoEntries => select(contactos).get();
  Stream<List<Contacto>> watchEntriesContactos() {
    return (select(contactos)).watch();
  }

  Future<List<Contacto>> allContactosOrdered() {
    return (select(contactos)..orderBy([(t)=>OrderingTerm(expression: t.id)])).get();
  }
  //get Contacto as id

  Stream<Contacto> watchContactoById(int id) {
    return (select(contactos)..where((t) => t.id.equals(id))).watchSingle();
  }
  Future<Contacto> ContactoById(int id) {
    return (select(contactos)..where((t) => t.id.equals(id))).getSingle();
  }
  //add Contacto
  Future<int> addContacto(Contacto entry) {
    return into(contactos).insert(entry);
  }
  Future<int> addContactoCompanion(ContactosCompanion entry) {
    return into(contactos).insert(entry);
  }

  //update Contacto
  Future updateContacto(Contacto entry) {
    return update(contactos).replace(entry);
  }
  Future updateContactoCompanion(ContactosCompanion entry) {
    return update(contactos).replace(entry);
  }

  //delete all Contactos
  Future deleteAllContacto() {
    // delete the oldest nine tasks
    return delete(contactos).go();
  }

  Future deleteContacto(int entry) {
    // delete the oldest nine tasks
    return (delete(contactos)..where((t) => t.id.equals(entry))).go();
  }

  //get all Benificiarios
  Future<List<Benificiario>> get allBenificiarioEntries => select(benificiarios).get();
  Stream<List<Benificiario>> watchEntriesBenificiarios() {
    return (select(benificiarios)).watch();
  }

  Future<List<Benificiario>> allBenificiariosOrdered() {
    return (select(benificiarios)..orderBy([(t)=>OrderingTerm(expression: t.nome)])).get();
  }
  //get Benificiario as id

  Stream<Benificiario> watchBenificiarioById(int id) {
    return (select(benificiarios)..where((t) => t.id.equals(id))).watchSingle();
  }
  Future<Benificiario> BenificiarioById(int id) {
    return (select(benificiarios)..where((t) => t.id.equals(id))).getSingle();
  }
  //add Benificiario
  Future<int> addBenificiario(Benificiario entry) {
    return into(benificiarios).insert(entry);
  }
  Future<int> addBenificiarioCompanion(BenificiariosCompanion entry) {
    return into(benificiarios).insert(entry);
  }

  //update Benificiario
  Future updateBenificiario(Benificiario entry) {
    return update(benificiarios).replace(entry);
  }
  Future updateBenificiarioCompanion(BenificiariosCompanion entry) {
    return update(benificiarios).replace(entry);
  }

  //delete all Benificiarios
  Future deleteAllBenificiario() {
    // delete the oldest nine tasks
    return delete(benificiarios).go();
  }

  Future deleteBenificiario(int entry) {
    // delete the oldest nine tasks
    return (delete(benificiarios)..where((t) => t.id.equals(entry))).go();
  }


  //get all BenificiarioEncaminhados
  Future<List<BenificiarioEncaminhado>> get allBenificiarioEncaminhadoEntries => select(benificiarioEncaminhados).get();
  Stream<List<BenificiarioEncaminhado>> watchEntriesBenificiarioEncaminhados() {
    return (select(benificiarioEncaminhados)).watch();
  }

  Future<List<BenificiarioEncaminhado>> allBenificiarioEncaminhadosOrdered() {
    return (select(benificiarioEncaminhados)..orderBy([(t)=>OrderingTerm(expression: t.id)])).get();
  }
  //get BenificiarioEncaminhado as id

  Stream<BenificiarioEncaminhado> watchBenificiarioEncaminhadoById(int id) {
    return (select(benificiarioEncaminhados)..where((t) => t.id.equals(id))).watchSingle();
  }
  Future<BenificiarioEncaminhado> BenificiarioEncaminhadoById(int id) {
    return (select(benificiarioEncaminhados)..where((t) => t.id.equals(id))).getSingle();
  }
  //add BenificiarioEncaminhado
  Future<int> addBenificiarioEncaminhado(BenificiarioEncaminhado entry) {
    return into(benificiarioEncaminhados).insert(entry);
  }
  Future<int> addBenificiarioEncaminhadoCompanion(BenificiarioEncaminhadosCompanion entry) {
    return into(benificiarioEncaminhados).insert(entry);
  }

  //update BenificiarioEncaminhado
  Future updateBenificiarioEncaminhado(BenificiarioEncaminhado entry) {
    return update(benificiarioEncaminhados).replace(entry);
  }
  Future updateBenificiarioEncaminhadoCompanion(BenificiarioEncaminhadosCompanion entry) {
    return update(benificiarioEncaminhados).replace(entry);
  }

  //delete all BenificiarioEncaminhados
  Future deleteAllBenificiarioEncaminhado() {
    // delete the oldest nine tasks
    return delete(benificiarioEncaminhados).go();
  }

  Future deleteBenificiarioEncaminhado(int entry) {
    // delete the oldest nine tasks
    return (delete(benificiarioEncaminhados)..where((t) => t.id.equals(entry))).go();
  }

  //get all BenificiarioEncaminhadoServicoEncaminhados
  Future<List<BenificiarioEncaminhadoServicoEncaminhado>> get allBenificiarioEncaminhadoServicoEncaminhadoEntries => select(benificiarioEncaminhadoServicoEncaminhados).get();
  Stream<List<BenificiarioEncaminhadoServicoEncaminhado>> watchEntriesBenificiarioEncaminhadoServicoEncaminhados() {
    return (select(benificiarioEncaminhadoServicoEncaminhados)).watch();
  }

  Future<List<BenificiarioEncaminhadoServicoEncaminhado>> allBenificiarioEncaminhadoServicoEncaminhadosOrdered() {
    return (select(benificiarioEncaminhadoServicoEncaminhados)..orderBy([(t)=>OrderingTerm(expression: t.id)])).get();
  }
  //get BenificiarioEncaminhadoServicoEncaminhado as id

  Stream<BenificiarioEncaminhadoServicoEncaminhado> watchBenificiarioEncaminhadoServicoEncaminhadoById(int id) {
    return (select(benificiarioEncaminhadoServicoEncaminhados)..where((t) => t.id.equals(id))).watchSingle();
  }
  Future<BenificiarioEncaminhadoServicoEncaminhado> BenificiarioEncaminhadoServicoEncaminhadoById(int id) {
    return (select(benificiarioEncaminhadoServicoEncaminhados)..where((t) => t.id.equals(id))).getSingle();
  }

  Future<List<ServicoEncaminhado>> servicosEncaminhados(int id) async {
    var txc = await (select(benificiarioEncaminhadoServicoEncaminhados)..where((bn) =>
        bn.benificiarioEncaminhadoId.equals(id))).map((row) => row.servicoEncaminhadoId).get();
        return  await (select(servicoEncaminhados)..where((se) => se.id.isIn(txc))).get();
  }

  Future<List<String>> servicosEncaminhadosEsp(int id) async {
   return  (select(benificiarioEncaminhadoServicoEncaminhados)..where((tbl) => tbl.benificiarioEncaminhadoId.equals(id))).map((row) => row.especificar).get();
  }

  //add BenificiarioEncaminhadoServicoEncaminhado
  Future<int> addBenificiarioEncaminhadoServicoEncaminhado(BenificiarioEncaminhadoServicoEncaminhado entry) {
    return into(benificiarioEncaminhadoServicoEncaminhados).insert(entry);
  }
  Future<int> addBenificiarioEncaminhadoServicoEncaminhadoCompanion(BenificiarioEncaminhadoServicoEncaminhadosCompanion entry) {
    return into(benificiarioEncaminhadoServicoEncaminhados).insert(entry);
  }

  //update BenificiarioEncaminhadoServicoEncaminhado
  Future updateBenificiarioEncaminhadoServicoEncaminhado(BenificiarioEncaminhadoServicoEncaminhado entry) {
    return update(benificiarioEncaminhadoServicoEncaminhados).replace(entry);
  }
  Future updateBenificiarioEncaminhadoServicoEncaminhadoCompanion(BenificiarioEncaminhadoServicoEncaminhadosCompanion entry) {
    return update(benificiarioEncaminhadoServicoEncaminhados).replace(entry);
  }

  //delete all BenificiarioEncaminhadoServicoEncaminhados
  Future deleteAllBenificiarioEncaminhadoServicoEncaminhado() {
    // delete the oldest nine tasks
    return delete(benificiarioEncaminhadoServicoEncaminhados).go();
  }

  Future deleteBenificiarioEncaminhadoServicoEncaminhado(int entry) {
    // delete the oldest nine tasks
    return (delete(benificiarioEncaminhadoServicoEncaminhados)..where((t) => t.id.equals(entry))).go();
  }

  //get all BenificiarioEncaminhadoDocumentos
  Future<List<BenificiarioEncaminhadoDocumento>> get allBenificiarioEncaminhadoDocumentoEntries => select(benificiarioEncaminhadoDocumentos).get();
  Stream<List<BenificiarioEncaminhadoDocumento>> watchEntriesBenificiarioEncaminhadoDocumentos() {
    return (select(benificiarioEncaminhadoDocumentos)).watch();
  }

  Future<List<BenificiarioEncaminhadoDocumento>> allBenificiarioEncaminhadoDocumentosOrdered() {
    return (select(benificiarioEncaminhadoDocumentos)..orderBy([(t)=>OrderingTerm(expression: t.id)])).get();
  }
  //get BenificiarioEncaminhadoDocumento as id

  Stream<BenificiarioEncaminhadoDocumento> watchBenificiarioEncaminhadoDocumentoById(int id) {
    return (select(benificiarioEncaminhadoDocumentos)..where((t) => t.id.equals(id))).watchSingle();
  }
  Future<BenificiarioEncaminhadoDocumento> BenificiarioEncaminhadoDocumentoById(int id) {
    return (select(benificiarioEncaminhadoDocumentos)..where((t) => t.id.equals(id))).getSingle();
  }

  Stream<List<BenificiarioEncaminhadoDocumento>> watchBenificiarioEncaminhadoDocumentoByIdList(int id) {
    return (select(benificiarioEncaminhadoDocumentos)..where((t) => t.benificiarioEncaminhadoId.equals(id))).watch();
  }
  Future<List<BenificiarioEncaminhadoDocumento>> BenificiarioEncaminhadoDocumentoByIdList(int id) {
    return (select(benificiarioEncaminhadoDocumentos)..where((t) => t.benificiarioEncaminhadoId.equals(id))).get();
  }

  Future<List<Documento>> manyDocs(int id) async {
     List<Map<String,dynamic>>  b  =  [];
    var t = await customSelect(
        'SELECT * FROM documentos where  documentos.id in '+
            '(select documento_id from benificiario_encaminhado_documentos where benificiario_encaminhado_documentos.benificiario_encaminhado_id in'+
                ' (select id from benificiario_encaminhados where benificiario_encaminhado_id = ${id} ) ) '
    ).get();
    t.forEach((element) {
      b.add(element.data);
    });
      return List.generate(b.length, (index) {
      return Documento.fromData(b[index],attachedDatabase);
    });
  }

  //add BenificiarioEncaminhadoDocumento
  Future<int> addBenificiarioEncaminhadoDocumento(BenificiarioEncaminhadoDocumento entry) {
    return into(benificiarioEncaminhadoDocumentos).insert(entry);
  }
  Future<int> addBenificiarioEncaminhadoDocumentoCompanion(BenificiarioEncaminhadoDocumentosCompanion entry) {
    return into(benificiarioEncaminhadoDocumentos).insert(entry);
  }

  //update BenificiarioEncaminhadoDocumento
  Future updateBenificiarioEncaminhadoDocumento(BenificiarioEncaminhadoDocumento entry) {
    return update(benificiarioEncaminhadoDocumentos).replace(entry);
  }
  Future updateBenificiarioEncaminhadoDocumentoCompanion(BenificiarioEncaminhadoDocumentosCompanion entry) {
    return update(benificiarioEncaminhadoDocumentos).replace(entry);
  }

  //delete all BenificiarioEncaminhadoDocumentos
  Future deleteAllBenificiarioEncaminhadoDocumento() {
    // delete the oldest nine tasks
    return delete(benificiarioEncaminhadoDocumentos).go();
  }

  Future deleteBenificiarioEncaminhadoDocumento(int entry) {
    // delete the oldest nine tasks
    return (delete(benificiarioEncaminhadoDocumentos)..where((t) => t.id.equals(entry))).go();
  }
  //get all Proviniencias
  Future<List<Proviniencia>> get allProvinienciaEntries => select(proviniencias).get();
  Stream<List<Proviniencia>> watchEntriesProviniencias() {
    return (select(proviniencias)).watch();
  }

  Future<List<Proviniencia>> allProvinienciasOrdered() {
    return (select(proviniencias)..orderBy([(t)=>OrderingTerm(expression: t.nome)])).get();
  }
  //get Proviniencia as id

  Stream<Proviniencia> watchProvinienciaById(int id) {
    return (select(proviniencias)..where((t) => t.id.equals(id))).watchSingle();
  }
  Future<Proviniencia> ProvinienciaById(int id) {
    return (select(proviniencias)..where((t) => t.id.equals(id))).getSingle();
  }
  //add Proviniencia
  Future<int> addProviniencia(Proviniencia entry) {
    return into(proviniencias).insert(entry);
  }
  Future<int> addProvinienciaCompanion(ProvinienciasCompanion entry) {
    return into(proviniencias).insert(entry);
  }

  //update Proviniencia
  Future updateProviniencia(Proviniencia entry) {
    return update(proviniencias).replace(entry);
  }
  Future updateProvinienciaCompanion(ProvinienciasCompanion entry) {
    return update(proviniencias).replace(entry);
  }

  //delete all Proviniencias
  Future deleteAllProviniencia() {
    // delete the oldest nine tasks
    return delete(proviniencias).go();
  }

  Future deleteProviniencia(int entry) {
    // delete the oldest nine tasks
    return (delete(proviniencias)..where((t) => t.id.equals(entry))).go();
  }
}