import 'package:Biospdatabase/Routes/app_routes.dart';
import 'package:Biospdatabase/data/moor_database.dart';
import 'package:flutter/material.dart';
import 'package:Biospdatabase/data/moor_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class SearchData extends SearchDelegate<Benificiario>{
  String capitalize(String string) {
    if (string == null) {
      throw ArgumentError("string: $string");
    }

    if (string.isEmpty) {
      return string;
    }

    return string[0].toUpperCase() + string.substring(1);
  }


  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(icon: Icon(Icons.clear), onPressed: (){
        query = "";

      }),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(icon: AnimatedIcon(icon: AnimatedIcons.menu_arrow, progress: transitionAnimation), onPressed: (){
      close(context, null);
    });
  }

  @override
  Widget buildResults(BuildContext context) {
    // TODO: implement buildResults
    throw UnimplementedError();
  }

  Color _color(Benificiario ben){
    Color color = Colors.lightBlue;
    switch (ben.nome[0]){
      case 'A':color = Colors.grey;break;
      case 'B':color = Colors.orangeAccent;break;
      case 'C':color = Colors.lightGreen;break;
      case 'D':color = Colors.deepPurpleAccent;break;
      case 'E':color = Colors.blue;break;
      case 'F':color = Colors.brown;break;
      case 'G':color = Colors.lime;break;
      case 'H':color = Colors.lightGreenAccent;break;
      case 'I':color = Colors.indigoAccent;break;
      case 'J':color = Colors.cyan;break;
      case 'K':color = Colors.pink;break;
      case 'L':color = Colors.lightBlueAccent;break;
      case 'M':color = Colors.amber;break;
      case 'N':color = Colors.amberAccent;break;
      case 'O':color = Colors.deepOrangeAccent;break;
      case 'P':color = Colors.purple;break;
      case 'Q':color = Colors.redAccent;break;
      case 'U':color = Colors.blueGrey;break;
      case 'V':color = Colors.deepOrangeAccent;break;
      case 'W':color = Colors.brown;break;
      case 'X':color = Colors.yellow;break;
      case 'Y':color = Colors.yellowAccent;break;
      case 'Z':color = Colors.green;break;
    }
    return color;
  }
  @override
  Widget buildSuggestions(BuildContext context) {
    final benificiarios = Provider.of<MyDatabase>(context);
    
    //var cities =[];
    // final sugestions = query.isEmpty?cities:cities;// .where((q) => q.startsWith(query)).toList();   );
    return FutureBuilder<List<Benificiario>>(
      // ignore: missing_return
      future: (benificiarios.allBenificiarioEntries.then((value) {
        var aux = value;
        List<Benificiario> toReturn;
        toReturn = value.where(
                (el) {
              return (
                      el.nome.toLowerCase().startsWith(query.toLowerCase()) || el.id.toString().startsWith(query.toLowerCase())
              );
            }
        ).toList();
        return toReturn;
      })
      ),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return new ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (context, index) {
                return new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: ListTile(

                          leading: CircleAvatar(child:Text('${snapshot.data[index].nome[0].toUpperCase()}',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,
                                  ),
                                ),
                            backgroundColor: _color(snapshot.data[index]) ,),
                          title: Text(capitalize(snapshot.data[index].nome),
                            style: GoogleFonts.roboto(
                                fontWeight: FontWeight.w400
                            )),
                          subtitle: Text('${snapshot.data[index].id}'),
                          trailing: Container(
                            width: 100,
                            child: Row(
                              children: <Widget>[

                                IconButton(
                                    icon: Icon(Icons.edit), onPressed: () {
                                  Navigator.of(context).pushNamed(
                                      AppRoutes.Forms,
                                      arguments: snapshot.data[index]
                                  );
                                }),
                                IconButton(
                                    icon: Icon(Icons.delete), onPressed: () {
                                  //
                                  showDialog(
                                      context: context,
                                      builder: (ctx) =>
                                          AlertDialog(
                                            title: Text('Apagar registo'),
                                            content: Text('Tem certeza?'),
                                            actions: <Widget>[
                                              FlatButton(
                                                onPressed: () {
                                                  Navigator.of(context).pop();
                                                },
                                                child: Text('Não',
                                                  style: TextStyle(
                                                      color: Colors.blueGrey),),

                                              ),
                                              FlatButton(
                                                onPressed: () {
                                                  Navigator.of(context).pop();},
                                                child: Text('Sim',
                                                    style: TextStyle(
                                                        color: Colors
                                                            .blueGrey)),

                                              )
                                            ],
                                          )
                                  );
                                  //
                                })
                              ],
                            ),
                          ),
                          onTap: () {
                            Navigator.of(context).pushNamed(
                                AppRoutes.Forms,
                                arguments: snapshot.data[index]
                            );
                          },
                        ),
                        decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.5),
                          border: Border(bottom: BorderSide(
                              width: 1,
                              color: Colors.black54.withOpacity(.05)
                          )),
                        ),
                      ),
                    ]);
              });
        } else if (snapshot.hasError) {
          return new Text("${snapshot.error}");
        }
        return new Container(alignment: AlignmentDirectional.center,
          child: new CircularProgressIndicator(),);
      },
    );
  }

}