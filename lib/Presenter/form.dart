import 'package:Biospdatabase/data/moor_database.dart';
import 'package:intl/intl.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:international_phone_input/international_phone_input.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:Biospdatabase/Model/Benificiario.dart';

class Forms extends StatefulWidget {
  @override
  _FormsState createState() => _FormsState();

}

class _FormsState extends State<Forms>{
  final _form = GlobalKey<FormState>();
  int _currentIndex = 0;
  PageController _pageController;

  final List<MotivoDeAberturaDeProcesso> selectedValues = <MotivoDeAberturaDeProcesso>[];
  final List<ObjectivoDaVisita> objectivos = <ObjectivoDaVisita>[];
  final List<Documento> selectedDocs = <Documento>[];
  ServicoEncaminhado _selectedServ;
  Proviniencia _proviniencia;
  Provincia _provincia;
  Bairro _bairro;
  Sexo _sexo;
  ServicoEncaminhado ss;
  List<int> values = [1,0];
  int _acompanhamento;
  int _problema;
  String nome;
  int visita;
  DateTime dateBorn;
  DateTime dateAtend;
  DateTime dateRec;
  String contact;
  String espServ;


  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }
  int cast(String value){
    int sol = 0;
    if(value.length == 1){
      for(int i =0;i<10;i++){
        if(value[0] == '${i}'){
          sol = i;
        }
      }
    }else if(value.length == 2){
      for(int b =0;b<value.length;b++){
        for(int i =0;i<10;i++){
          if((b == 0) && value[0] == '${i}'){
            sol = i*10;
          }else if((b == 1) && value[1] == '${i}'){
            sol +=i;
          }
        }
      }
    }
    return sol;
  }

  @override
  Widget build(BuildContext context) {
    final database = Provider.of<MyDatabase>(context);
    final Map<String,String> _formData = {};
    Benificiario benificiario = ModalRoute.of(context).settings.arguments;
    String title = "Editar benificiario";
    String tooltip = "Actualizar benificiario";
    bool edit = true;
    BenificiarioResource json = new BenificiarioResource();
    if(benificiario.id!=null){
      title = benificiario.nome;
      json.maps(
        database,
        new Benificiario(
            id: benificiario.id,
            nome: benificiario.nome,
            ref: benificiario.ref,
            ObjectivoDaVistaId: benificiario.ObjectivoDaVistaId,
            bairroId: benificiario.bairroId,
            provinienciaId: benificiario.provinciaId,
            provinciaId: benificiario.provinciaId,
            sexoId: benificiario.sexoId,
            numeroDeVisita: benificiario.numeroDeVisita,
            dataDeNascimento: benificiario.dataDeNascimento,
            dataDeAtendimento: benificiario.dataDeAtendimento,
            createdAt: benificiario.createdAt,
            updatedAat: benificiario.updatedAat
        )
      ).then((value){
        print(value);
      });

    }else{
      benificiario  = null;
      title = "Novo benificiario";
      edit = false;
      tooltip  = "Novo benificiario";
    }


    final format = DateFormat("MM/dd/yyyy");

    String phoneNumber;
    String phoneIsoCode;
    bool visible = false;
    String confirmedNumber = '';

    void onPhoneNumberChange(String number, String internationalizedPhoneNumber, String isoCode) {
      print(internationalizedPhoneNumber);
      phoneNumber = number;
      contact = internationalizedPhoneNumber;
      phoneIsoCode = isoCode;
    }

    onValidPhoneNumber(
        String number, String internationalizedPhoneNumber, String isoCode) {
      visible = true;
      confirmedNumber = internationalizedPhoneNumber;
    }


    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(title,
        style: TextStyle(
          color: Colors.black54
        ),),
        actions: [
          Padding(
              padding: EdgeInsets.all(10),
              child:FloatingActionButton.extended(
                foregroundColor: Colors.orange,
                label:Text('Salvar'),
                tooltip: tooltip,
                icon: Icon(edit?Icons.update:Icons.sd_storage),
                backgroundColor: Color(0xFFFFEACC),
                onPressed: (){
                  final isValid = _form.currentState.validate();
                  if(isValid && (benificiario == null)){
                    _form.currentState.save();
                    database.addBenificiario(
                      Benificiario(
                          nome: nome,
                          provinciaId:_provincia.id,
                          provinienciaId: _proviniencia.id,
                          bairroId: _bairro.id,
                          sexoId: _sexo.id,
                          numeroDeVisita: visita,
                          dataDeNascimento: dateBorn,
                          dataDeAtendimento: dateAtend,
                          ObjectivoDaVistaId: objectivos.first.id,
                          createdAt: DateTime.now(),
                          updatedAat: DateTime.now()
                      )
                    ).then((value) {
                        database.addBenificiarioEncaminhado(
                            BenificiarioEncaminhado(
                                benificiarioId: value,
                                motivoDeAberturaDeProcesso: selectedValues.first.id,
                                necessitaDeAcompanhamenBairromiciliar: (_acompanhamento==1)?true:false,
                                dataQueFoiRecebido: dateRec,
                                problemaResolvido: (_problema == 1)?true:false,
                                createdAt: DateTime.now(),
                                updatedAt: DateTime.now()
                            )
                        ).then((value) {
                          selectedDocs.forEach((element) {
                            database.addBenificiarioEncaminhadoDocumento(
                                BenificiarioEncaminhadoDocumento(
                                    benificiarioEncaminhadoId: value,
                                    especificar: 'Normal',
                                    documentoId: element.id
                                    ,createdAt: DateTime.now(),
                                    updatedAt:DateTime.now()
                                )
                            );
                          });
                          selectedValues.forEach((element) {
                            database.addBenificiarioEncaminhadoServicoEncaminhado(
                                BenificiarioEncaminhadoServicoEncaminhado(
                                    benificiarioEncaminhadoId: value,
                                    servicoEncaminhadoId: element.id,
                                    especificar: espServ,
                                    createdAt: DateTime.now(),
                                    updatedAt: DateTime.now()
                                )
                            );
                          });
                        });
                        database.addContacto(
                          Contacto(
                              benificiarioId:value,
                              contacto: contact,
                              NomeDoResponsavel: nome,
                              createdAt: DateTime.now(),
                              updatedAt: DateTime.now()
                          )
                        );
                    });
                  }
                },
                elevation: 0.0,
              )

          )
        ],

        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.white,
      ),
      body: ListView(
        children: [
          Column(
            children: [
              Container(

                height: 250,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 250,
                      child: Container(
                        color: Colors.white,
                        height: 150,
                        child: CircleAvatar(
                          backgroundColor: Colors.orangeAccent,
                          child: (edit?Text("${benificiario.nome[0]}",style: TextStyle(fontSize: 75, color: Colors.white)):Icon(
                            FontAwesomeIcons.userPlus,
                            color: Colors.white,
                            size: 50
                          )),
                        ),
                      ),
                    ),

                  ],
                ),
              ),
              Divider(),
              Container(
                child:Form(
                    key: _form,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Padding(
                              padding: const EdgeInsets.only(
                                  left: 35, right: 35, top: 0),
                              child: TextFormField(
                                autofocus: true,
                                initialValue: benificiario!=null?benificiario.nome:'',
                                onSaved: (value){
                                  setState(() {
                                    nome = value;
                                  });

                                },
                                cursorColor: Theme.of(context).cursorColor,
                                maxLength: 20,
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.white,
                                  labelText: 'Nome Completo',
                                  labelStyle: TextStyle(
                                    color: Color(0xFF000000),
                                  ),
                                  //helperText: 'Helper text',
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                    BorderSide(color: Color(0xFF000000)),
                                  ),
                                ),
                              )),
                          Padding(
                              padding: const EdgeInsets.only(
                                  left: 35, right: 35, top: 0),
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                inputFormatters: <TextInputFormatter>[
                                  FilteringTextInputFormatter.digitsOnly
                                ],
                                cursorColor: Theme.of(context).cursorColor,
                                maxLength: 2,
                                onSaved: (String value){
                                  value = value.trim();

                                  visita  =  cast(value);
                                },
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.white,
                                  labelText: '1° Visita ou Frequência',
                                  labelStyle: TextStyle(
                                    color: Color(0xFF000000),
                                  ),
                                  //helperText: 'Helper text',
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                    BorderSide(color: Color(0xFF000000)),
                                  ),
                                ),
                              )),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 35, right: 35, top: 0),
                            child: FutureBuilder<List<Proviniencia>>(
                                future: database.allProvinienciaEntries,
                                builder: (BuildContext context,
                                    AsyncSnapshot<List<Proviniencia>> snapshot) {
                                  if (!snapshot.hasData)
                                    return CircularProgressIndicator();
                                  return DropdownButtonFormField<Proviniencia>(
                                    decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      labelStyle: TextStyle(
                                        color: Color(0xFF000000),
                                      ),
                                      //helperText: 'Helper text',
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide:
                                        BorderSide(color: Color(0xFF000000)),
                                      ),
                                    ),
                                    items: snapshot.data
                                        .map(
                                            (user) => DropdownMenuItem<Proviniencia>(
                                          child:  Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Icon(
                                                Icons.check,
                                                color: (_proviniencia == null) ? Colors.transparent : ( _proviniencia == user?null:Colors.transparent),
                                              ),
                                              SizedBox(width: 16),
                                              Text(user.nome),

                                            ],
                                          ),
                                          value: user,
                                        ))
                                        .toList(),
                                    onChanged: (Proviniencia value) {
                                      setState(() {
                                        _proviniencia = value;
                                      });
                                    },
                                    isExpanded: false,
                                    //value: _proviniencia,
                                    hint: Text('Proveniência'),
                                  );
                                }),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 35, right: 35, top: 20),
                            child: FutureBuilder<List<Provincia>>(
                                future: database.allProvinciaEntries,
                                builder: (BuildContext context,
                                    AsyncSnapshot<List<Provincia>> snapshot) {
                                  if (!snapshot.hasData)
                                    return CircularProgressIndicator();
                                  return DropdownButtonFormField<Provincia>(
                                    decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      labelStyle: TextStyle(
                                        color: Color(0xFF000000),
                                      ),
                                      //helperText: 'Helper text',
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide:
                                        BorderSide(color: Color(0xFF000000)),
                                      ),
                                    ),
                                    items: snapshot.data
                                        .map(
                                            (user) => DropdownMenuItem<Provincia>(

                                          child:  Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Icon(
                                                Icons.check,
                                                color: (_provincia == null) ? Colors.transparent : ( _provincia == user?null:Colors.transparent),),
                                              SizedBox(width: 6),
                                              Text(user.nome),
                                            ],
                                          ),
                                          value: user,
                                        ))
                                        .toList(),
                                    onChanged: (Provincia value) {
                                      setState(() {
                                        _provincia = value;
                                        print(_provincia);
                                      });
                                    },
                                    isExpanded: false,
                                    //value: _provincia,
                                    hint: Text('Província'),
                                  );
                                }),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 35, right: 35, top: 20),
                            child: FutureBuilder<List<Bairro>>(
                                future: database.allBairroEntries,
                                builder: (BuildContext context,
                                    AsyncSnapshot<List<Bairro>> snapshot) {
                                  if (!snapshot.hasData)
                                    return CircularProgressIndicator();
                                  return DropdownButtonFormField<Bairro>(
                                    decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      labelStyle: TextStyle(
                                        color: Color(0xFF000000),
                                      ),
                                      //helperText: 'Helper text',
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide:
                                        BorderSide(color: Color(0xFF000000)),
                                      ),
                                    ),
                                    items: snapshot.data
                                        .map(
                                            (user) => DropdownMenuItem<Bairro>(

                                          child:  Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Icon(Icons.check,
                                                color: (_bairro == null) ? Colors.transparent : ( _bairro == user?null:Colors.transparent),),
                                              SizedBox(width: 16),
                                              Text(user.nome),
                                            ],
                                          ),
                                          value: user,
                                        ))
                                        .toList(),
                                    onChanged: (Bairro value) {
                                      setState(() {
                                        _bairro = value;
                                        print(_bairro);
                                      });
                                    },
                                    isExpanded: false,
                                    //value: _provincia,
                                    hint: Text('Bairro'),
                                  );
                                }),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 35, right: 35, top: 20),
                            child: FutureBuilder<List<Sexo>>(
                                future: database.allSexoEntries,
                                builder: (BuildContext context,
                                    AsyncSnapshot<List<Sexo>> snapshot) {
                                  if (!snapshot.hasData)
                                    return CircularProgressIndicator();
                                  return DropdownButtonFormField<Sexo>(
                                    decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      labelStyle: TextStyle(
                                        color: Color(0xFF000000),
                                      ),
                                      //helperText: 'Helper text',
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide:
                                        BorderSide(color: Color(0xFF000000)),
                                      ),
                                    ),
                                    items: snapshot.data
                                        .map(
                                            (user) => DropdownMenuItem<Sexo>(

                                          child:  Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Icon(Icons.check,
                                                color: (_sexo == null) ? Colors.transparent : ( _sexo == user?null:Colors.transparent),),
                                              SizedBox(width: 6),
                                              Text(user.nome),
                                            ],
                                          ),
                                          value: user,
                                        ))
                                        .toList(),
                                    onChanged: (Sexo value) {
                                      setState(() {
                                        _sexo = value;
                                        print(_sexo);
                                      });
                                    },
                                    isExpanded: false,
                                    //value: _provincia,
                                    hint: Text('Sexo'),
                                  );
                                }),
                          ),
                          Padding(
                              padding: const EdgeInsets.only(
                                  left: 35, right: 35, top: 20),
                              child: DateTimeField(
                                format: format,
                                onShowPicker: (context, currentValue) {
                                  //print(currentValue);
                                  return showDatePicker(
                                      context: context,
                                      firstDate: DateTime(1900),
                                      initialDate: currentValue ?? DateTime.now(),
                                      lastDate: DateTime(2100));
                                },
                                onChanged:(DateTime time){
                                  setState(() {
                                    dateBorn = time;
                                  });
                                },
                                onSaved: (set){
                                  dateBorn = set;
                                },

                                decoration: InputDecoration(
                                  filled: true,
                                  hintText:'Data de Nascimento',
                                  fillColor: Colors.white,
                                  labelStyle: TextStyle(
                                    color: Color(0xFF000000),
                                  ),
                                  //helperText: 'Helper text',
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                    BorderSide(color: Color(0xFF000000)),
                                  ),
                                ),
                              )
                          ),
                          Padding(padding: const EdgeInsets.only(left: 35,right: 35,top: 20),
                            child: InternationalPhoneInput(
                              decoration: InputDecoration.collapsed(
                                hintText:'Contacto',
                                fillColor: Colors.white,
                              ),
                              onPhoneNumberChange: onPhoneNumberChange,
                              initialPhoneNumber: phoneNumber,
                              initialSelection: phoneIsoCode,
                              enabledCountries: ['+258'],
                              showCountryCodes: true,
                              showCountryFlags: true,
                            ),),
                          Padding(
                              padding: const EdgeInsets.only(
                                  left: 35, right: 35, top: 0),
                              child: DateTimeField(
                                format: format,
                                onShowPicker: (context, currentValue) {
                                  //print(currentValue);
                                  return showDatePicker(
                                      context: context,
                                      firstDate: DateTime(1900),
                                      initialDate: currentValue ?? DateTime.now(),
                                      lastDate: DateTime(2100));
                                },
                                onChanged:(DateTime time){
                                  setState(() {
                                    dateAtend = time;
                                  });
                                }
                                ,
                                onSaved: (DateTime time){
                                  setState(() {
                                    dateAtend = time;
                                  });
                                },

                                decoration: InputDecoration(
                                  filled: true,
                                  hintText:'Data de atendimento',
                                  fillColor: Colors.white,
                                  labelStyle: TextStyle(
                                    color: Color(0xFF000000),
                                  ),
                                  //helperText: 'Helper text',
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                    BorderSide(color: Color(0xFF000000)),
                                  ),
                                ),
                              )
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 35, right: 35, top: 20),
                            child: FutureBuilder<List<ObjectivoDaVisita>>(
                              future:   database.allObjectivoDaVisitaEntries,
                              // ignore: missing_return
                              builder: (BuildContext context,AsyncSnapshot<List<ObjectivoDaVisita>> snapshot){
                                if (!snapshot.hasData)return CircularProgressIndicator();

                                return DropdownButtonFormField<ObjectivoDaVisita>(
                                  value: objectivos.isEmpty?null:objectivos.last,
                                  onChanged: (ObjectivoDaVisita newValue){
                                    setState(() {
                                      if (objectivos.contains(newValue)){
                                        print(newValue);
                                        objectivos.remove(newValue);
                                      }
                                      else{
                                        objectivos.add(newValue);
                                        print(objectivos);
                                      }
                                      // print(newValue);

                                      //print(selectedValues);
                                    });

                                  },
                                  items: snapshot.data.map<DropdownMenuItem<ObjectivoDaVisita>>(
                                          (ObjectivoDaVisita value){
                                        return DropdownMenuItem<ObjectivoDaVisita>(
                                          value: value,
                                          child: Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Icon(
                                                Icons.playlist_add_check,
                                                color: objectivos.contains(value) ? null : Colors.transparent,
                                              ),
                                              SizedBox(width: 16),
                                              Text(value.nome),
                                            ],
                                          ),
                                        );
                                      }
                                  ).toList(),
                                  decoration:InputDecoration(
                                    filled:true,
                                    fillColor: Colors.white,
                                    labelText: 'Objectivo(s) da visita',
                                    labelStyle: TextStyle(
                                      color: Color(0xFF000000),
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: Color(0xFF000000)),
                                    ),
                                  ) ,
                                );

                              },
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 35, right: 35, top: 20),
                            child: FutureBuilder<List<MotivoDeAberturaDeProcesso>>(
                              future:   database.allMotivoDeAberturaDeProcessoEntries,
                              // ignore: missing_return
                              builder: (BuildContext context,AsyncSnapshot<List<MotivoDeAberturaDeProcesso>> snapshot){
                                if (!snapshot.hasData)return CircularProgressIndicator();

                                return DropdownButtonFormField<MotivoDeAberturaDeProcesso>(
                                  value: selectedValues.isEmpty?null:selectedValues.last,
                                  onChanged: (MotivoDeAberturaDeProcesso newValue){
                                    setState(() {
                                      if (selectedValues.contains(newValue)){
                                        print(newValue);
                                        selectedValues.remove(newValue);
                                      }
                                      else{
                                        selectedValues.add(newValue);
                                        print(selectedValues);
                                      }
                                      // print(newValue);

                                      //print(selectedValues);
                                    });

                                  },
                                  onSaved: (das){
                                    setState(() {
                                      selectedValues;
                                    });
                                  },
                                  items: snapshot.data.map<DropdownMenuItem<MotivoDeAberturaDeProcesso>>(
                                          (MotivoDeAberturaDeProcesso value){
                                        return DropdownMenuItem<MotivoDeAberturaDeProcesso>(
                                          value: value,
                                          child: Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Icon(
                                                Icons.playlist_add_check,
                                                color: selectedValues.contains(value) ? null : Colors.transparent,
                                              ),
                                              SizedBox(width: 16),
                                              Text(value.nome),
                                            ],
                                          ),
                                        );
                                      }
                                  ).toList(),
                                  decoration:InputDecoration(
                                    filled:true,
                                    fillColor: Colors.white,
                                    labelText: 'Motivo de abertura do processo',
                                    labelStyle: TextStyle(
                                      color: Color(0xFF000000),
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: Color(0xFF000000)),
                                    ),
                                  ) ,
                                );

                              },
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 35, right: 35, top: 20),
                            child: FutureBuilder<List<Documento>>(
                              future:   database.allDocumentoEntries,
                              // ignore: missing_return
                              builder: (BuildContext context,AsyncSnapshot<List<Documento>> snapshot){
                                if (!snapshot.hasData)return CircularProgressIndicator();

                                return DropdownButtonFormField<Documento>(
                                  value: selectedDocs.isEmpty?null:selectedDocs.last,
                                  onChanged: (Documento newValue){
                                    setState(() {
                                      if (selectedDocs.contains(newValue)){
                                        print(newValue);
                                        selectedDocs.remove(newValue);
                                      }
                                      else{
                                        selectedDocs.add(newValue);
                                        print(selectedDocs);
                                      }
                                      // print(newValue);

                                      //print(selectedValues);
                                    });

                                  },
                                  items: snapshot.data.map<DropdownMenuItem<Documento>>(
                                          (Documento value){
                                        return DropdownMenuItem<Documento>(
                                          value: value,
                                          child: Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Icon(
                                                Icons.playlist_add_check,
                                                color: selectedDocs.contains(value) ? null : Colors.transparent,
                                              ),
                                              SizedBox(width: 16),
                                              Text(value.nome),
                                            ],
                                          ),
                                        );
                                      }
                                  ).toList(),
                                  decoration:InputDecoration(
                                    filled:true,
                                    fillColor: Colors.white,
                                    labelText: 'Documentos necessários',
                                    labelStyle: TextStyle(
                                      color: Color(0xFF000000),
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: Color(0xFF000000)),
                                    ),
                                  ) ,
                                );

                              },
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 35, right: 35, top: 20),
                            child: FutureBuilder<List<ServicoEncaminhado>>(
                                future: database.allServicoEncaminhadoEntries,
                                builder: (BuildContext context,
                                    AsyncSnapshot<List<ServicoEncaminhado>> snapshot) {
                                  if (!snapshot.hasData)
                                    return CircularProgressIndicator();
                                  return DropdownButtonFormField<ServicoEncaminhado>(
                                    decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      labelStyle: TextStyle(
                                        color: Color(0xFF000000),
                                      ),
                                      //helperText: 'Helper text',
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide:
                                        BorderSide(color: Color(0xFF000000)),
                                      ),
                                    ),
                                    items: snapshot.data
                                        .map(
                                            (user) => DropdownMenuItem<ServicoEncaminhado>(

                                          child:  Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Icon(
                                                Icons.check,
                                                color: (_selectedServ == null) ? Colors.transparent : ( _selectedServ == user?null:Colors.transparent),
                                              ),
                                              SizedBox(width: 16),
                                              Text(user.nome),

                                            ],
                                          ),
                                          value: user,
                                        ))
                                        .toList(),
                                    onChanged: (ServicoEncaminhado value) {
                                      setState(() {
                                        _selectedServ = value;
                                        print(_selectedServ);
                                      });

                                    },
                                    isExpanded: false,
                                    //value: _proviniencia,
                                    hint: Text('Especificar Serviço'),
                                  );
                                }),
                          ),
                          Padding(
                              padding: const EdgeInsets.only(
                                  left: 35, right: 35, top: 20),
                              child: TextFormField(
                                onSaved: (String espc){
                                  espServ = espc;
                                },
                                onChanged: (String espc){
                                  espServ = espc;
                                },
                                cursorColor: Theme.of(context).cursorColor,
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.white,
                                  labelText: 'Especificar Serviço Encaminhado',
                                  labelStyle: TextStyle(
                                    color: Color(0xFF000000),
                                  ),
                                  //helperText: 'Helper text',
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                    BorderSide(color: Color(0xFF000000)),
                                  ),
                                ),
                              )),
                          Padding(
                              padding: const EdgeInsets.only(
                                  left: 35, right: 35, top: 20),
                              child: DropdownButtonFormField<int>(
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.white,
                                  labelStyle: TextStyle(
                                    color: Color(0xFF000000),
                                  ),
                                  //helperText: 'Helper text',
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                    BorderSide(color: Color(0xFF000000)),
                                  ),
                                ),
                                items: values
                                    .map(
                                        (user) => DropdownMenuItem<int>(
                                      child:  Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          Icon(Icons.check,
                                              color: (_acompanhamento  == null) ? Colors.transparent :( (_acompanhamento == user)?null:Colors.transparent)),
                                          SizedBox(width: 6),
                                          Text("${(user == 1)?"Sim":"Não"}"),

                                        ],
                                      ),
                                      value: user,
                                    ))
                                    .toList(),
                                onChanged: (int value) {
                                  setState(() {
                                    _acompanhamento  =  value;
                                    print(_acompanhamento);
                                  });
                                },
                                isExpanded: false,
                                //value: _provincia,
                                hint: Text('Acompanhamento'),
                              )

                          ),
                          Padding(
                              padding: const EdgeInsets.only(
                                  left: 35, right: 35, top: 20),
                              child: DateTimeField(
                                format: format,
                                onShowPicker: (context, currentValue) {
                                  //print(currentValue);
                                  return showDatePicker(
                                      context: context,
                                      firstDate: DateTime(1900),
                                      initialDate: currentValue ?? DateTime.now(),
                                      lastDate: DateTime(2100));
                                },
                                onChanged:(DateTime time){
                                  setState(() {
                                    dateRec = time;
                                  });
                                },
                                onSaved: (DateTime time){
                                  dateRec = time;
                                },
                                decoration: InputDecoration(
                                  filled: true,
                                  hintText:'Data em que foi recebida pelo serviço',
                                  fillColor: Colors.white,
                                  labelStyle: TextStyle(
                                    color: Color(0xFF000000),
                                  ),
                                  //helperText: 'Helper text',
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                    BorderSide(color: Color(0xFF000000)),
                                  ),
                                ),
                              )
                          ),
                          Padding(
                              padding: const EdgeInsets.only(
                                  left: 35, right: 35, top: 20,bottom: 50),
                              child: DropdownButtonFormField<int>(
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.white,
                                  labelStyle: TextStyle(
                                    color: Color(0xFF000000),
                                  ),
                                  //helperText: 'Helper text',
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                    BorderSide(color: Color(0xFF000000)),
                                  ),
                                ),
                                items: values
                                    .map(
                                        (user) => DropdownMenuItem<int>(
                                      child:  Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          Icon(Icons.check,
                                              color: (_problema  == null) ? Colors.transparent :( (_problema == user)?null:Colors.transparent)),
                                          SizedBox(width: 6),
                                          Text("${(user == 1)?"Sim":"Não"}"),
                                        ],
                                      ),
                                      value: user,
                                    ))
                                    .toList(),
                                onChanged: (int value) {
                                  setState(() {
                                    _problema  =  value;
                                    print(_problema);
                                  });
                                },
                                isExpanded: false,
                                //value: _provincia,
                                hint: Text('Problema resolvido?'),
                              )

                          ),
                        ])),
              )
            ],
          )
        ],
      ),
    );
  }
}
