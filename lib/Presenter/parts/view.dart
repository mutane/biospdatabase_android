import 'package:Biospdatabase/data/moor_database.dart';
import 'package:intl/intl.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:path/path.dart';
import 'package:provider/provider.dart';
import 'package:international_phone_input/international_phone_input.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:url_launcher/url_launcher.dart' as url2;

class ViewBen extends StatefulWidget {
  @override
  _ViewBenState createState() => _ViewBenState();

}

class _ViewBenState extends State<ViewBen>{


  @override
  Widget build(BuildContext context) {
    Benificiario benificiario = ModalRoute.of(context).settings.arguments;
    final database  = Provider.of<MyDatabase>(context);
    List<int> ts = [];
    for(int i=0;i<40;i++){
      ts.add(857607095+i);
    }
    Color color = Colors.lightBlue;
    print(benificiario);
    if(benificiario.id!=null) {
      switch (benificiario.nome[0]) {
        case 'A':
          color = Colors.grey;
          break;
        case 'B':
          color = Colors.orangeAccent;
          break;
        case 'C':
          color = Colors.lightGreen;
          break;
        case 'D':
          color = Colors.deepPurpleAccent;
          break;
        case 'E':
          color = Colors.blue;
          break;
        case 'F':
          color = Colors.brown;
          break;
        case 'G':
          color = Colors.lime;
          break;
        case 'H':
          color = Colors.lightGreenAccent;
          break;
        case 'I':
          color = Colors.indigoAccent;
          break;
        case 'J':
          color = Colors.cyan;
          break;
        case 'K':
          color = Colors.pink;
          break;
        case 'L':
          color = Colors.lightBlueAccent;
          break;
        case 'M':
          color = Colors.amber;
          break;
        case 'N':
          color = Colors.amberAccent;
          break;
        case 'O':
          color = Colors.deepOrangeAccent;
          break;
        case 'P':
          color = Colors.purple;
          break;
        case 'Q':
          color = Colors.redAccent;
          break;
        case 'U':
          color = Colors.blueGrey;
          break;
        case 'V':
          color = Colors.deepOrangeAccent;
          break;
        case 'W':
          color = Colors.brown;
          break;
        case 'X':
          color = Colors.yellow;
          break;
        case 'Y':
          color = Colors.yellowAccent;
          break;
        case 'Z':
          color = Colors.green;
          break;
      }
    }
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text((benificiario.id!=null)?benificiario.nome:"Novo registo",
            style: TextStyle(
                color: Colors.black54
            ),
          ),
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: Colors.white,
          actions: [
            Padding(
                padding: EdgeInsets.all(10),
                child:FloatingActionButton.extended(
                  foregroundColor: Colors.orange,
                  label:Text('Editar'),
                  tooltip: 'Editar Benificiario',
                  icon: Icon(Icons.mode_edit),
                  backgroundColor: Color(0xFFFFEACC),
                  onPressed: (){},
                  elevation: 0.0,
                )

            ),
            IconButton(onPressed: (){}, icon: Icon(Icons.delete,color: Colors.red,))
          ],
        ),
        body: ListView(
            children: [
              Column(
                children: [
                  Container(
                    height: 200,
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 200,
                          child: Container(
                            color: Colors.white,
                            height: 150,
                            child: CircleAvatar(
                              backgroundColor: Colors.orangeAccent,
                              child: (Text("${benificiario.nome[0]}",style: TextStyle(fontSize: 75, color: Colors.white))),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  ListTile(
                    title: Text("\t" + benificiario.nome,style: TextStyle(
                        color: Colors.black54,
                        fontWeight: FontWeight.bold,
                        fontSize: 20

                    ),),
                  ),
                  Divider(),
                  Card(
                    margin: EdgeInsets.only(left: 10,right: 10),
                    child:  FutureBuilder<List<Contacto>>(
                      future:  database.allContactoEntries,
                      builder: (BuildContext context,AsyncSnapshot<List<Contacto>> snapshot){
                        if(snapshot.data!=null){
                          var t = snapshot.data.where((element) {
                            return (element.benificiarioId == benificiario.id);
                          });
                          Contacto s;
                          if(t.length!=0){
                            s = t.first;
                          }
                          print(t);
                          return ListTile(
                            title:Text(s!=null?s.contacto:""),
                            subtitle: Text('Contacto'),
                            leading: IconButton(icon: Icon(Icons.call,color: Colors.orange), onPressed: (){
                              url2.launch("open?addresses=${s.contacto}");
                            }),
                            trailing: IconButton(icon: Icon(Icons.message,color: Colors.orange), onPressed: (){
                              url2.launch("sms:/${s.contacto}");
                            }),
                          );
                        }
                        return ListTile(
                          title:Text("Sem contacto"),
                          subtitle: Text('Contacto'),
                          leading: IconButton(icon: Icon(Icons.call,color: Colors.orange), onPressed: null),
                          trailing: IconButton(icon: Icon(Icons.message,color: Colors.orange), onPressed: null),
                        );


                      },
                    ),

                  ),
                  Card(
                    child: Column(
                      children:<Widget>[

                      ]
                    )
                  )
                  
                ],
              ),]
        )
    );
  }

}


/**
 *  FlatButton(
    onPressed: () {
    //print(benificiario);
    },
    child:Padding(
    padding: EdgeInsets.all(10),
    child:FloatingActionButton.extended(
    foregroundColor: Colors.red,
    label:Text('Deletar'),
    tooltip: 'Deletar Benificiario',
    icon: Icon(Icons.delete),
    backgroundColor: Color(0xFFFFEACC),
    onPressed: (){},
    elevation: 0.0,
    )

    )
    )
 *
 * **/