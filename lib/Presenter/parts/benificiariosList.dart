

import 'package:Biospdatabase/Model/Benificiario.dart';
import 'package:Biospdatabase/Presenter/parts/view.dart';
import 'package:Biospdatabase/Routes/app_routes.dart';
import 'package:Biospdatabase/data/moor_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import '../form.dart';
import '../searchData.dart';
import 'drawer.dart';
import 'newBen.dart';

class BenificiariosList extends StatefulWidget {
  @override
  _BenificiariosListState createState() => _BenificiariosListState();

}
class _BenificiariosListState extends State<BenificiariosList>{
  @override
  Widget build(BuildContext context) {
   return Scaffold(
     appBar: AppBar(
       leading: IconButton(icon: Icon(Icons.menu), onPressed: (){
         Scaffold.of(context).openDrawer();
       }),
       title: Text('Biosp Database',style: TextStyle(
           color: Colors.black
       ),),
       iconTheme: IconThemeData(
           color: Colors.black
       ),
       backgroundColor: Colors.white,
       actions: <Widget>[
         IconButton(tooltip: 'Pesquisar',icon: Icon(Icons.search),onPressed: (){
           showSearch(context: context, delegate: SearchData());
         }),
       ],
     ),
     body:  Container(
       child: _buildListBenificiario(context),
     ),
     drawer: MyDrawer(),
          floatingActionButton: FloatingActionButton(
         child: Icon(FontAwesomeIcons.userPlus),
         foregroundColor: Colors.orange,
         backgroundColor: Color(0xFFFFEACC),
         onPressed: (){
           Navigator.of(context).push(_createRoute(new Benificiario()));
         }
         ),
   );


  }
}
StreamBuilder<List<Benificiario>> _buildListBenificiario(BuildContext context){

  final database  = Provider.of<MyDatabase>(context);
  //  database.addBairro(Bairro( nome: 'Dondo', createdAt: DateTime.now(), updatedAt:  DateTime.now()));
 /*   database.allBairroEntries.then((value) => print(value));
      database.addProvincia(Provincia( nome: 'Gaza', createdAt: DateTime.now(), updatedAt: DateTime.now()));
      database.allProvinciaEntries.then((value) => print(value));
      database.addProviniencia(Proviniencia(nome: 'Pre-Escolar', createdAt: DateTime.now(), updatedAt: DateTime.now()));
      database.allProvinienciaEntries.then((value) => print(value));
      database.addSexo(Sexo( nome: 'Feminino', createdAt: DateTime.now(), updatedAt: DateTime.now()));
      database.allSexoEntries.then((value) => print(value));
      database.addDocumento(Documento( nome: 'B.I', createdAt: DateTime.now(), updatedAt: DateTime.now()));
      database.allDocumentoEntries.then((value) => print(value));
      database.addMotivoDeAberturaDeProcesso(MotivoDeAberturaDeProcesso(nome: 'Tratar Dir', createdAt: DateTime.now(), updatedAt: DateTime.now()));
      database.allMotivoDeAberturaDeProcessoEntries.then((value) => print(value));
      database.addServicoEncaminhado(ServicoEncaminhado(nome: 'Educ', createdAt: DateTime.now(), updatedAt: DateTime.now()));
      database.allServicoEncaminhadoEntries.then((value) => print(value));
      database.addObjectivoDaVisita(ObjectivoDaVisita(nome: 'Tratar BI', createdAt:DateTime.now() , updatedAt: DateTime.now()));
      database.allObjectivoDaVisitaEntries.then((value) => print(value));
      database.addBenificiario(Benificiario(nome: 'Eldemar Mussa',  numeroDeVisita: 2, dataDeNascimento:DateTime.now(),provinciaId:1,bairroId: 1, sexoId: 1, provinienciaId: 1,ObjectivoDaVistaId: 1,dataDeAtendimento: DateTime.now(), createdAt: DateTime.now(), updatedAat: DateTime.now()));
      database.allBenificiarioEntries.then((value) => print(value));
      database.addBenificiarioEncaminhado(BenificiarioEncaminhado(necessitaDeAcompanhamenBairromiciliar: true,benificiarioId: 1, dataQueFoiRecebido: DateTime.now(), problemaResolvido: true, createdAt: DateTime.now(), updatedAt: DateTime.now()));
      database.allBenificiarioEncaminhadoEntries.then((value) => print(value));
      database.addContacto(Contacto(contacto: '+258847607095', NomeDoResponsavel: 'Inkomo', benificiarioId: 1,createdAt: DateTime.now(), updatedAt: DateTime.now()));
      database.allContactoEntries.then((value) => print(value));
      database.addBenificiarioEncaminhadoDocumento(BenificiarioEncaminhadoDocumento(especificar: 'FIP', benificiarioEncaminhadoId: 1,documentoId: 1,createdAt: DateTime.now(), updatedAt: DateTime.now()));
      database.allBenificiarioEncaminhadoDocumentoEntries.then((value) => print(value));
      database.addBenificiarioEncaminhadoServicoEncaminhado(BenificiarioEncaminhadoServicoEncaminhado(especificar: 'Dic',benificiarioEncaminhadoId:1,servicoEncaminhadoId:1, createdAt: DateTime.now(), updatedAt: DateTime.now()));
      database.allBenificiarioEncaminhadoServicoEncaminhadoEntries.then((value) => print(value));
*/
    return StreamBuilder(
        stream:database.watchEntriesBenificiarios(),
        // ignore: missing_return
        builder: (context,AsyncSnapshot<List<Benificiario>> snapshot){
          final benificiarios = snapshot.data ?? List();
          return ListView.builder(
            itemCount: benificiarios.length,
            itemBuilder: (_,index) {
              final itemBenificiario = benificiarios[index];
              BenificiarioResource data;
              return _builListBen(itemBenificiario,database,context);
            },
          );
        }
    );
  }


Widget _builListBen(Benificiario benificiario,MyDatabase database,BuildContext context){
  print(benificiario.nome);
  String l = benificiario.nome[0].toUpperCase();
  Color color = Colors.lightBlue;
  switch (l){
    case 'A':color = Colors.grey;break;
    case 'B':color = Colors.orangeAccent;break;
    case 'C':color = Colors.lightGreen;break;
    case 'D':color = Colors.deepPurpleAccent;break;
    case 'E':color = Colors.blue;break;
    case 'F':color = Colors.brown;break;
    case 'G':color = Colors.lime;break;
    case 'H':color = Colors.lightGreenAccent;break;
    case 'I':color = Colors.indigoAccent;break;
    case 'J':color = Colors.cyan;break;
    case 'K':color = Colors.pink;break;
    case 'L':color = Colors.lightBlueAccent;break;
    case 'M':color = Colors.amber;break;
    case 'N':color = Colors.amberAccent;break;
    case 'O':color = Colors.deepOrangeAccent;break;
    case 'P':color = Colors.purple;break;
    case 'Q':color = Colors.redAccent;break;
    case 'U':color = Colors.blueGrey;break;
    case 'V':color = Colors.deepOrangeAccent;break;
    case 'W':color = Colors.brown;break;
    case 'X':color = Colors.yellow;break;
    case 'Y':color = Colors.yellowAccent;break;
    case 'Z':color = Colors.green;break;
  }
  return Slidable(
    actionPane: SlidableDrawerActionPane(),
    actionExtentRatio: 0.10,
    child: Column(
      children: <Widget> [
        Container(
          child: ListTile(
              leading: CircleAvatar(
                child: Text('${l}',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
                backgroundColor: color,
              ),
              title: Text((benificiario.nome),style: GoogleFonts.roboto(
                  fontWeight: FontWeight.w400
              )),
              subtitle: Text('${benificiario.id}'),
              trailing:Container(
                width: 150,
                child: Row(
                  children: <Widget>[
                    IconButton(icon: Icon(Icons.remove_red_eye),onPressed: (){
                      Navigator.of(context).push(_viewBen(benificiario));
                    }),
                    IconButton(icon: Icon(Icons.edit),onPressed: (){
                      Navigator.of(context).push(_createRoute(benificiario));
                    }),
                    IconButton(icon: Icon(Icons.delete), onPressed: (){
                      showDialog(
                          context: context,
                          builder: (ctx) =>AlertDialog(
                            title: Text('Apagar registo'),
                            content: Text('Tem certeza?'),
                            actions: <Widget>[
                              FlatButton(
                                onPressed: () { Navigator.of(context).pop();},
                                child: Text('Não',style: TextStyle(color: Colors.blueGrey),),

                              ),
                              FlatButton(
                                onPressed: (){
                                  Navigator.of(context).pop();
                                  database.deleteBenificiario(benificiario.id);
                                  //Provider.of<Users>(context,listen: false).deleteUser(snapshot.data[index]);
                                },
                                child: Text('Sim',style: TextStyle(color: Colors.blueGrey)),

                              )
                            ],
                          )
                      );
                    })
                  ],
                ),
              )
          ),
          decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.5),
            border: Border(bottom: BorderSide(
                width: 1,
                color: Colors.black54.withOpacity(.05)
            )),
          ),
        )
      ],
    ),
  );
}

Route _createRoute(args) {
  return PageRouteBuilder(

    pageBuilder: (context, animation, secondaryAnimation) => Forms(),
    settings: RouteSettings(
        arguments: args
    ),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = Offset(2.0, 0.0);
      var end = Offset.zero;
      var curve = Curves.ease;
      var tween = Tween(begin:begin,end:end).chain(CurveTween(curve: curve));
      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}

Route _newBen() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => NewBenificiario(),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = Offset(2.0, 0.0);
      var end = Offset.zero;
      var curve = Curves.ease;
      var tween = Tween(begin:begin,end:end).chain(CurveTween(curve: curve));
      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}
Route _viewBen(args) {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => ViewBen(),
    settings: RouteSettings(
        arguments:args

    ),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = Offset(2.0, 0.0);
      var end = Offset.zero;
      var curve = Curves.ease;
      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}