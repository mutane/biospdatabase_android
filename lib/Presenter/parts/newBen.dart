import 'package:Biospdatabase/data/moor_database.dart';
import 'package:intl/intl.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:international_phone_input/international_phone_input.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
class NewBenificiario extends StatefulWidget {
  @override
  _NewBenificiarioState createState() => _NewBenificiarioState();

}

class _NewBenificiarioState extends State<NewBenificiario>{
  final _form = GlobalKey<FormState>();
  int _currentIndex = 0;
  PageController _pageController;

  final List<MotivoDeAberturaDeProcesso> selectedValues = <MotivoDeAberturaDeProcesso>[];
  final List<Documento> selectedDocs = <Documento>[];
  ServicoEncaminhado _selectedServ;
  Proviniencia _proviniencia;
  Provincia _provincia;
  Bairro _bairro;
  Sexo _sexo;
  ServicoEncaminhado ss;
  // List<bool> _acompanhamentoList = <bool>[];
  List<int> values = [1,0];
  int _acompanhamento;
  int _problema;
  //Documento _documento;

  //final Proviniencia _selectedProv = null;
  //final List<String> selected = <String>[];

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    final database = Provider.of<MyDatabase>(context);
    Benificiario benificiario = ModalRoute.of(context).settings.arguments;
    print(benificiario);
    if( benificiario!= null){

    }

    final format = DateFormat("MM/dd/yyyy");

    String phoneNumber;
    String phoneIsoCode;
    bool visible = false;
    String confirmedNumber = '';

    void onPhoneNumberChange(
        String number, String internationalizedPhoneNumber, String isoCode) {
      print(number);
      phoneNumber = number;
      phoneIsoCode = isoCode;
    }

    onValidPhoneNumber(
        String number, String internationalizedPhoneNumber, String isoCode) {
      visible = true;
      confirmedNumber = internationalizedPhoneNumber;
    }


    return Scaffold(
      backgroundColor: Colors.blueAccent,
      appBar: AppBar(
        actions: [
          FlatButton(
            onPressed: () {
             // print(benificiario);
            },
            child: Text(
              "GUARDAR",
            ),
          )
        ],
        title: Text(
          'Criar novo registo',
          style: TextStyle(color: Colors.black),
        ),
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.white,
      ),
      body: ListView(
        children: [
          Column(
            children: [
              Container(

                height: 250,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 100,
                      child: Container(
                        color: Colors.blueAccent,
                        height: 250,
                        child: CircleAvatar(
                          backgroundColor: Colors.transparent,
                          child: Icon(Icons.person,size: 250,color: Colors.white,),
                        ),
                      ),
                    ),

                  ],
                ),
              ),
              Container(

                child:Form(
                    key: _form,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Padding(
                              padding: const EdgeInsets.only(
                                  left: 35, right: 35, top: 30),
                              child: TextField(
                                cursorColor: Theme.of(context).cursorColor,
                                maxLength: 20,
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.white,
                                  labelText: 'Nome Completo',
                                  labelStyle: TextStyle(
                                    color: Color(0xFF000000),
                                  ),
                                  //helperText: 'Helper text',
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                    BorderSide(color: Color(0xFF000000)),
                                  ),
                                ),
                              )),
                          Padding(
                              padding: const EdgeInsets.only(
                                  left: 35, right: 35, top: 0),
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                inputFormatters: <TextInputFormatter>[
                                  FilteringTextInputFormatter.digitsOnly
                                ],
                                cursorColor: Theme.of(context).cursorColor,
                                maxLength: 20,
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.white,
                                  labelText: '1° Visita ou Frequência',
                                  labelStyle: TextStyle(
                                    color: Color(0xFF000000),
                                  ),
                                  //helperText: 'Helper text',
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                    BorderSide(color: Color(0xFF000000)),
                                  ),
                                ),
                              )),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 35, right: 35, top: 0),
                            child: FutureBuilder<List<Proviniencia>>(
                                future: database.allProvinienciaEntries,
                                builder: (BuildContext context,
                                    AsyncSnapshot<List<Proviniencia>> snapshot) {
                                  if (!snapshot.hasData)
                                    return CircularProgressIndicator();
                                  return DropdownButtonFormField<Proviniencia>(
                                    decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      labelStyle: TextStyle(
                                        color: Color(0xFF000000),
                                      ),
                                      //helperText: 'Helper text',
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide:
                                        BorderSide(color: Color(0xFF000000)),
                                      ),
                                    ),
                                    items: snapshot.data
                                        .map(
                                            (user) => DropdownMenuItem<Proviniencia>(

                                          child:  Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Icon(
                                                Icons.check,
                                                color: (_proviniencia == null) ? Colors.transparent : ( _proviniencia == user?null:Colors.transparent),
                                              ),
                                              SizedBox(width: 16),
                                              Text(user.nome),

                                            ],
                                          ),
                                          value: user,
                                        ))
                                        .toList(),
                                    onChanged: (Proviniencia value) {
                                      setState(() {
                                        _proviniencia = value;
                                        print(_proviniencia);
                                      });

                                    },
                                    isExpanded: false,
                                    //value: _proviniencia,
                                    hint: Text('Proveniência'),
                                  );
                                }),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 35, right: 35, top: 20),
                            child: FutureBuilder<List<Provincia>>(
                                future: database.allProvinciaEntries,
                                builder: (BuildContext context,
                                    AsyncSnapshot<List<Provincia>> snapshot) {
                                  if (!snapshot.hasData)
                                    return CircularProgressIndicator();
                                  return DropdownButtonFormField<Provincia>(
                                    decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      labelStyle: TextStyle(
                                        color: Color(0xFF000000),
                                      ),
                                      //helperText: 'Helper text',
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide:
                                        BorderSide(color: Color(0xFF000000)),
                                      ),
                                    ),
                                    items: snapshot.data
                                        .map(
                                            (user) => DropdownMenuItem<Provincia>(

                                          child:  Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Icon(
                                                Icons.check,
                                                color: (_provincia == null) ? Colors.transparent : ( _provincia == user?null:Colors.transparent),),
                                              SizedBox(width: 6),
                                              Text(user.nome),
                                            ],
                                          ),
                                          value: user,
                                        ))
                                        .toList(),
                                    onChanged: (Provincia value) {
                                      setState(() {
                                        _provincia = value;
                                        print(_provincia);
                                      });
                                    },
                                    isExpanded: false,
                                    //value: _provincia,
                                    hint: Text('Província'),
                                  );
                                }),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 35, right: 35, top: 20),
                            child: FutureBuilder<List<Bairro>>(
                                future: database.allBairroEntries,
                                builder: (BuildContext context,
                                    AsyncSnapshot<List<Bairro>> snapshot) {
                                  if (!snapshot.hasData)
                                    return CircularProgressIndicator();
                                  return DropdownButtonFormField<Bairro>(
                                    decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      labelStyle: TextStyle(
                                        color: Color(0xFF000000),
                                      ),
                                      //helperText: 'Helper text',
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide:
                                        BorderSide(color: Color(0xFF000000)),
                                      ),
                                    ),
                                    items: snapshot.data
                                        .map(
                                            (user) => DropdownMenuItem<Bairro>(

                                          child:  Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Icon(Icons.check,
                                                color: (_bairro == null) ? Colors.transparent : ( _bairro == user?null:Colors.transparent),),
                                              SizedBox(width: 16),
                                              Text(user.nome),
                                            ],
                                          ),
                                          value: user,
                                        ))
                                        .toList(),
                                    onChanged: (Bairro value) {
                                      setState(() {
                                        _bairro = value;
                                        print(_bairro);
                                      });
                                    },
                                    isExpanded: false,
                                    //value: _provincia,
                                    hint: Text('Bairro'),
                                  );
                                }),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 35, right: 35, top: 20),
                            child: FutureBuilder<List<Sexo>>(
                                future: database.allSexoEntries,
                                builder: (BuildContext context,
                                    AsyncSnapshot<List<Sexo>> snapshot) {
                                  if (!snapshot.hasData)
                                    return CircularProgressIndicator();
                                  return DropdownButtonFormField<Sexo>(
                                    decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      labelStyle: TextStyle(
                                        color: Color(0xFF000000),
                                      ),
                                      //helperText: 'Helper text',
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide:
                                        BorderSide(color: Color(0xFF000000)),
                                      ),
                                    ),
                                    items: snapshot.data
                                        .map(
                                            (user) => DropdownMenuItem<Sexo>(

                                          child:  Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Icon(Icons.check,
                                                color: (_sexo == null) ? Colors.transparent : ( _sexo == user?null:Colors.transparent),),
                                              SizedBox(width: 6),
                                              Text(user.nome),
                                            ],
                                          ),
                                          value: user,
                                        ))
                                        .toList(),
                                    onChanged: (Sexo value) {
                                      setState(() {
                                        _sexo = value;
                                        print(_sexo);
                                      });
                                    },
                                    isExpanded: false,
                                    //value: _provincia,
                                    hint: Text('Sexo'),
                                  );
                                }),
                          ),
                          Padding(
                              padding: const EdgeInsets.only(
                                  left: 35, right: 35, top: 20),
                              child: DateTimeField(
                                format: format,
                                onShowPicker: (context, currentValue) {
                                  //print(currentValue);
                                  return showDatePicker(
                                      context: context,
                                      firstDate: DateTime(1900),
                                      initialDate: currentValue ?? DateTime.now(),
                                      lastDate: DateTime(2100));
                                },
                                onChanged:(DateTime time){
                                  print(time);
                                },

                                decoration: InputDecoration(
                                  filled: true,
                                  hintText:'Data de Nascimento',
                                  fillColor: Colors.white,
                                  labelStyle: TextStyle(
                                    color: Color(0xFF000000),
                                  ),
                                  //helperText: 'Helper text',
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                    BorderSide(color: Color(0xFF000000)),
                                  ),
                                ),
                              )
                          ),
                          Padding(padding: const EdgeInsets.only(left: 35,right: 35,top: 20),
                            child: InternationalPhoneInput(
                              decoration: InputDecoration.collapsed(
                                hintText:'Contacto',
                                fillColor: Colors.white,
                              ),
                              onPhoneNumberChange: onPhoneNumberChange,
                              initialPhoneNumber: phoneNumber,
                              initialSelection: phoneIsoCode,
                              enabledCountries: ['+258'],
                              showCountryCodes: true,
                              showCountryFlags: true,
                            ),),
                          Padding(
                              padding: const EdgeInsets.only(
                                  left: 35, right: 35, top: 0),
                              child: DateTimeField(
                                format: format,
                                onShowPicker: (context, currentValue) {
                                  //print(currentValue);
                                  return showDatePicker(
                                      context: context,
                                      firstDate: DateTime(1900),
                                      initialDate: currentValue ?? DateTime.now(),
                                      lastDate: DateTime(2100));
                                },
                                onChanged:(DateTime time){
                                  print(time);
                                },

                                decoration: InputDecoration(
                                  filled: true,
                                  hintText:'Data de atendimento',
                                  fillColor: Colors.white,
                                  labelStyle: TextStyle(
                                    color: Color(0xFF000000),
                                  ),
                                  //helperText: 'Helper text',
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                    BorderSide(color: Color(0xFF000000)),
                                  ),
                                ),
                              )
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 35, right: 35, top: 20),
                            child: FutureBuilder<List<MotivoDeAberturaDeProcesso>>(
                              future:   database.allMotivoDeAberturaDeProcessoEntries,
                              // ignore: missing_return
                              builder: (BuildContext context,AsyncSnapshot<List<MotivoDeAberturaDeProcesso>> snapshot){
                                if (!snapshot.hasData)return CircularProgressIndicator();

                                return DropdownButtonFormField<MotivoDeAberturaDeProcesso>(
                                  value: selectedValues.isEmpty?null:selectedValues.last,
                                  onChanged: (MotivoDeAberturaDeProcesso newValue){
                                    setState(() {
                                      if (selectedValues.contains(newValue)){
                                        print(newValue);
                                        selectedValues.remove(newValue);
                                      }
                                      else{
                                        selectedValues.add(newValue);
                                        print(selectedValues);
                                      }
                                      // print(newValue);

                                      //print(selectedValues);
                                    });

                                  },
                                  items: snapshot.data.map<DropdownMenuItem<MotivoDeAberturaDeProcesso>>(
                                          (MotivoDeAberturaDeProcesso value){
                                        return DropdownMenuItem<MotivoDeAberturaDeProcesso>(
                                          value: value,
                                          child: Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Icon(
                                                Icons.playlist_add_check,
                                                color: selectedValues.contains(value) ? null : Colors.transparent,
                                              ),
                                              SizedBox(width: 16),
                                              Text(value.nome),
                                            ],
                                          ),
                                        );
                                      }
                                  ).toList(),
                                  decoration:InputDecoration(
                                    filled:true,
                                    fillColor: Colors.white,
                                    labelText: 'Motivo de abertura do processo',
                                    labelStyle: TextStyle(
                                      color: Color(0xFF000000),
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: Color(0xFF000000)),
                                    ),
                                  ) ,
                                );

                              },
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 35, right: 35, top: 20),
                            child: FutureBuilder<List<Documento>>(
                              future:   database.allDocumentoEntries,
                              // ignore: missing_return
                              builder: (BuildContext context,AsyncSnapshot<List<Documento>> snapshot){
                                if (!snapshot.hasData)return CircularProgressIndicator();

                                return DropdownButtonFormField<Documento>(
                                  value: selectedDocs.isEmpty?null:selectedDocs.last,
                                  onChanged: (Documento newValue){
                                    setState(() {
                                      if (selectedDocs.contains(newValue)){
                                        print(newValue);
                                        selectedDocs.remove(newValue);
                                      }
                                      else{
                                        selectedDocs.add(newValue);
                                        print(selectedDocs);
                                      }
                                      // print(newValue);

                                      //print(selectedValues);
                                    });

                                  },
                                  items: snapshot.data.map<DropdownMenuItem<Documento>>(
                                          (Documento value){
                                        return DropdownMenuItem<Documento>(
                                          value: value,
                                          child: Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Icon(
                                                Icons.playlist_add_check,
                                                color: selectedDocs.contains(value) ? null : Colors.transparent,
                                              ),
                                              SizedBox(width: 16),
                                              Text(value.nome),
                                            ],
                                          ),
                                        );
                                      }
                                  ).toList(),
                                  decoration:InputDecoration(
                                    filled:true,
                                    fillColor: Colors.white,
                                    labelText: 'Documentos necessários',
                                    labelStyle: TextStyle(
                                      color: Color(0xFF000000),
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: Color(0xFF000000)),
                                    ),
                                  ) ,
                                );

                              },
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 35, right: 35, top: 20),
                            child: FutureBuilder<List<ServicoEncaminhado>>(
                                future: database.allServicoEncaminhadoEntries,
                                builder: (BuildContext context,
                                    AsyncSnapshot<List<ServicoEncaminhado>> snapshot) {
                                  if (!snapshot.hasData)
                                    return CircularProgressIndicator();
                                  return DropdownButtonFormField<ServicoEncaminhado>(
                                    decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      labelStyle: TextStyle(
                                        color: Color(0xFF000000),
                                      ),
                                      //helperText: 'Helper text',
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide:
                                        BorderSide(color: Color(0xFF000000)),
                                      ),
                                    ),
                                    items: snapshot.data
                                        .map(
                                            (user) => DropdownMenuItem<ServicoEncaminhado>(

                                          child:  Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Icon(
                                                Icons.check,
                                                color: (_selectedServ == null) ? Colors.transparent : ( _selectedServ == user?null:Colors.transparent),
                                              ),
                                              SizedBox(width: 16),
                                              Text(user.nome),

                                            ],
                                          ),
                                          value: user,
                                        ))
                                        .toList(),
                                    onChanged: (ServicoEncaminhado value) {
                                      setState(() {
                                        _selectedServ = value;
                                        print(_selectedServ);
                                      });

                                    },
                                    isExpanded: false,
                                    //value: _proviniencia,
                                    hint: Text('Especificar Serviço Encaminhado'),
                                  );
                                }),
                          ),
                          Padding(
                              padding: const EdgeInsets.only(
                                  left: 35, right: 35, top: 20),
                              child: TextField(
                                cursorColor: Theme.of(context).cursorColor,
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.white,
                                  labelText: 'Especificar Serviço Encaminhado',
                                  labelStyle: TextStyle(
                                    color: Color(0xFF000000),
                                  ),
                                  //helperText: 'Helper text',
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                    BorderSide(color: Color(0xFF000000)),
                                  ),
                                ),
                              )),
                          Padding(
                              padding: const EdgeInsets.only(
                                  left: 35, right: 35, top: 20),
                              child: DropdownButtonFormField<int>(
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.white,
                                  labelStyle: TextStyle(
                                    color: Color(0xFF000000),
                                  ),
                                  //helperText: 'Helper text',
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                    BorderSide(color: Color(0xFF000000)),
                                  ),
                                ),
                                items: values
                                    .map(
                                        (user) => DropdownMenuItem<int>(
                                      child:  Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          Icon(Icons.check,
                                              color: (_acompanhamento  == null) ? Colors.transparent :( (_acompanhamento == user)?null:Colors.transparent)),
                                          SizedBox(width: 6),
                                          Text("${(user == 1)?"Sim":"Nao"}"),
                                        ],
                                      ),
                                      value: user,
                                    ))
                                    .toList(),
                                onChanged: (int value) {
                                  setState(() {
                                    _acompanhamento  =  value;
                                    print(_acompanhamento);
                                  });
                                },
                                isExpanded: false,
                                //value: _provincia,
                                hint: Text('Acompanhamento'),
                              )

                          ),
                          Padding(
                              padding: const EdgeInsets.only(
                                  left: 35, right: 35, top: 20),
                              child: DateTimeField(
                                format: format,
                                onShowPicker: (context, currentValue) {
                                  //print(currentValue);
                                  return showDatePicker(
                                      context: context,
                                      firstDate: DateTime(1900),
                                      initialDate: currentValue ?? DateTime.now(),
                                      lastDate: DateTime(2100));
                                },
                                onChanged:(DateTime time){
                                  print(time);
                                },

                                decoration: InputDecoration(
                                  filled: true,
                                  hintText:'Data em que foi recebida pelo serviço',
                                  fillColor: Colors.white,
                                  labelStyle: TextStyle(
                                    color: Color(0xFF000000),
                                  ),
                                  //helperText: 'Helper text',
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                    BorderSide(color: Color(0xFF000000)),
                                  ),
                                ),
                              )
                          ),
                          Padding(
                              padding: const EdgeInsets.only(
                                  left: 35, right: 35, top: 20,bottom: 50),
                              child: DropdownButtonFormField<int>(
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.white,
                                  labelStyle: TextStyle(
                                    color: Color(0xFF000000),
                                  ),
                                  //helperText: 'Helper text',
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                    BorderSide(color: Color(0xFF000000)),
                                  ),
                                ),
                                items: values
                                    .map(
                                        (user) => DropdownMenuItem<int>(
                                      child:  Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          Icon(Icons.check,
                                              color: (_problema  == null) ? Colors.transparent :( (_problema == user)?null:Colors.transparent)),
                                          SizedBox(width: 6),
                                          Text("${(user == 1)?"Sim":"Nao"}"),
                                        ],
                                      ),
                                      value: user,
                                    ))
                                    .toList(),
                                onChanged: (int value) {
                                  setState(() {
                                    _problema  =  value;
                                    print(_problema);
                                  });
                                },
                                isExpanded: false,
                                //value: _provincia,
                                hint: Text('Problema resolvido?'),
                              )

                          ),



                        ])),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(50),
                      topRight: Radius.circular(50)
                  ),

                  color: Colors.white,

                ),
              )

            ],
          )
        ],
      ),

    );
  }
}