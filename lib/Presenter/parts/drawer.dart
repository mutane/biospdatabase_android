import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
class MyDrawer extends StatefulWidget {
  @override
  _MyDrawerState createState() => _MyDrawerState();

}
class _MyDrawerState extends State<MyDrawer>{
  @override
  Widget build(BuildContext context) {
    return  Drawer(
        child: ListView(
          children: [
            Container(

              child: UserAccountsDrawerHeader(
                  margin: EdgeInsets.only(top: 0),
                  accountName: new Text('Beira Mananga',style:TextStyle(
                      color: Color(0xFF185c37),
                      fontWeight: FontWeight.bold,
                      fontSize: 15
                  )),

                  decoration: BoxDecoration(

                      image: DecorationImage(
                          image: AssetImage('images/dr.png'),
                          fit: BoxFit.fitWidth
                      )
                  )
              ),

            ),
            ListTile(
              leading: FaIcon(FontAwesomeIcons.addressCard,size: 20,color: Colors.orange,),
              title:new Text( "Registos", style: TextStyle(
                  color: Colors.orange
              ),),
              onTap: (){
                Navigator.pop(context);
              },
              selected:true,

            ),
            ListTile(

              leading: FaIcon(FontAwesomeIcons.syncAlt,size: 20),
              title:new Text( "Sincronizar dados"),
              onTap: (){
                Navigator.pop(context);
              },
            ),
            ListTile(

              leading: FaIcon(FontAwesomeIcons.facebookMessenger,size: 20),
              title:new Text( "Enviar Mensagens"),
              onTap: (){
                Navigator.pop(context);
              },
            ),
            ListTile(

              leading: Icon(Icons.assignment),
              title:new Text( "Relatorios"),
              onTap: (){
                Navigator.pop(context);
              },
            ),
            Divider(),
            ListTile(
              leading: Icon(Icons.info_outline),
              title:new Text( "Relatar problema"),
              onTap: (){},
            ),
            ListTile(
              leading: Icon(Icons.settings),

              title:new Text( "Definições"),
              onTap: (){},
            ),

          ],
        )
    );
  }

}

