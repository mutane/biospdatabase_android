import 'dart:ui';
import 'package:Biospdatabase/Model/Benificiario.dart';
import 'package:Biospdatabase/Presenter/form.dart';
import 'package:Biospdatabase/Presenter/parts/benificiariosList.dart';
import 'package:Biospdatabase/Presenter/parts/drawer.dart';
import 'package:Biospdatabase/Presenter/parts/newBen.dart';
import 'package:Biospdatabase/Presenter/searchData.dart';
import 'package:Biospdatabase/Routes/app_routes.dart';
import 'package:Biospdatabase/data/moor_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:google_fonts/google_fonts.dart';
class HomePage extends StatefulWidget{

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState  extends State<HomePage> {

  int _currentIndex = 0;
  PageController _pageController;
  final List<Widget> _list = <Widget>[
    BenificiariosList(),
    NewBenificiario(),
  ];
  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  BenificiarioResource st = new BenificiarioResource();
  String _value;
  String capitalize(String string) {
    if (string == null) {
      throw ArgumentError("string: $string");
    }

    if (string.isEmpty) {
      return string;
    }

    return string[0].toUpperCase() + string.substring(1);
}
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      bottomNavigationBar: BottomNavyBar(
      selectedIndex: _currentIndex,
      showElevation: true, // use this to remove appBar's elevation
      onItemSelected: (index) => setState(() {
        _currentIndex = index;
        _pageController.animateToPage(index,
            duration: Duration(milliseconds: 300), curve: Curves.ease);
      }),
      items: [
        BottomNavyBarItem(
          icon: Icon(Icons.apps,size: 25),
          title: Text(' Todos'),
          activeColor: Colors.orange,
            inactiveColor:Colors.black
        ),
        BottomNavyBarItem(
            icon: FaIcon(FontAwesomeIcons.addressCard,size: 20),
            title: Text(' Registar'),
            activeColor: Colors.orange,
            inactiveColor:Colors.black
        ),
        BottomNavyBarItem(
            icon: FaIcon(FontAwesomeIcons.solidChartBar,size: 20),
            title: Text(' Relatorios'),
            activeColor: Colors.orange,
            inactiveColor:Colors.black
        ),
        BottomNavyBarItem(
            icon: FaIcon(FontAwesomeIcons.sync,size: 20,),
            title: Text(' Sincronizar'),
            activeColor: Colors.orange,
            inactiveColor:Colors.black
        ),
      ],
    ),
      body: SizedBox.expand(
          child: PageView(
              controller: _pageController,
                onPageChanged: (index) {
                  setState(() => _currentIndex = index);
                },
            children: <Widget>[
              BenificiariosList(),
            ],
    ),
    ),
     drawer: MyDrawer(),
    );
  }
//


}


