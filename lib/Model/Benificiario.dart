import 'package:Biospdatabase/data/moor_database.dart';

class BenificiarioResource{
  int id;
  String nome;
  int  visita;
  String proviniencia;
  String provincia;
  String bairro;
  String sexo;
  List<dynamic> contatco;
  DateTime dataDeAtendimento;
  String objectivoDaVistax;
  String motivoDeAberturaDoProcessox;
  List<dynamic> documentos;
  List<dynamic> servicos;
  List<dynamic> especificarServico;
  bool necessidaDAD;
  DateTime dataQueFoiRecebido;
  bool problemaResolvido;
  String ref;
  DateTime createdAt;
  DateTime updatedAt;

  BenificiarioResource({
          this.id,
          this.nome,
          this.visita,
          this.proviniencia,
          this.provincia,
          this.bairro,
          this.sexo,
          this.contatco,
          this.dataDeAtendimento,
          this.objectivoDaVistax,
          this.motivoDeAberturaDoProcessox,
          this.documentos,
          this.servicos,
          this.especificarServico,
          this.necessidaDAD,
          this.dataQueFoiRecebido,
          this.problemaResolvido,
          this.ref,
          this.createdAt,
          this.updatedAt
      });

  Future<Map<String,dynamic>> resource(MyDatabase database,Benificiario benificiario) async{
    Proviniencia proviniencia  =  await database.ProvinienciaById(benificiario.provinienciaId);
    Provincia provincia  =  await database.provinciaById(benificiario.provinciaId);
    Bairro bairro  =  await database.bairroById(benificiario.bairroId);
    Sexo sex  =  await database.SexoById(benificiario.sexoId);
    ObjectivoDaVisita objectivoDaVisita = await database.ObjectivoDaVisitaById(benificiario.ObjectivoDaVistaId);
    List<Contacto> contact  =  (await database.allContactoEntries..where((element) => (element.benificiarioId == benificiario.id)) );
    List<BenificiarioEncaminhado> benf  = await database.allBenificiarioEncaminhadoEntries..where((element) => (element.benificiarioId  == benificiario.id));
    var ben = benf.elementAt(0);
    var docs  = await database.manyDocs(benificiario.id);
    var service  =  await database.servicosEncaminhados(benificiario.id);
    var    serveEsp =  await database.servicosEncaminhadosEsp(benificiario.id);
    var motvDab = await database.MotivoDeAberturaDeProcessoById(ben.motivoDeAberturaDeProcesso);
    var t  = []; var d  = []; var s  = []; var s_esp = [];
    serveEsp.forEach((element) {s_esp.add(element);});

    service.forEach((element) {s.add(element.nome);});

    contact.forEach((element) {t.add(element.contacto);});

    docs.forEach((element) {d.add(element.nome);});

    return {
      'Id':benificiario.id,
      'Nome Completo':benificiario.nome,
      '1° Visita ou Frequência':benificiario.numeroDeVisita,
      'Proveniência':proviniencia.nome,
      'Província':provincia.nome,
      'Bairro':bairro.nome,
     'Sexo':sex.nome,
      'Contactos':t,
      'Data de atendimento':benificiario.dataDeAtendimento,
      'Objetivo da visita':objectivoDaVisita.nome,
      'Motivo de abertura do processo':motvDab,
      'Documentos necessários':d,
      'Serviço Encaminhado':s,
      'Especificar Serviço Encaminhado':s_esp,
      'Necessita de acompanhamento domiciliar?':ben.necessitaDeAcompanhamenBairromiciliar?'Sim':'Não',
      'Data em que foi recebido pelo serviço':ben.dataQueFoiRecebido,
      'Problema resolvido?':ben.problemaResolvido?'Sim':'Não ',
      'Referencia':benificiario.ref,
      'Data de lançamento do dado':benificiario.createdAt,
      'Data da atualização do dado':benificiario.updatedAat
    };
  }
  Future<BenificiarioResource> maps(MyDatabase database,Benificiario benificiario) async{
    Proviniencia proviniencia  =  await database.ProvinienciaById(benificiario.provinienciaId);
    Provincia provincia  =  await database.provinciaById(benificiario.provinciaId);
    Bairro bairro  =  await database.bairroById(benificiario.bairroId);
    Sexo sex  =  await database.SexoById(benificiario.sexoId);
    ObjectivoDaVisita objectivoDaVisita = await database.ObjectivoDaVisitaById(benificiario.ObjectivoDaVistaId);
    List<Contacto> contact  =  (await database.allContactoEntries..where((element) {return (element.benificiarioId == benificiario.id);}) );
    List<BenificiarioEncaminhado> benf  = await database.allBenificiarioEncaminhadoEntries.. where((element) => (element.benificiarioId == benificiario.id));
    var ben = benf.last;
    var dd = await database.allBenificiarioEncaminhadoDocumentoEntries;
    var docs  = await database.manyDocs(benf.first.id);
    var service  =  await database.servicosEncaminhados(benf.first.id);
    var serveEsp =  await database.servicosEncaminhadosEsp(benf.first.id);
    var motvDab = await database.MotivoDeAberturaDeProcessoById(ben.motivoDeAberturaDeProcesso);
    var txc = await database.allMotivoDeAberturaDeProcessoEntries;
    var t  = []; var d  = []; var s  = []; var s_esp = [];
    serveEsp.forEach((element) {s_esp.add(element);});
  print("servicos: ${ben}");
    service.forEach((element) {s.add(element.nome);});
    contact.forEach((element) {
      if(element.benificiarioId == benificiario.id){
        t.add(element.contacto);
      }
     });
    docs.forEach((element) {d.add(element.nome);});

    return BenificiarioResource(
      id:benificiario.id,
      nome:benificiario.nome,
      visita:benificiario.numeroDeVisita,
      proviniencia:provincia.nome,
      provincia:provincia.nome,
      bairro:bairro.nome,
      sexo:sex.nome,
      contatco:t,
      dataDeAtendimento:benificiario.dataDeAtendimento,
      objectivoDaVistax:objectivoDaVisita.nome,
        motivoDeAberturaDoProcessox:motvDab!=null?motvDab.nome:null,
      documentos :d,
      servicos:s,
      especificarServico:s_esp,
      necessidaDAD:ben.necessitaDeAcompanhamenBairromiciliar,
      dataQueFoiRecebido:ben.dataQueFoiRecebido,
      problemaResolvido:ben.problemaResolvido,
      ref:benificiario.ref,
      createdAt:benificiario.createdAt,
      updatedAt:benificiario.updatedAat
    );
  }


  @override
  String toString() {
    // TODO: implement toString
    return "{id:${this.id}, nome: ${this.nome}, visitas: ${this.visita}, proviniencia: ${this.proviniencia}, provincia: ${this.provincia}, bairro: ${this.bairro},"+
        "sexo: ${this.sexo},contatcos: ${this.contatco}, data de tendimento: ${this.dataDeAtendimento}, objectivvo da visita: ${this.objectivoDaVistax}, MvAP : ${this.motivoDeAberturaDoProcessox},"+
" documentos: ${this.documentos}, servicos: ${this.servicos}, especificar Servico: ${this.especificarServico}, necessida de acompanhamento : ${this.necessidaDAD}, "+
" data que foi recebido: ${this.dataQueFoiRecebido}, problema resolvido? : ${this.problemaResolvido},referencia: ${this.ref}, criado em : ${this.createdAt}, atuallizado em : ${this.updatedAt}";
  }
}